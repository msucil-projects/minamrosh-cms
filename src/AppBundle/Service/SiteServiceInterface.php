<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 5/31/2017
 * Time: 10:54 PM
 */

namespace AppBundle\Service;



use AppBundle\Domain\DTO\SiteDTO;

interface SiteServiceInterface {

	public function save(SiteDTO $site);

	public function update(SiteDTO $site);

	public function findOne($id = null);
}