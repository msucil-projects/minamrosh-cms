<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 7:58 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\DataTableRequestBody;
use AppBundle\Domain\DTO\PostCategoryDTO;

interface PostCategoryServiceInterface
{
    public function save(PostCategoryDTO $categoryDTO);

    public function update(PostCategoryDTO $categoryDTO);

    public function findById($id);

    public function findBySlug($slug);

    public function deleteById($id);

    public function getPostMenuItems();

    public function findAll(array $criteria = null, array  $orderBy = null, $limit = null, $offset = null);

    public function getPostCategories(DataTableRequestBody $dataTableRequestBody);

}