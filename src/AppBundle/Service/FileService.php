<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/7/17
 * Time: 12:17 PM
 */

namespace AppBundle\Service;


use AppBundle\Util\StringHelper;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService implements FileServiceInterface
{
    private $absolutePath;

    private $relativePath;

    public function __construct($absolutePath, $relativePath)
    {
        $this->absolutePath = $absolutePath;
        $this->relativePath = $relativePath;
    }

    private function generateFileName($fileName){
        $name = StringHelper::generateSlug($fileName);
        return StringHelper::generateUnique($name);
    }

    public function uploadFile(UploadedFile $file)
    {
        $name = $this->generateFileName($file->getClientOriginalName());

        if($file->move($this->absolutePath, $name)){
            return $name;
        };
        return null;
    }

    public function getViewFileName($fileName)
    {
        return $this->relativePath . $fileName;
    }

    public function deleteFile($fileName)
    {
        @unlink($this->absolutePath . $fileName);
    }

    public function getAbsoluteFileName($fileName)
    {
        return $this->absolutePath . $fileName;
    }
}