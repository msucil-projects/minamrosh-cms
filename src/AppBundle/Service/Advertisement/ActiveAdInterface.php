<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 7/5/17
 * Time: 10:54 PM
 */

namespace AppBundle\Service\Advertisement;


interface ActiveAdInterface
{
    public function getActiveAds($adSection, $limit = null, $offset = null);
}