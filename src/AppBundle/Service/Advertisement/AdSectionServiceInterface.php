<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/25/17
 * Time: 11:23 PM
 */

namespace AppBundle\Service\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdSectionDTO;

interface AdSectionServiceInterface
{
    public function save(AdSectionDTO $sectionDTO);

    public function update(AdSectionDTO $sectionDTO);

    public function findById($id);

    public function deleteById($id);

    public function findAll();

    public function findAllBy(array $criteria = null, array  $orderBy = null, $limit = null, $offset = null);

    public function findOneBy(array $criteria);
}