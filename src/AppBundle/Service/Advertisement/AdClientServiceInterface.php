<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 11:45 PM
 */

namespace AppBundle\Service\Advertisement;



use AppBundle\Domain\DTO\Advertisement\AdClientDTO;

interface AdClientServiceInterface
{
    public function save(AdClientDTO $adsClientDTO);

    public function update(AdClientDTO $adsClientDTO);

    public function deleteById($id);

    public function findAll(array $criteria, $orderBy = null, $limit = null, $offset = null);

    public function findOne(array $criteria);

}