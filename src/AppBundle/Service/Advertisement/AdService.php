<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/27/17
 * Time: 2:12 PM
 */

namespace AppBundle\Service\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdDTO;
use AppBundle\Domain\Entity\Advertisement\Ad;
use AppBundle\Domain\Mapper\Advertisement\AdMapper;
use AppBundle\Repository\Advertisement\AdRepository;

class AdService implements AdServiceInterface, ActiveAdInterface
{
    private $adRepository;
    private $sectionRepository;
    private $clientService;

    public function __construct(AdRepository $repository,
                                AdSectionServiceInterface $sectionRepository,
                                AdClientServiceInterface $clientService
    )
    {
        $this->adRepository = $repository;
        $this->sectionRepository = $sectionRepository;
        $this->clientService = $clientService;
    }

    public function save(AdDTO $adDTO)
    {
        $ad = AdMapper::mapToEntity($adDTO);
        $this->setAdSection($ad);
        $this->setAdClient($ad);

        return $this->adRepository->save($ad);
    }

    public function update(AdDTO $adDTO)
    {
        $ad = AdMapper::mapToEntity($adDTO);
        $this->setAdClient($ad);
        $this->setAdSection($ad);

        return $this->adRepository->update($ad);
    }

    public function deleteById($id)
    {
        return $this->adRepository->deleteById($id);
    }

    public function findAll()
    {
        return $this->adRepository->findAll();
    }

    public function findOneBy(array $criteria)
    {
        return $this->adRepository->findOneBy($criteria);
    }

    public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->adRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    private function setAdSection(Ad $ad){
        $ad->setSection($this->sectionRepository->findOneBy(['id' => $ad->getSection()->getId()]));
    }

    private function setAdClient(Ad $ad)
    {
        $ad->setClient($this->clientService->findOne(['id' => $ad->getClient()->getId()]));
    }

    public function getActiveAds($adSection, $limit = null, $offset = null)
    {
        return $this->adRepository->getActiveAds($adSection);
    }
}