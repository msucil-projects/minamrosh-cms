<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/27/17
 * Time: 2:09 PM
 */

namespace AppBundle\Service\Advertisement;



use AppBundle\Domain\DTO\Advertisement\AdDTO;

interface AdServiceInterface
{

    public function save(AdDTO $adDTO);

    public function update(AdDTO $adDTO);

    public function deleteById($id);

    public function findAll();

    public function findOneBy(array $criteria);

    public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}