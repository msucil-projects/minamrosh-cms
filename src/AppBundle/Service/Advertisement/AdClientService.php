<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 11:47 PM
 */

namespace AppBundle\Service\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdClientDTO;
use AppBundle\Domain\Mapper\Advertisement\AdClientMapper;
use AppBundle\Repository\Advertisement\AdClientRepository;

class AdClientService implements AdClientServiceInterface
{
    private $clientRepository;

    public function __construct(AdClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function save(AdClientDTO $adsClientDTO)
    {
        return $this->clientRepository->save(AdClientMapper::mapToEntity($adsClientDTO));
    }

    public function update(AdClientDTO $adsClientDTO)
    {
        return $this->clientRepository->update(AdClientMapper::mapToEntity($adsClientDTO));
    }

    public function deleteById($id)
    {
        return $this->clientRepository->deleteById($id);
    }

    public function findAll(array $criteria, $orderBy = null, $limit = null, $offset = null)
    {
        return $this->clientRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOne(array $criteria)
    {
        return $this->clientRepository->findOneBy($criteria);
    }
}