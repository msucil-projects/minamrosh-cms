<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/25/17
 * Time: 11:25 PM
 */

namespace AppBundle\Service\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdSectionDTO;
use AppBundle\Domain\Mapper\Advertisement\AdSectionMapper;
use AppBundle\Repository\Advertisement\AdSectionRepository;

class AdSectionService implements AdSectionServiceInterface
{
    private $sectionRepository;

    public function __construct(AdSectionRepository $sectionRepository)
    {
        $this->sectionRepository = $sectionRepository;
    }

    public function save(AdSectionDTO $sectionDTO)
    {
        $section = AdSectionMapper::mapToEntity($sectionDTO);
        return $this->sectionRepository->save($section);
    }

    public function update(AdSectionDTO $sectionDTO)
    {
        $section = AdSectionMapper::mapToEntity($sectionDTO);
        return $this->sectionRepository->update($section);
    }

    public function findById($id)
    {
        return $this->sectionRepository->findOneBy(['id' => $id]);
    }


    public function deleteById($id)
    {
       return $this->sectionRepository->deleteById($id);
    }

    public function findAllBy(array $criteria = null, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->sectionRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findAll()
    {
        return $this->sectionRepository->findAll();
    }

    public function findOneBy(array $criteria)
    {
        return $this->sectionRepository->findOneBy($criteria);
    }
}