<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 5/31/2017
 * Time: 10:55 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\SiteDTO;
use AppBundle\Domain\Mapper\SiteMapper;
use AppBundle\Repository\SiteRepository;

class SiteService implements SiteServiceInterface {

	private $repository;

	public function __construct(SiteRepository $repository) {
		$this->repository = $repository;
	}

	public function save( SiteDTO $siteDto ) {
		$site = SiteMapper::mapToEntity($siteDto);

		return $this->repository->save($site);
	}

	public function update( SiteDTO $siteDto ) {
		$site = SiteMapper::mapToEntity($siteDto);

		return $this->repository->update($site);
	}

	public function findOne( $id = null ) {
		$sites =  $this->repository->findAll();
		if($sites){
			return $sites[0];
		}

		return null;
	}
}