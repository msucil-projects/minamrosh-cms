<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/12/17
 * Time: 9:38 PM
 */

namespace AppBundle\Service;

use AppBundle\Domain\DTO\DataTableRequestBody;

interface DataTableInterface
{
    function getDataTableResponse(array $response);

    function getOrderColumnAndDirection(DataTableRequestBody $dataTableRequestBody);
}