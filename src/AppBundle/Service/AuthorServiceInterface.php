<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:24 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\AuthorDto;

interface AuthorServiceInterface {

	public function save(AuthorDto $dto);

	public function update(AuthorDto $dto);

	public function deleteById($id);

	public function findOne(array $criteria);

	public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}