<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/31/17
 * Time: 12:14 AM
 */

namespace AppBundle\Service;


use AppBundle\Service\Advertisement\AdClientServiceInterface;
use AppBundle\Service\Advertisement\AdServiceInterface;

class DashboardService implements DashboardServiceInterface
{
    private static $DEFAULT_LIMIT = 7;

    private $postService;
    private $pageService;
    private $adService;
    private $clientService;

    public function __construct(PostServiceInterface $postService,
                                PageServiceInterface $pageService,
                                AdServiceInterface $adService,
                                AdClientServiceInterface $clientService
    )
    {
        $this->postService = $postService;
        $this->pageService = $pageService;
        $this->adService = $adService;
        $this->clientService = $clientService;
    }

    public function getRecentPost($limit)
    {
        $postLimit = self::$DEFAULT_LIMIT;

        if ($limit) {
            $postLimit = $limit;
        }

        return $this->postService->findAll([], ['createdAt' => 'DESC'], $postLimit);
    }

    public function getPages()
    {
        return $this->pageService->findAll();
    }

    public function getAdvertisements($limit)
    {
        $adLimit = self::$DEFAULT_LIMIT;

        if ($limit) {
            $adLimit = $limit;
        }

        return $this->adService->findAllBy(['expired' => false], ['createdAt' => 'DESC'], $adLimit);
    }

    public function getAdvertisementClients()
    {
        return $this->clientService->findAll(
            [],
            ['created_at' => 'DESC'],
            self::$DEFAULT_LIMIT
        );
    }

    public function getExpiredAdvertisements()
    {
        return $this->adService
            ->findAllBy(
                ['expired' => true],
                ['createdAt' => 'DESC'],
                self::$DEFAULT_LIMIT
            );
    }
}