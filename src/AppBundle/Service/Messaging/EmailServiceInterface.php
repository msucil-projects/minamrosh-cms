<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 8/21/2017
 * Time: 10:50 PM
 */

namespace AppBundle\Service\Messaging;


interface EmailServiceInterface {

	function sendSystemEmail( $subject, array $message, $to, $contentType = ContentType::PLAIN );

	function sendSystemInfoEmail( $subject, array $message, $to, $contentType = ContentType::PLAIN );

	function sendEmail( $from, $subject, array $message, $to, $contentType = ContentType::PLAIN );
}