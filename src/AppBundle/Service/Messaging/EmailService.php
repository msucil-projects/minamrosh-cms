<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 8/21/2017
 * Time: 10:54 PM
 */

namespace AppBundle\Service\Messaging;


use Symfony\Component\DependencyInjection\Container;

class EmailService implements EmailServiceInterface {
	private $container;
	private $mailer;

	public function __construct( Container $container, \Swift_Mailer $mailer ) {
		$this->container = $container;
		$this->mailer    = $mailer;
	}

	function sendSystemEmail( $subject, array $msg, $to, $contentType = ContentType::PLAIN ) {
		$message = new \Swift_Message();

		$message->setFrom( $this->container->getParameter( 'sys_email' ) )
		        ->setSubject( $subject )
		        ->setBody( $this->container->get( 'twig' )->render( ':email:system-email.html.twig', $msg ), $contentType )
		        ->setTo( $to );

		return $this->mailer->send( $message );
	}

	function sendSystemInfoEmail( $subject, array $msg, $to, $contentType = ContentType::PLAIN ) {
		$message = new \Swift_Message();
		$message->setFrom( $this->container->getParameter( 'info_email' ) )
		        ->setSubject( $subject )
			->setBody( $this->container->get( 'twig' )->render( ':email:system-email.html.twig', $msg ), $contentType )
		        ->setTo( $to );

		return $this->mailer->send( $message );
	}

	function sendEmail( $from, $subject, array $msg, $to, $contentType = ContentType::PLAIN ) {
		$message = new \Swift_Message();
		$message->setFrom( $from )
		        ->setSubject( $subject )
			->setBody( $this->container->get( 'twig' )->render( ':email:system-email.html.twig', $msg ), $contentType )
		        ->setTo( $to );

		return $this->mailer->send( $message );
	}
}