<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 8/21/2017
 * Time: 11:07 PM
 */

namespace AppBundle\Service\Messaging;


class ContentType {
	const PLAIN = 'text/plain';
	const HTML = 'text/html';
}