<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/11/17
 * Time: 10:00 PM
 */

namespace AppBundle\Service\Frontend;


use AppBundle\Repository\FrontPageRepositoryInterface;

class FrontPageService implements FrontPageServiceInterface
{
    private $pageRepository;

    public function __construct(FrontPageRepositoryInterface $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    public function getPage($category)
    {
        return $this->pageRepository->getPageByCategory($category);
    }
}