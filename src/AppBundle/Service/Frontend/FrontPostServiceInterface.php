<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/14/17
 * Time: 10:39 PM
 */

namespace AppBundle\Service\Frontend;


interface FrontPostServiceInterface
{
    public function getPostsByCategory($category, $limit = 7, $offset = null);

    public function getPostByCategory($category, $slug);

    public function search($search, $limit = 10, $offset = null);

    public function getFeaturedPost($limit, $offset);

    public function getFeaturedPostByCategory($category, $limit);

    public function getPopularPosts($limit);

    public function getPosts(array $criteria, array $orderBy = null, $limit = 10);

    public function getRelatedPosts($category, $limit, $exclude);

}