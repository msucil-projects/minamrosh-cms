<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/11/17
 * Time: 9:59 PM
 */

namespace AppBundle\Service\Frontend;


interface FrontPageServiceInterface
{
    public function getPage($category);
}