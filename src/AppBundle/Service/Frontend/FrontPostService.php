<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/14/17
 * Time: 10:40 PM
 */

namespace AppBundle\Service\Frontend;


use AppBundle\Repository\FrontPostRepository;
use AppBundle\Repository\PostViewRepository;

class FrontPostService implements FrontPostServiceInterface
{
    private $postRepository;
    private $postViewRepository;

    public function __construct(FrontPostRepository $postRepository, PostViewRepository $postViewRepository)
    {
        $this->postRepository = $postRepository;
        $this->postViewRepository = $postViewRepository;
    }

    public function getPostsByCategory($category, $limit = 10, $offset = null){
        return $this->postRepository->getPostsByCategory($category, $limit, $offset);
    }

    public function getFeaturedPostByCategory( $category, $limit ) {
	    return $this->postRepository->getFeaturedPostsByCategory($category, $limit);
    }


	public function getPostByCategory($category, $slug)
    {
        return $this->postRepository->getPostByCategory($category, $slug);
    }

    public function search($term, $limit = 10, $offset = null)
    {
        return $this->postRepository->search($term, $limit, $offset);
    }

    public function getFeaturedPost($limit, $offset)
    {
        return $this->postRepository->getFeaturedPosts($limit, $offset);
    }

    public function getPosts(array $criteria = [], array $orderBy = null, $limit = 10)
    {
        return $this->postRepository->getRecentPosts($criteria,$orderBy, $limit);
    }

    public function getPopularPosts($limit)
    {
        $postIds = $this->postViewRepository->getPopularPostIdsForCurrentMonth($limit);

        if($postIds){
            return $this->postRepository->getPopularPosts($postIds);
        }

        return null;
    }

	public function getRelatedPosts( $category, $limit, $exclude ) {
		return $this->postRepository->getRelatedPostsByCategory($category, $limit, $exclude);
	}
}