<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/8/17
 * Time: 11:04 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\DataTableRequestBody;
use AppBundle\Domain\DTO\PageDTO;
use AppBundle\Domain\Mapper\PageMapper;
use AppBundle\Repository\PageRepository;

class PageService implements PageServiceInterface
{
    private $pageRepository;
    private $dataTableService;
    private $categoryService;

    public function __construct(
        PageRepository $pageRepository,
        DataTableInterface $dataTableService,
        SectionCategoryServiceInterface $categoryService
    )
    {
        $this->pageRepository = $pageRepository;
        $this->dataTableService = $dataTableService;
        $this->categoryService = $categoryService;
    }

    public function save(PageDTO $pageDTO){
        $page = PageMapper::mapToEntity($pageDTO);
        $page->setCategory($this->categoryService->findOneBy(['id' =>$page->getCategory()->getId()]));
        return $this->pageRepository->save($page);
    }

    public function update(PageDTO $pageDTO)
    {
        $page = PageMapper::mapToEntity($pageDTO);
        $page->setCategory($this->categoryService->findOneBy(['id' =>$page->getCategory()->getId()]));
        return $this->pageRepository->update($page);
    }

    public function findById($id)
    {
        return $this->pageRepository->findOneBy(["id" => $id]);
    }

    public function findBySlug($slug)
    {
        return $this->pageRepository->findOneBy(["slug" => $slug]);
    }

    public function findAll(array $criteria = null, array $orderBy = null, $limit = null, $offset = null)
    {
        if($criteria == null || empty($criteria)){
            return $this->pageRepository->findAll();
        }

        return $this->pageRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function deleteById($id)
    {
        return $this->pageRepository->deleteById($id);
    }

    public function getPages(DataTableRequestBody $dataTableRequestBody)
    {
        $draw = $dataTableRequestBody->getDraw();
        $limit = $dataTableRequestBody->getLength();
        $offset = $dataTableRequestBody->getStart();
        $searchTerm = $dataTableRequestBody->getSearch()["value"];
        $orderDirection = $dataTableRequestBody->getOrder();

        $orderBy =$this->dataTableService->getOrderColumnAndDirection($dataTableRequestBody);
        $queryResult = $this->pageRepository->findAllBySortQuery([], $orderBy, $limit, $offset);

        $response = [
            "draw" => $draw,
            "recordsTotal" => $queryResult->getTotalResult(),
            "recordsFiltered" => $queryResult->getTotalResult(),
            "data" => $queryResult->getResult()
        ];

        return $this->dataTableService->getDataTableResponse($response);
    }
}