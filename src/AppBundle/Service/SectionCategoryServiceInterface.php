<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 10:14 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\SectionCategoryDTO;

interface SectionCategoryServiceInterface
{

    public function save(SectionCategoryDTO $dto);

    public function update(SectionCategoryDTO $dto);

    public function delete($id);

    public function findAll();

    public function findOneBy(array $criteria);

}