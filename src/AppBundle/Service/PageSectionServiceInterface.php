<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 12:10 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\PageSectionDTO;

interface PageSectionServiceInterface
{
    public function save(PageSectionDTO $post);

    public function update(PageSectionDTO $postDto);

    public function findById($id);

    public function findBySlug($slug);

    public function deleteById($id);

    public function findAll(array $criteria = null, array  $orderBy = null, $limit = null, $offset = null);

    public function findAllPageSections();

    public function findAllSections();

    public function findBySectionCategory($section);
}