<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/9/17
 * Time: 5:39 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\DataTableRequestBody;
use AppBundle\Domain\DTO\PageDTO;

interface PageServiceInterface
{
    public function save(PageDTO $post);

    public function update(PageDTO $postDto);

    public function findById($id);

    public function findBySlug($slug);

    public function deleteById($id);

    public function findAll(array $criteria = null, array  $orderBy = null, $limit = null, $offset = null);

    public function getPages(DataTableRequestBody $dataTableRequestBody);
}