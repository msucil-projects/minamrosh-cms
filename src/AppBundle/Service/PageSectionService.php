<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 12:09 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\PageSectionDTO;
use AppBundle\Domain\Entity\PageSection;
use AppBundle\Domain\Entity\SectionCategory;
use AppBundle\Domain\Mapper\PageSectionMapper;
use AppBundle\Repository\PageSectionRepository;
use AppBundle\Util\SectionType;

class PageSectionService implements PageSectionServiceInterface
{
    private $repository;
    private $categoryService;
    private $pageService;

    public function __construct(PageSectionRepository $repository,
                                SectionCategoryServiceInterface $categoryService,
                                PageServiceInterface $pageService
    )
    {
        $this->repository = $repository;
        $this->categoryService = $categoryService;
        $this->pageService = $pageService;
    }

    public function save(PageSectionDTO $dto)
    {
        $pageSection = PageSectionMapper::mapToEntity($dto);

        $this->setPage($pageSection);
        $this->setCategory($pageSection);

        $this->repository->save($pageSection);
        return $pageSection;
    }

    public function update(PageSectionDTO $dto)
    {
        $pageSection = PageSectionMapper::mapToEntity($dto);

        $this->setPage($pageSection);
        $this->setCategory($pageSection);

        $this->repository->update($pageSection);

        return $pageSection;
    }

    public function findById($id)
    {
        return $this->repository->findOneBy(['id' => $id]);
    }

    public function findBySlug($slug)
    {
        return $this->repository->findOneBy(['slug' => $slug]);
    }

    public function deleteById($id)
    {
        return $this->repository->deleteById($id);
    }

    public function findAll(array $criteria = null, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findAllPageSections()
    {
        return $this->repository->findBy(['section_type' => SectionType::PAGE]);
    }

    public function findAllSections()
    {
        return $this->repository->findBy(['section_type' => SectionType::SECTION]);
    }

    public function findBySectionCategory( $section ) {
	    return $this->repository->findBySectionCategory($section);
    }

	private function setPage(PageSection $section)
    {
        if ($section->getPage()) {
            $section->setPage($this->pageService->findById($section->getPage()->getId()));
        }
    }

    private function setCategory(PageSection $section){
        if($section->getCategory()){
            $section->setCategory($this->categoryService->findOneBy(['id' => $section->getCategory()->getId()]));
        }
    }
}