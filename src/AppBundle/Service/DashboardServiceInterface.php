<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/31/17
 * Time: 12:09 AM
 */

namespace AppBundle\Service;


interface DashboardServiceInterface
{

    public function getRecentPost($limit);

    public function getPages();

    public function getAdvertisements($limit);

    public function getAdvertisementClients();

    public function getExpiredAdvertisements();

}