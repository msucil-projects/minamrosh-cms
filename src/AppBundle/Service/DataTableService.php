<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/12/17
 * Time: 9:41 PM
 */

namespace AppBundle\Service;

use AppBundle\Domain\DTO\SortQuery;
use AppBundle\Domain\DTO\DataTableRequestBody;
use AppBundle\Domain\Mapper\DataTableMapper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class DataTableService implements DataTableInterface
{
    private static $CONTENT_TYPE = "application/json";
    private static $JSON = "json";
    private $serializer;
    private $response;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param array $result
     * @return Response
     * @internal param array $response array keys = draw, recordsTotal, recordsFiltered, data* array keys = draw, recordsTotal, recordsFiltered, data
     */
    public function getDataTableResponse(array $result)
    {
        $dataTableResponseBody = DataTableMapper::mapToDataTableResponseBody($result);

        $this->response = new Response();
        $this->response->headers->set("content-type", self::$CONTENT_TYPE);
        $this->response->setContent($this->serializer->serialize($dataTableResponseBody, self::$JSON));

        return $this->response;
    }

    function getOrderColumnAndDirection(DataTableRequestBody $dataTableRequestBody)
    {
        $columnIndex = $dataTableRequestBody->getOrder()[0]["column"];
        if ($columnIndex == "") {
            return "";
        }
        $orderDirection = strtoupper($dataTableRequestBody->getOrder()[0]["dir"]);
        $columns = $dataTableRequestBody->getColumns();

        return new SortQuery($columns[$columnIndex]["data"], $orderDirection);
    }
}