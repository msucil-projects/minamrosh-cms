<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:29 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\MemberDTO;
use AppBundle\Domain\Mapper\MemberMapper;
use AppBundle\Repository\MemberRepository;

class MemberService implements MemberServiceInterface
{
	private $repository;

	public function __construct(MemberRepository $repository) {
		$this->repository = $repository;
	}

	public function save( MemberDTO $dto ) {
		$member = MemberMapper::mapToEntity($dto);

		return $this->repository->save($member);
	}

	public function update( MemberDTO $dto ) {
		$member = MemberMapper::mapToEntity($dto);

		return $this->repository->update($member);
	}

	public function deleteById( $id ) {
		return $this->repository->deleteById($id);
	}

	public function findOne( array $criteria ) {
		return $this->repository->findOneBy($criteria);
	}

	public function findAllBy( array $criteria, array $orderBy = null, $limit = null, $offset = null ) {
		return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
	}
}