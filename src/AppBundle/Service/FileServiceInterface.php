<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/7/17
 * Time: 12:16 PM
 */

namespace AppBundle\Service;


use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileServiceInterface
{

    public function uploadFile(UploadedFile $file);

    public function getViewFileName($fileName);

    public function deleteFile($fileName);

    public function getAbsoluteFileName($fileName);

}