<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 10:15 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\SectionCategoryDTO;
use AppBundle\Domain\Mapper\SectionCategoryMapper;
use AppBundle\Repository\SectionCategoryRepository;

class SectionCategoryService implements SectionCategoryServiceInterface
{
    private $repository;

    public function __construct(SectionCategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(SectionCategoryDTO $dto)
    {
        $category = SectionCategoryMapper::mapToEntity($dto);
        return $this->repository->save($category);
    }

    public function update(SectionCategoryDTO $dto)
    {
        $category = SectionCategoryMapper::mapToEntity($dto);
        return $this->repository->update($category);
    }

    public function findOneBy(array $criteria){
        return $this->repository->findOneBy($criteria);
    }

    public function delete($id)
    {
        return $this->repository->deleteById($id);
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function findAllBy(array $criteria){
        return $this->repository->findBy($criteria);
    }
}