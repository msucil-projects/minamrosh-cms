<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:29 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\AuthorDto;
use AppBundle\Domain\Mapper\AuthorMapper;
use AppBundle\Repository\AuthorRepository;

class AuthorService implements AuthorServiceInterface
{
	private $repository;

	public function __construct(AuthorRepository $repository) {
		$this->repository = $repository;
	}

	public function save( AuthorDto $dto ) {
		$member = AuthorMapper::mapToEntity($dto);

		return $this->repository->save($member);
	}

	public function update( AuthorDto $dto ) {
		$member = AuthorMapper::mapToEntity($dto);

		return $this->repository->update($member);
	}

	public function deleteById( $id ) {
		return $this->repository->deleteById($id);
	}

	public function findOne( array $criteria ) {
		return $this->repository->findOneBy($criteria);
	}

	public function findAllBy( array $criteria, array $orderBy = null, $limit = null, $offset = null ) {
		return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
	}
}