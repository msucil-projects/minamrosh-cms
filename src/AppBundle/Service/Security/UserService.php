<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/5/17
 * Time: 12:35 AM
 */

namespace AppBundle\Service\Security;

use AppBundle\Domain\DTO\Security\ProfileDto;
use AppBundle\Domain\DTO\Security\RegisterUser;
use AppBundle\Domain\DTO\Security\UpdateUser;
use AppBundle\Domain\Entity\Security\User;
use AppBundle\Domain\Mapper\Security\UserMapper;
use AppBundle\Repository\Security\RoleRepository;
use AppBundle\Repository\Security\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService implements UserServiceInterface
{
    private $passwordEncoder;
    private $userRepository;
    private $roleRepository;
    private $currentUserService;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        UserRepository $userRepository,
        RoleRepository $roleRepository,
        CurrentUserServiceInterface $currentUserService
    )
    {
        $this->passwordEncoder = $encoder;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->currentUserService = $currentUserService;
    }

    public function register(RegisterUser $registerUser)
    {
        $newUser = new User();
        $roleEditor = $this->roleRepository->findOneBy(['name' => 'ROLE_EDITOR']);

        $newUser->setFirstName($registerUser->getFirstName())
            ->setLastName($registerUser->getLastName())
            ->setUsername($registerUser->getUsername())
            ->setEmail($registerUser->getEmail())
            ->setPassword($this->passwordEncoder->encodePassword(
                $newUser, $registerUser->getPassword()
            ))
            ->setRole($roleEditor)
            ->setCreatedAt(new \DateTime());

        $this->userRepository->save($newUser);
    }

    public function update(UpdateUser $user)
    {
        $user = UserMapper::mapUpdateUserToUser($user);
        if($user->getPassword()){
            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        }

        return $this->userRepository->update($user);
    }

    public function changeProfile(ProfileDto $profile)
    {
        $user = $this->userRepository->findOneBy(['id' => $this->currentUserService->getUser()->getId()]);

        $user->setFirstName($profile->getFirstName())
            ->setLastName($profile->getLastName())
            ->setEmail($profile->getEmail());

        $this->userRepository->update($user);
    }

    public function deleteById($id)
    {
        $this->userRepository->deleteById($id);
    }

    public function findAll()
    {
        return $this->userRepository->findAll();
    }

	public function findOne( array $criteria ) {
		return $this->userRepository->findOneBy($criteria);
	}

    public function changePassword($newPassword, $userId)
    {
        $user = $this->userRepository->findOneBy(['id' => $userId]);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $newPassword));
        return $this->userRepository->update($user);
    }
}