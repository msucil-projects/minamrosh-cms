<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/9/17
 * Time: 9:00 PM
 */

namespace AppBundle\Service\Security;


use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class TokenGenerator implements TokenGeneratorInterface
{

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}