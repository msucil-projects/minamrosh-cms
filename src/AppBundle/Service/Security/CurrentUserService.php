<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/9/17
 * Time: 7:49 PM
 */

namespace AppBundle\Service\Security;


use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CurrentUserService implements CurrentUserServiceInterface
{
    private $security;

    public function __construct(TokenStorageInterface $token)
    {
        $this->security = $token;
    }

    public function getUser()
    {
        return $this->security->getToken()->getUser();
    }

	public function getUsername() {
		return $this->security->getToken()->getUsername();
	}

	public function getEmail() {
		return $this->security->getToken()->getUser()->getEmail();
	}

	public function getId() {
		return $this->security->getToken()->getUser()->getId();
	}
}