<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/10/17
 * Time: 3:45 PM
 */

namespace AppBundle\Service\Security;


use AppBundle\Domain\Entity\Security\RequestPassword;
use AppBundle\Domain\Entity\Security\User;
use AppBundle\Repository\Security\RequestPasswordRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class RequestPasswordService
{
    private $passwordRepository;
    private $tokenGenerator;
    private $userService;
    private $urlGenerator;

    public function __construct(
        RequestPasswordRepository $passwordRepository,
        TokenGeneratorInterface $tokenGenerator,
        UserServiceInterface $userService,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->passwordRepository = $passwordRepository;
        $this->tokenGenerator = $tokenGenerator;
        $this->userService = $userService;
        $this->urlGenerator = $urlGenerator;
    }

    public function requestPassword(User $user){
        $requestPassword = new RequestPassword();
        $requestPassword->setUserId($user->getId());
        $requestPassword->setRequestKey($this->tokenGenerator->generateToken());
        $this->passwordRepository->save($requestPassword);

        //TODO: Add Email Service to send generated url
        /*die(dump($this->urlGenerator
            ->generate('reset-password',
                ['token' =>  $requestPassword->getRequestKey()],
                UrlGeneratorInterface::ABSOLUTE_URL)
        ));*/

        return $requestPassword;
    }

    public function changePassword($newPassword, RequestPassword $requestPassword){
        $this->userService->changePassword($newPassword, $requestPassword->getUserId());
        $this->passwordRepository->remove($requestPassword);
    }

    public function findOneBy(array $criteria) {
        return $this->passwordRepository->findOneBy($criteria);
    }

}