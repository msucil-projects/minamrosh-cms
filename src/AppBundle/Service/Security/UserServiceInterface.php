<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/5/17
 * Time: 12:32 AM
 */

namespace AppBundle\Service\Security;


use AppBundle\Domain\DTO\Security\ProfileDto;
use AppBundle\Domain\DTO\Security\RegisterUser;
use AppBundle\Domain\DTO\Security\UpdateUser;

interface UserServiceInterface
{
    public function register(RegisterUser $registerUser);

    public function update(UpdateUser $user);

    public function changeProfile(ProfileDto $profile);

    public function deleteById($criteria);

    public function findAll();

    public function findOne(array $criteria);

    public function changePassword($newPassword, $userId);
}