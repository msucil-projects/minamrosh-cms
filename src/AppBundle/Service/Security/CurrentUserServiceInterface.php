<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/9/17
 * Time: 7:48 PM
 */

namespace AppBundle\Service\Security;


interface CurrentUserServiceInterface
{
    public function getUser();

    public function getUsername();

    public function getEmail();

    public function getId();
}