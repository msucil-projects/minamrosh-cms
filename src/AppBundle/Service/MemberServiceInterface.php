<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:24 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\MemberDTO;

interface MemberServiceInterface {

	public function save(MemberDTO $dto);

	public function update(MemberDTO $dto);

	public function deleteById($id);

	public function findOne(array $criteria);

	public function findAllBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}