<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/8/17
 * Time: 11:04 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\DataTableRequestBody;
use AppBundle\Domain\DTO\PostDTO;
use AppBundle\Domain\Mapper\PostMapper;
use AppBundle\Repository\PostRepository;

class PostService implements PostServiceInterface
{
    private $postRepository;
    private $dataTableService;
    private $categoryService;
    private $authorService;

    public function __construct(
        PostRepository $postRepository,
        DataTableInterface $dataTableService,
        PostCategoryServiceInterface $categoryService,
        AuthorServiceInterface $authorService
    )
    {
        $this->postRepository = $postRepository;
        $this->dataTableService = $dataTableService;
        $this->categoryService = $categoryService;
        $this->authorService = $authorService;
    }

    public function save(PostDTO $postDTO){
        $post = PostMapper::mapToEntity($postDTO);

        $post->setCategory($this->categoryService->findById($post->getCategory()->getId()));

        if($post->getAuthor()) {
            $post->setAuthor($this->authorService->findOne(['id' => $post->getAuthor()->getId()]));
        }

        return $this->postRepository->save($post);
    }

    public function update(PostDTO $postDto)
    {
        $post = PostMapper::mapToEntity($postDto);
        $post->setCategory($this->categoryService->findById($post->getCategory()->getId()));

        if($post->getAuthor()) {
            $post->setAuthor($this->authorService->findOne(['id' => $post->getAuthor()->getId()]));
        }

        return $this->postRepository->update($post);
    }

    public function findById($id)
    {
        return $this->postRepository->findOne(["id" => $id]);
    }

    public function findBySlug($slug)
    {
        return $this->postRepository->findOne(["slug" => $slug]);
    }

    public function findAll(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->postRepository->findAllBy($criteria, $orderBy, $limit, $offset);
    }

    public function deleteById($id)
    {
        $this->postRepository->deleteById($id);
    }

    public function getPosts(DataTableRequestBody $dataTableRequestBody)
    {
        $draw = $dataTableRequestBody->getDraw();
        $limit = $dataTableRequestBody->getLength();
        $offset = $dataTableRequestBody->getStart();
        $searchTerm = $dataTableRequestBody->getSearch()["value"] . '%';
        $orderDirection = $dataTableRequestBody->getOrder();

        $orderBy =$this->dataTableService->getOrderColumnAndDirection($dataTableRequestBody);
        $queryResult = $this->postRepository->findAllBySortQuery($searchTerm, $orderBy, $limit, $offset);

        $response = [
            "draw" => $draw,
            "recordsTotal" => $queryResult->getTotalResult(),
            "recordsFiltered" => $queryResult->getTotalResult(),
            "data" => $queryResult->getResult()
        ];

        return $this->dataTableService->getDataTableResponse($response);
    }
}