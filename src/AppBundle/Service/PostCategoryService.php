<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 7:57 PM
 */

namespace AppBundle\Service;


use AppBundle\Domain\DTO\DataTableRequestBody;
use AppBundle\Domain\DTO\PostCategoryDTO;
use AppBundle\Domain\Entity\PostCategory;
use AppBundle\Domain\Mapper\PostCategoryMapper;
use AppBundle\Repository\PostCategoryRepository;

class PostCategoryService implements PostCategoryServiceInterface {
	private $repository;

	public function __construct( PostCategoryRepository $repository ) {
		$this->repository = $repository;
	}

	public function save( PostCategoryDTO $categoryDTO ) {
		$category = PostCategoryMapper::mapToEntity( $categoryDTO );

		$category = $this->setParentCategory($category);

		return $this->repository->save( $category );
	}

	public function update( PostCategoryDTO $categoryDTO ) {
		$category = PostCategoryMapper::mapToEntity( $categoryDTO );

		$category = $this->setParentCategory($category);

		return $this->repository->update( $category );
	}

	public function findById( $id ) {
		return $this->repository->findOneBy( [ 'id' => $id ] );
	}

	public function findBySlug( $slug ) {
		// TODO: Implement findBySlug() method.
	}

	public function deleteById( $id ) {
		return $this->repository->deleteById( $id );
	}

	public function findAll( array $criteria = null, array $orderBy = null, $limit = null, $offset = null ) {
		return $this->repository->findAll();
	}

	public function getPostCategories( DataTableRequestBody $dataTableRequestBody ) {
		// TODO: Implement getPostCategories() method.
	}

	public function getPostMenuItems() {
		return $this->repository->findBy( [], [ 'displayOrder' => 'ASC', 'name' => 'ASC' ] );
	}

	private function setParentCategory( PostCategory $category ) {
		if ( $category->getParent() ) {
			$category->setParent( $this->repository->find( $category->getParent()->getId() ) );
		}

		return $category;
	}
}