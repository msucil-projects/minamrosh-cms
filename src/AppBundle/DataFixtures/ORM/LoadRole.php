<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/3/17
 * Time: 10:48 PM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Domain\Entity\Security\Role;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRole extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $editor = new Role();
        $editor->setName("ROLE_EDITOR");

        $admin = new Role();
        $admin->setName("ROLE_ADMIN");
        $admin->setChildRole($editor);

        $manager->persist($editor);
        $manager->persist($admin);

        $manager->flush();

        $this->addReference('admin-role', $admin);
        $this->addReference('editor-role', $editor);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}