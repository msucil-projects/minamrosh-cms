<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/16/17
 * Time: 9:48 PM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Domain\Entity\PostCategory;
use AppBundle\Util\StringHelper;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPostCategories extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
	    $agriculture = new PostCategory();
	    $agriculture->setName("Agriculture")
	             ->setSlug( StringHelper::generateSlug( $agriculture->getName() ) )
	             ->setEditable( false )
	             ->setParent( null );

	    $technology = new PostCategory();
	    $technology->setName("Technology")
	                ->setSlug( StringHelper::generateSlug( $technology->getName() ) )
	                ->setEditable( false )
	                ->setParent( null );

	    $lifestyle = new PostCategory();
	    $lifestyle->setName("Lifestyle")
	                ->setSlug( StringHelper::generateSlug( $lifestyle->getName() ) )
	                ->setEditable( false )
	                ->setParent( null );

	    $international = new PostCategory();
	    $international->setName("International")
	                ->setSlug( StringHelper::generateSlug( $international->getName() ) )
	                ->setEditable( false )
	                ->setParent( null );

	    $sport = new PostCategory();
	    $sport->setName("Sport")
	                ->setSlug( StringHelper::generateSlug( $sport->getName() ) )
	                ->setEditable( false )
	                ->setParent( null );

	    $manager->persist($agriculture);
	    $manager->persist($technology);
	    $manager->persist($lifestyle);
	    $manager->persist($international);
	    $manager->persist($sport);
	    $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}