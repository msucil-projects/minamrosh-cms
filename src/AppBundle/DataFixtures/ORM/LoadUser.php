<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/2/17
 * Time: 9:52 PM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Domain\Entity\Security\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUser extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $passwordEncoder = $this->container->get('security.password_encoder');

        $admin = new User();
        $adminPassword = $passwordEncoder->encodePassword($admin, "password");

        $admin->setUsername("admin")
            ->setEmail("np.msushil@gmail.com")
            ->setPassword($adminPassword)
            ->setFirstName("Sushil")
            ->setRole($this->getReference('admin-role'));

        $manager->persist($admin);

        $editor = new User();
        $editorPassword = $passwordEncoder->encodePassword($editor, "password");
        $editor->setUsername("editor")
            ->setEmail("magar.sushilale@gmail.com")
            ->setPassword($editorPassword)
            ->setFirstName("Sushil")
            ->setRole($this->getReference('editor-role'));
        $manager->persist($editor);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}