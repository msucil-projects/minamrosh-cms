<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/12/17
 * Time: 12:43 AM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Domain\Entity\SectionCategory;
use AppBundle\Util\SectionType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Stopwatch\Section;

class LoadSectionCategories extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $aboutUs = new SectionCategory();
        $aboutUs->setSection(SectionType::PAGE)
            ->setName('About Us')
            ->setDescription('About Us Page')
            ->setEditable(false);

        $contactUs = new SectionCategory();
        $contactUs->setSection(SectionType::PAGE)
            ->setName('Contact Us')
            ->setDescription('Contact Us Page')
            ->setEditable(false);

        $infoSection = new SectionCategory();
	    $infoSection->setSection(SectionType::SECTION)
	              ->setName('Info Section')
	              ->setDescription('Short info section displayed in the footer bar')
	              ->setEditable(false);

        $manager->persist($aboutUs);
        $manager->persist($contactUs);
        $manager->persist($infoSection);
        $manager->flush();

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}