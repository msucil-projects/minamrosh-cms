<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 7/5/17
 * Time: 9:59 PM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Domain\Entity\Advertisement\AdSection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAdSections extends AbstractFixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /*$headerAdSection = new AdSection();
        $headerAdSection->setName('Header Ad')
            ->setEditable(false)
            ->setDescription('Full Width Header Ad Section')
            ->setHeight(100)
            ->setWidth(728);

        $topAdSection = new AdSection();
        $topAdSection->setName('Top Content Ad')
            ->setEditable(false)
            ->setDescription('Full Width Top Content Ad displayed above main content area')
            ->setHeight(100)
            ->setWidth(728);

        $bottomAdSection = new AdSection();
        $bottomAdSection->setName('Bottom Content Ad')
            ->setEditable(false)
            ->setDescription('Full Width Bottom Content Ad')
            ->setHeight(100)
            ->setWidth(728);

        $homepageAdSection = new AdSection();
        $homepageAdSection->setName('Homepage Ad')
            ->setEditable(false)
            ->setDescription('Full Width Homepage Ad, displayed after each category')
            ->setHeight(100)
            ->setWidth(728);

        $categoryAdSection = new AdSection();
        $categoryAdSection->setName('Category Ad')
            ->setEditable(false)
            ->setDescription('Full Width Category Ad, displayed in each news category page in home page')
            ->setHeight(100)
            ->setWidth(728);

        $topSidebarAdSection = new AdSection();
        $topSidebarAdSection->setName('Top Sidebar Ad')
            ->setEditable(false)
            ->setDescription('300X250 Top Sidebar Ad')
            ->setHeight(250)
            ->setWidth(300);

        $bottomSidebarAdSection = new AdSection();
        $bottomSidebarAdSection->setName('Bottom Sidebar Ad')
            ->setEditable(false)
            ->setDescription('300X250 Bottom Sidebar Ad')
            ->setHeight(250)
            ->setWidth(300);

        $manager->persist($headerAdSection);
        $manager->persist($topAdSection);
        $manager->persist($bottomAdSection);
        $manager->persist($homepageAdSection);
        $manager->persist($categoryAdSection);
        $manager->persist($topSidebarAdSection);
        $manager->persist($bottomSidebarAdSection);

        $manager->flush();*/
    }
}