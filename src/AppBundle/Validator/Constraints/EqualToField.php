<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 11:16 PM
 */

namespace AppBundle\Validator\Constraints;


use Doctrine\Common\Annotations\Annotation\Target;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

/**
 * Class EqualToField
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class EqualToField extends Constraint
{
    const NOT_EQUAL_FIELD = "002";
    protected static $errorNames = array(
        self::NOT_EQUAL_FIELD => 'NOT_EQUAL_FIELD',
    );
    public $message = 'This field is not equal to another field';
    public $field;

    public function __construct($options = null)
    {
        if (!isset($options['field'])) {
            throw new ConstraintDefinitionException(sprintf(
                'The %s constraint requires the "field" option to be set.',
                get_class($this)
            ));
        }

        parent::__construct($options);
    }

    public function getDefaultOption()
    {
        return "field";
    }
}