<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 11:28 PM
 */

namespace AppBundle\Validator\Constraints;


use AppBundle\Util\StringHelper;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class EqualToFieldValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $object = $this->context->getObject();

        $compareValue = $object->{"get" . StringHelper::upperFirst($constraint->field)}();

        if($value !== $compareValue){
            $this->context->buildViolation($constraint->message)
                ->setCode(UniqueProperty::NOT_UNIQUE_PROPERTY_ERROR)
                ->addViolation();
        }
    }
}