<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 9:55 PM
 */

namespace AppBundle\Validator\Constraints;


use Doctrine\Common\Annotations\Annotation\Target;
use Symfony\Component\Validator\Constraint;

/**
 * Class UniqueProperty
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION", "CLASS"})
 */
class UniqueProperty extends Constraint
{
    const NOT_UNIQUE_PROPERTY_ERROR = '001';

    public $message = "This value is already used.";
    public $service = 'doctrine.orm.validator.unique';
    public $em = null;
    public $entityClass = null;
    public $repositoryMethod = 'findBy';
    public $property;
    public $errorPath = null;

    protected static $errorNames = array(
        self::NOT_UNIQUE_PROPERTY_ERROR => 'NOT_UNIQUE_PROPERTY_ERROR',
    );

    public function getRequiredOptions()
    {
        return array("entityClass", "property", "repositoryMethod");
    }

    public function getDefaultOption()
    {
        return array("entityClass", "property", "repositoryMethod");
    }
}