<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 9:48 PM
 */

namespace AppBundle\Validator\Constraints;


use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniquePropertyValidator extends ConstraintValidator {
    private $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    public function validate($propertyValue, Constraint $constraint) {

        if(!$constraint instanceof UniqueProperty){
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\UniqueProperty');
        }

        if ($constraint->em) {
            $em = $this->registry->getManager($constraint->em);

            if (!$em) {
                throw new ConstraintDefinitionException(sprintf('Object manager "%s" does not exist.', $constraint->em));
            }
        } else {
            $em = $this->registry->getManagerForClass($constraint->entityClass);

            if (!$em) {
                throw new ConstraintDefinitionException(sprintf('Unable to find the object manager associated with an entity of class "%s".', $constraint->entityClass));
            }
        }

        if (null !== $constraint->entityClass) {
            /* Retrieve repository from given entity name.
             * We ensure the retrieved repository can handle the entity
             * by checking the entity is the same, or subclass of the supported entity.
             */
            $repository = $em->getRepository($constraint->entityClass);
        } else {
            throw new InvalidArgumentException();
        }

        $object = $this->context->getObject();
        $result = $repository->{$constraint->repositoryMethod}([$constraint->property => $propertyValue]);

        if($result && ($object->getId() == $result->getId())){
            return true;
        }
        else if($result) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $propertyValue)
                ->setInvalidValue($propertyValue)
                ->setCode(UniqueProperty::NOT_UNIQUE_PROPERTY_ERROR)
                ->addViolation();
        }

    }
}