<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 8/11/2017
 * Time: 11:33 PM
 */

namespace AppBundle\Twig;


use AppBundle\Service\Frontend\FrontPostServiceInterface;

class HomePageExtension extends \Twig_Extension {

	private $frontPostservice;

	public function __construct( FrontPostServiceInterface $frontPostService ) {
		$this->frontPostservice = $frontPostService;
	}

	public function getFunctions() {
		return [
			new \Twig_SimpleFunction( 'homepage_slider',
				[ $this, 'getHomepageSlider' ],
				[ 'is_safe' => [ 'html' ], 'needs_environment' => true ] ),
			new \Twig_SimpleFunction( 'homepage_posts',
				[ $this, 'getHomepagePosts' ],
				[ 'is_safe' => [ 'html' ], 'needs_environment' => true ] )
		];
	}

	public function getHomepageSlider( \Twig_Environment $environment ) {
		return $environment->render( ':layout/public/partials:home-slider.html.twig',
			[
				'slider1' => $this->frontPostservice->getFeaturedPost( 3, 0 ),
				'slider2' => $this->frontPostservice->getFeaturedPost( 3, 3 ),
				'slider3' => $this->frontPostservice->getFeaturedPost( 3, 6 )
			] );
	}

	public function getHomepagePosts( \Twig_Environment $environment, $category, $limit, $templateType ) {

		if ( strcmp( $templateType, 'top-double-column' ) == 0 ) {
			return $environment->render( ':layout/public/partials/homepage:top-double-column.html.twig', [ 'posts' => $this->frontPostservice->getPostsByCategory( $category, $limit ) ] );
		}

		if ( strcmp( $templateType, 'top-single-column' ) == 0 ) {
			return $environment->render( ':layout/public/partials/homepage:top-single-column.html.twig', [ 'posts' => $this->frontPostservice->getPostsByCategory( $category, $limit ) ] );
		}

		if ( strcmp( $templateType, 'highlight' ) == 0 ) {
			return $environment->render( ':layout/public/partials/homepage:highlight.html.twig',
				[
					'posts'    => $this->frontPostservice->getPostsByCategory( $category, $limit ),
					'category' => $category
				] );
		}

		if ( strcmp( $templateType, 'normal' ) == 0 ) {
			return $environment->render( ':layout/public/partials/homepage:normal.html.twig',
				[
					'posts'    => $this->frontPostservice->getPostsByCategory( $category, $limit ),
					'category' => $category
				] );
		}

		if ( strcmp( $templateType, 'double-column' ) == 0 ) {
			return $environment->render( ':layout/public/partials/homepage:double-column.html.twig',
				[
					'posts'    => $this->frontPostservice->getPostsByCategory( $category, $limit ),
					'category' => $category
				] );
		}
	}
}