<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/27/17
 * Time: 11:11 PM
 */

namespace AppBundle\Twig;


use AppBundle\Util\StringHelper;

class StringExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(new \Twig_SimpleFunction('truncate_words', [$this, 'truncateWords']));
    }

    public function truncateWords($string, $count, $suffix = '[...]'){
        return StringHelper::truncateWords(strip_tags($string), $count, $suffix);
    }
}