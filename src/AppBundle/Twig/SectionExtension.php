<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 8/17/2017
 * Time: 11:44 PM
 */

namespace AppBundle\Twig;

use AppBundle\Service\PageSectionServiceInterface;

class SectionExtension extends \Twig_Extension {

	private $sectionService;

	public function __construct(PageSectionServiceInterface $sectionService)
	{
		$this->sectionService = $sectionService;
	}

	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('page_section',
				[$this, 'getPageSection'],
				['is_safe' => ['html'], 'needs_environment' => true])
		];
	}

	public function getPageSection(\Twig_Environment $environment, $section)
	{
		return $this->sectionService->findBySectionCategory($section);
	}

}