<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/28/17
 * Time: 8:36 PM
 */

namespace AppBundle\Twig;


class PaginationExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('paginate',
                [$this, 'getPagination'],
                ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('pager',
                [$this, 'getPager'],
                ['is_safe' => ['html'], 'needs_environment' => true])

        ];
    }

    public function getPagination(\Twig_Environment $environment, $pagination)
    {
        return $environment->render(':twig/pagination:pagination.html.twig', ['pagination' => $pagination]);
    }

    public function getPager(\Twig_Environment $environment, $pagination){
        return $environment->render(':twig/pagination:pager.html.twig', ['pagination' => $pagination]);
    }
}