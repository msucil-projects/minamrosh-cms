<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 7/10/2017
 * Time: 7:58 PM
 */

namespace AppBundle\Twig;


use AppBundle\Service\SiteServiceInterface;

class SiteExtension extends \Twig_Extension {
	private $site;

	public function getFunctions(){
		return [
			new \Twig_SimpleFunction('site_name', [$this, 'getSiteName']),
			new \Twig_SimpleFunction('site_title', [$this, 'getSiteTitle']),
			new \Twig_SimpleFunction('site_banner', [$this, 'getSiteBanner']),
			new \Twig_SimpleFunction('site_favicon', [$this, 'getSiteFavIcon']),
			new \Twig_SimpleFunction('site_address', [$this, 'getAddress']),
			new \Twig_SimpleFunction('site_logo', [$this, 'getSiteLogo']),
			new \Twig_SimpleFunction('site_twitter', [$this, 'getTwitterLink']),
			new \Twig_SimpleFunction('site_facebook', [$this, 'getFacebookLink']),
			new \Twig_SimpleFunction('site_googleplus', [$this, 'getGooglePlusLink']),
			new \Twig_SimpleFunction('site_googleanlaytic_script', [$this, 'getGoogleAnalyticScript']),
			new \Twig_SimpleFunction('og_tags', [$this, 'generateOgTags'], ['is_safe' => ['html'], 'needs_environment' => true])
		];
	}

	public function __construct(SiteServiceInterface $service) {
		$this->site = $service->findOne();
	}


	public function getSiteName(){
		return $this->site ? $this->site->getName() : null;
	}

	public function getSiteTitle($title = null) {
		$siteTitle = ($this->site->getMeta() && $this->site->getMeta()->getTitle() && !empty($this->site->getMeta()->getTitle())) ? $this->site->getMeta()->getTitle() : $this->site->getName();

		return ($title) ? $title . ' | ' . $siteTitle : $siteTitle;
	}

	public function getSiteBanner() {
		return ($this->site->getBanner()) ? $this->site->getBanner()->getName() : null;
	}

	public function getSiteFavIcon() {
		return ($this->site->getLogo()) ? $this->site->getLogo()->getName() : null;
	}

	public function getSiteLogo() {
		return $this->getSiteFavIcon();
	}

	public function getFacebookLink() {
		return ($this->site->getFacebookLink()) ? $this->site->getFacebookLink() : null;
	}

	public function getTwitterLink() {
		return ($this->site->getTwitterLink()) ? $this->site->getTwitterLink() : null;
	}

	public function getGooglePlusLink() {
		return ($this->site->getGooglePlusLink()) ? $this->site->getGooglePlusLink() : null;
	}

	public function getAddress() {
		return $this->site->getAddress();
	}

	public function generateOgTags(\Twig_Environment $env, $obj = null, $url = null){

		$meta = [];
		$meta['type'] = 'website';
		$meta['title'] = '';
		$meta['url'] = $url;
		$meta['image'] = '';
		$meta['description'] = '';
		$meta['siteName'] = $this->site->getName();
		$meta['tags'] = '';

		if($obj) {
			if($obj->getMeta()){
				$meta['type'] = 'article';
				$meta['title'] = $obj->getMeta()->getTitle();
				$meta['tags'] = $obj->getMeta()->getKeywords();
				$meta['description'] = $obj->getMeta()->getDescription();
			}

			if($obj->getFeatureImage()) {
				$meta['image'] = $obj->getFeatureImage()->getName();
			}
			else {
				$meta['image'] = $this->getSiteBanner();
			}
		}
		else {
			$obj = $this->site;
			if($obj->getMeta()){
				$meta['title'] = $obj->getMeta()->getTitle();
				$meta['tags'] = $obj->getMeta()->getKeywords();
				$meta['description'] = $obj->getMeta()->getDescription();
			}

			if($obj->getBanner()){
				$meta['image'] = $obj->getBanner()->getName();
			}
		}

		return $env->render(':layout/public/partials/meta:og-website.html.twig', ['meta' => $meta]);
	}

	public function getGoogleAnalyticScript() {
		return $this->site->getGoogleAnalyticScript();
	}

}