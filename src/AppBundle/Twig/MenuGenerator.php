<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/15/17
 * Time: 9:45 PM
 */

namespace AppBundle\Twig;


use AppBundle\Service\PostCategoryServiceInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MenuGenerator {
	private $categoryService;
	private $urlGenerator;

	public function __construct( PostCategoryServiceInterface $categoryService, UrlGeneratorInterface $urlGenerator ) {
		$this->categoryService = $categoryService;
		$this->urlGenerator    = $urlGenerator;
	}

	public function generateMainMenu() {
		$categories = $this->categoryService->getPostMenuItems();


		foreach ( $categories as $category ) {
			if ( ! $category->getParent() ) {

				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li class='dropdown menu-color1'>
                            <a href='{$this->getPostUrl($category->getSlug())}' class='dropdown-toggle'
                               data-toggle='dropdown' role='button'
                               aria-expanded='false'>{$category->getName()}</a>";

					$this->generateMainChildMenu( $category->getChildren(), true );
				} else {
					echo "<li>";
					echo "<a href='{$this->getPostUrl($category->getSlug())}'>{$category->getName()}</a>";
				}
				echo "</li>";
			}
		}
	}

	private function getPostUrl( $postCategorySlug ) {
		return $this->urlGenerator->generate( 'post-category', [ 'category' => $postCategorySlug ] );
	}

	private function generateMainChildMenu( $children, $requireRole = false ) {
		if ( $children ):
			echo ( $requireRole ) ? "<ul class='dropdown-menu' role='menu'>" : "<ul class='dropdown-menu'>";
			foreach ( $children as $category ) {
				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li class='dropdown-submenu'>
                            <a href='{$this->getPostUrl($category->getSlug())}'>{$category->getName()}</a>";
					$this->generateMainChildMenu( $category->getChildren(), false );
				} else {
					echo "<li>";
					echo "<a href='{$this->getPostUrl($category->getSlug())}'>{$category->getName()}</a>";
				}
				echo "</li>";
			}

			echo "</ul>";
		endif;
	}

	public function generateMobileMenu() {
		$categories = $this->categoryService->getPostMenuItems();


		foreach ( $categories as $category ) {
			if ( ! $category->getParent() ) {

				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li><a href='{$this->getPostUrl($category->getSlug())}'>{$category->getName()}</a>";
					$this->generateMobileChildMenu( $category->getChildren() );
				} else {
					echo "<li>";
					echo "<a href='{$this->getPostUrl($category->getSlug())}'>{$category->getName()}</a>";
				}
				echo "</li>";
			}
		}
	}

	private function generateMobileChildMenu( $children ) {
		if ( $children ):
			echo "<ul>";

			foreach ( $children as $category ) {
				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li><a href='{$this->getPostUrl($category->getSlug())}'>{$category->getName()}</a>";
					$this->generateMobileChildMenu( $category->getChildren() );
				} else {
					echo "<li>";
					echo "<a href='{$this->getPostUrl($category->getSlug())}'>{$category->getName()}</a>";
				}
				echo "</li>";
			}

			echo "</ul>";
		endif;
	}

}