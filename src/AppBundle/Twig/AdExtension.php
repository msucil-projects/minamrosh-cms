<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 7/5/17
 * Time: 10:51 PM
 */

namespace AppBundle\Twig;


use AppBundle\Service\Advertisement\ActiveAdInterface;

class AdExtension extends \Twig_Extension
{
    private $adService;

    public function __construct(ActiveAdInterface $adService)
    {
        $this->adService = $adService;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('full_width_ad',
                [$this, 'getFullWidthAd'],
                ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('sidebar_ad',
                [$this, 'getSidebarAd'],
                ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('content_ad',
                [$this, 'getContentAd'],
                ['is_safe' => ['html'], 'needs_environment' => true])

        ];
    }


    public function getFullWidthAd(\Twig_Environment $environment, $adSection, $limit = null, $offset = null){
        $ads = $this->adService->getActiveAds($adSection, $limit, $offset);

        return $environment->render(':layout/public/partials/ad:ad-full-width.html.twig',['ads' => $ads]);
    }

    public function getSidebarAd(\Twig_Environment $environment, $adSection, $limit = null, $offset = null){
        $ads = $this->adService->getActiveAds($adSection, $limit, $offset);

        return $environment->render(':layout/public/partials/ad:ad-widget.html.twig', ['ads' => $ads]);
    }

    public function getContentAd(\Twig_Environment $environment, $adSection, $limit = null, $offset = null){
        $ads = $this->adService->getActiveAds($adSection, $limit, $offset);

        return $environment->render(':layout/public/partials/ad:ad-mid-widget.html.twig', ['ads' => $ads]);
    }
}