<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/29/17
 * Time: 12:37 AM
 */

namespace AppBundle\Twig;


class Pagination
{
    private $totalRecords;

    private $currentPage;

    private $pageSize;

    private $parameter;

    private $routeName;

    private $totalPages;

    public function __construct($routeName, array $parameter, $currentPage, $pageSize, $totalRecords)
    {
        $this->routeName = $routeName;
        $this->parameter = $parameter;
        $this->currentPage = $currentPage;
        $this->pageSize = $pageSize;
        $this->totalRecords = $totalRecords;

        $this->totalPages = $totalRecords / $pageSize;
        if(is_float($this->totalPages)){
            $this->totalPages = intval($this->totalPages) + 1;
        }

    }

    /**
     * @return mixed
     */
    public function getTotalRecords()
    {
        return $this->totalRecords;
    }

    /**
     * @return mixed
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @return mixed
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @return mixed
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @return mixed
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }
}