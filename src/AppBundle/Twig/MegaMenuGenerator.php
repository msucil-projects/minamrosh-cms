<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 8/13/2017
 * Time: 5:46 PM
 */

namespace AppBundle\Twig;


use AppBundle\Service\Frontend\FrontPostServiceInterface;
use AppBundle\Service\PostCategoryServiceInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MegaMenuGenerator {
	private $categoryService;
	private $urlGenerator;
	private $frontPostService;
	private $mediaPath;

	public function __construct( PostCategoryServiceInterface $categoryService, UrlGeneratorInterface $urlGenerator, FrontPostServiceInterface $frontPostService, $mediaPath ) {
		$this->categoryService = $categoryService;
		$this->urlGenerator    = $urlGenerator;
		$this->frontPostService = $frontPostService;
		$this->mediaPath = $mediaPath;
	}

	public function generateMainMenu() {
		$categories = $this->categoryService->getPostMenuItems();


		foreach ( $categories as $category ) {
			if ( ! $category->getParent() ) {

				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li class='dropdown menu-color1'>
                            <a href='{$this->getCategoryUrl($category->getSlug())}' class='dropdown-toggle'
                               data-toggle='dropdown' role='button'
                               aria-expanded='false'>{$category->getName()}</a>";

					$this->generateMainChildMenu( $category->getChildren(), true );
				} else {
					echo "<li class='dropdown mega-full menu-color3'>";
					echo "<a href='{$this->getCategoryUrl($category->getSlug())}' class='dropdown-toggle' role='button' aria-expanded='false'>
						{$category->getName()}
						</a>";
					$this->generateFeaturedPost($category, 5);
				}
				echo "</li>";
			}
		}
	}

	private function getCategoryUrl( $postCategorySlug ) {
		return $this->urlGenerator->generate( 'post-category', [ 'category' => $postCategorySlug ] );
	}

	private function getPostUrl($post) {
		return $this->urlGenerator->generate('post-detail', ['category' => $post->getCategory()->getSlug(), 'slug' => $post->getSlug()]);
	}

	private function getMediaPath($filename) {
		return '/web/' . $this->mediaPath . $filename;
	}

	private function generateFeaturedPost($category, $limit) {
		$posts = $this->frontPostService->getFeaturedPostByCategory($category->getSlug(), $limit);
		if($posts):
			echo "<ul class='dropdown-menu fullwidth animated fadeIn'>";
				echo "<li>";
					echo "<div class='mega-menu-5block'>";
						foreach ($posts as $post):
							echo '<div class="mega-menu-news">';
								if($post->getFeatureImage() && $post->getFeatureImage()->getName()):
                                echo '<div class="mega-menu-img">';
                                    echo "<a href='{$this->getPostUrl($post)}'>";
                                        echo "<img class='entry-thumb' src='{$this->getMediaPath($post->getFeatureImage()->getName())}' />";
                                    echo '</a>';
                                echo '</div>';
                                endif;
                                echo '<div class="mega-menu-detail">';
                                    echo '<h4 class="entry-title">';
                                        echo "<a href='{$this->getPostUrl($post)}'>{$post->getTitle()}</a>";
                                    echo '</h4>';
                                echo '</div>';
                            echo '</div>';
						endforeach;
					echo "</div>";
				echo "</li>";
			echo "</ul>";
		endif;
	}

	private function generateMainChildMenu( $children, $requireRole = false ) {
		if ( $children ):
			echo ( $requireRole ) ? "<ul class='dropdown-menu' role='menu'>" : "<ul class='dropdown-menu'>";
			foreach ( $children as $category ) {
				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li class='dropdown-submenu'>
                            <a href='{$this->getCategoryUrl($category->getSlug())}'>{$category->getName()}</a>";
					$this->generateMainChildMenu( $category->getChildren(), false );
				} else {
					echo "<li>";
					echo "<a href='{$this->getCategoryUrl($category->getSlug())}'>{$category->getName()}</a>";
				}
				echo "</li>";
			}

			echo "</ul>";
		endif;
	}

	public function generateMobileMenu() {
		$categories = $this->categoryService->getPostMenuItems();


		foreach ( $categories as $category ) {
			if ( ! $category->getParent() ) {

				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li><a href='{$this->getCategoryUrl($category->getSlug())}'>{$category->getName()}</a>";
					$this->generateMobileChildMenu( $category->getChildren() );
				} else {
					echo "<li>";
					echo "<a href='{$this->getCategoryUrl($category->getSlug())}'>{$category->getName()}</a>";
				}
				echo "</li>";
			}
		}
	}

	private function generateMobileChildMenu( $children ) {
		if ( $children ):
			echo "<ul>";

			foreach ( $children as $category ) {
				if ( count( $category->getChildren() ) > 0 ) {
					echo "<li><a href='{$this->getCategoryUrl($category->getSlug())}'>{$category->getName()}</a>";
					$this->generateMobileChildMenu( $category->getChildren() );
				} else {
					echo "<li>";
					echo "<a href='{$this->getCategoryUrl($category->getSlug())}'>{$category->getName()}</a>";
				}
				echo "</li>";
			}

			echo "</ul>";
		endif;
	}

}