<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/29/17
 * Time: 10:28 PM
 */

namespace AppBundle\Twig;


use AppBundle\Domain\PropertyOption\Status;
use AppBundle\Service\Frontend\FrontPostService;

class PostExtension extends \Twig_Extension
{
    private $postService;

    public function __construct(FrontPostService $postService)
    {
        $this->postService = $postService;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('recent_posts',
                [$this, 'getRecentPosts'],
                ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('popular_posts',
                [$this, 'getPopularPosts'],
                ['is_safe' => ['html'], 'needs_environment' => true])

        ];
    }

    public function getRecentPosts(\Twig_Environment $environment, $limit)
    {
        $posts = $this->postService->getPosts(['status' => Status::PUBLISH], ['createdAt' => 'desc'], $limit);
        return $environment->render(':layout/public/partials/widget:recent-post.html.twig', ['posts' => $posts]);
    }

    public function getPopularPosts(\Twig_Environment $environment, $limit){
        $posts = $this->postService->getPopularPosts($limit);
        return $environment->render(':layout/public/partials/widget:recent-post.html.twig', ['posts' => $posts]);
    }
}