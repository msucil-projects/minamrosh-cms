<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170521054246 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('page_section');
        $table->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $table->addColumn('title', StringType::STRING)->setNotnull(true);
        $table->addColumn('slug', StringType::STRING)->setNotnull(true);
        $table->addColumn('content', TextType::TEXT)->setNotnull(true);
        $table->addColumn('status', IntegerType::INTEGER)->setNotnull(true);
        $table->addColumn('media_id', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('section_type', BooleanType::BOOLEAN)->setNotnull(true);
        $table->addColumn('page_id', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('category_id', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);
        $table->addColumn('created_at', DateType::DATETIME)->setNotnull(true);
        $table->addColumn('created_by', BigIntType::BIGINT)->setNotnull(true);
        $table->addColumn('updated_at', DateType::DATETIME)->setNotnull(false);
        $table->addColumn('updated_by', BigIntType::BIGINT)->setNotnull(false);

        $table->setPrimaryKey(['id']);
        $table->addIndex(['slug', 'title'], 'idx_page_section_slug_title');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
