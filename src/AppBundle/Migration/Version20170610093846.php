<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170610093846 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
       $table = $schema->createTable('request_password');
       $table->addColumn('request_key', StringType::STRING)->setNotnull(true);
       $table->addColumn('user_id', BigIntType::BIGINT)->setNotnull(true);
       $table->addColumn('created_at', DateType::DATETIME)->setNotnull(true);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('request_password');
    }
}
