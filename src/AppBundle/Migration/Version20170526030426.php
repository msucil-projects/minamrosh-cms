<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170526030426 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('ad');
        $table->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $table->addColumn('media_id', BigIntType::BIGINT)->setNotnull(true);
        $table->addColumn('status', IntegerType::INTEGER)->setNotnull(true);
        $table->addColumn('ads_client_id', BigIntType::BIGINT)->setNotnull(true);
        $table->addColumn('ads_section_id', BigIntType::BIGINT)->setNotnull(true);
        $table->addColumn('expired', BooleanType::BOOLEAN)->setNotnull(true);
        $table->addColumn('description', StringType::STRING)->setNotnull(false);
        $table->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);
        $table->addColumn('created_at', DateType::DATETIME)->setNotnull(true);
        $table->addColumn('created_by', BigIntType::BIGINT)->setNotnull(true);
        $table->addColumn('updated_by', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('updated_at', DateType::DATETIME)->setNotnull(false);
        $table->addColumn('valid_from', DateType::DATE)->setNotnull(true);
        $table->addColumn('valid_to', DateType::DATE)->setNotnull(true);

        $table->setPrimaryKey(['id']);
        $table->addForeignKeyConstraint("media", ['media_id'], ['id'], [], 'fk_ads_media_id');
        $table->addForeignKeyConstraint("ads_client", ['ads_client_id'], ['id'], [],  'fk_ads_ads_client_id');
        $table->addForeignKeyConstraint("ads_section", ['ads_section_id'], ['id'], [], 'fk_ads_section_id');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
