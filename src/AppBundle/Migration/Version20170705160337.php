<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170705160337 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('ads_section');
        $table->addColumn('slug', StringType::STRING)->setNotnull(true);
        $table->addColumn('editable', BooleanType::BOOLEAN)->setNotnull(true)->setDefault(true);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('ads_section');
        $table->dropColumn('slug');
        $table->dropColumn('editable');
    }
}
