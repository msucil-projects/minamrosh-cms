<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170500464617 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable("role");
        $table->addColumn("id", BigIntType::BIGINT)->setAutoincrement(true);
        $table->addColumn("name", StringType::STRING)->setNotnull(false);
        $table->addColumn("role_id", BigIntType::BIGINT)->setNotnull(false)->setDefault(null);
        $table->addColumn("version", IntegerType::INTEGER)->setNotnull(false)->setDefault(0);

        $table->setPrimaryKey(["id"]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
