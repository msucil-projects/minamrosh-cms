<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\DateType;


class Version20170627144859 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('post_view');

        $table->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $table->addColumn('post_id', BigIntType::BIGINT)->setNotnull(true);
        $table->addColumn('view_count', BigIntType::BIGINT)->setNotnull(true)->setDefault(1);
        $table->addColumn('view_date', DateType::DATE)->setNotnull(true);

        $table->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('post_view');
    }
}
