<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BooleanType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170605180218 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $schema->getTable('post')->addColumn('feature_post', BooleanType::BOOLEAN)->setNotnull(true)->setDefault(false);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->getTable('post')->dropColumn('feature_post');
    }
}
