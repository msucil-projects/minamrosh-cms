<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170408065622 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema
            ->createTable('post');

        $table->addColumn("id", BigIntType::BIGINT, ['autoIncrement' => true]);

        $table
            ->addColumn("title", StringType::STRING);

        $table
            ->addColumn("slug", StringType::STRING);

        $table
            ->addColumn("content", TextType::TEXT)->setNotnull(false);

        $table
            ->addColumn("status", IntegerType::INTEGER)
            ->setNotnull(true);

        $table->addColumn('version', IntegerType::INTEGER)->setDefault(0)->setNotnull(false);

        $table
            ->addColumn("created_at", DateType::DATETIME);

        $table
            ->addColumn("updated_at", DateType::DATETIME)
            ->setNotnull(false);

        $table
            ->addColumn("created_by", BigIntType::BIGINT);

        $table
            ->addColumn("updated_by", BigIntType::BIGINT)
            ->setNotnull(false);

        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(["title"], "udx_post_title");
        $table->addUniqueIndex(["slug"], "udx_post_slug");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable("post");
    }
}
