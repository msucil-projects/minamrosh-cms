<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170531165428 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('site');
        $table->addColumn('id', IntegerType::INTEGER)->setAutoincrement(true);
        $table->addColumn('name', StringType::STRING)->setNotnull(true);
        $table->addColumn('meta_id', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('banner', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('logo', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('phone_no', StringType::STRING)->setNotnull(false);
        $table->addColumn('mobile_no', StringType::STRING)->setNotnull(false);
        $table->addColumn('fax_no', StringType::STRING)->setNotnull(false);
        $table->addColumn('email', StringType::STRING)->setNotnull(true);
        $table->addColumn('facebook_link', StringType::STRING)->setNotnull(false);
        $table->addColumn('twitter_link', StringType::STRING)->setNotnull(false);
        $table->addColumn('google_plus_link', StringType::STRING)->setNotnull(false);
	    $table->addColumn('linkedin_link', StringType::STRING)->setNotnull(false);
	    $table->addColumn('google_analytic_script', StringType::STRING)->setNotnull(false);
	    $table->addColumn('google_analytic_link', StringType::STRING)->setNotnull(false);
	    $table->addColumn('address', StringType::STRING)->setNotnull(false);
	    $table->addColumn('enabled', BooleanType::BOOLEAN)->setNotnull(true);
	    $table->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(true);

	    $table->setPrimaryKey(['id']);
	    $table->addUniqueIndex(['name'], 'udx_site_name');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('site');
    }
}
