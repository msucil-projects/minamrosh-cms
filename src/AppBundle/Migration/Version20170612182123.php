<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\IntegerType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170612182123 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $schema->getTable('post_category')
            ->addColumn('display_order', IntegerType::INTEGER)
            ->setNotnull(true)->setDefault(0);

        $schema->getTable('section_category')
            ->addColumn('display_order', IntegerType::INTEGER)
            ->setNotnull(true)->setDefault(0);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
