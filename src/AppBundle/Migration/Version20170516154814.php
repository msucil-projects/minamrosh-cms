<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170516154814 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $category = $schema->createTable('post_category');
        $category->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $category->addColumn('name', StringType::STRING)->setNotnull(true);
        $category->addColumn('slug', StringType::STRING)->setNotnull(true);
        $category->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);
        $category->addColumn('editable', BooleanType::BOOLEAN)->setDefault(true)->setNotnull(false);
        $category->addColumn('parent_id', BigIntType::BIGINT)->setNotnull(false)->setDefault(0);
        $category->addColumn('description', StringType::STRING)->setNotnull(false);

        $category->setPrimaryKey(['id']);
        $category->addUniqueIndex(['name'], 'udx_post_category_name');
        $category->addUniqueIndex(['slug'], 'udx_post_category_slug');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
