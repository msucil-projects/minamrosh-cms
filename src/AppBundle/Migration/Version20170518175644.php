<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170518175644 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $page = $schema->createTable('page');
        $page->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $page->addColumn('title', StringType::STRING)->setNotnull(true);
        $page->addColumn('slug', StringType::STRING)->setNotnull(true);
        $page->addColumn('content', TextType::TEXT)->setNotnull(false);
        $page->addColumn('media_id', BigIntType::BIGINT)->setNotnull(false);
        $page->addColumn('status', IntegerType::INTEGER)->setNotnull(true);
        $page->addColumn('meta_id', BigIntType::BIGINT)->setNotnull(false);
        $page->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);
        $page->addColumn('category_id', BigIntType::BIGINT)->setNotnull(false);
        $page->addColumn('created_at', DateType::DATETIME)->setNotnull(true);
        $page->addColumn('created_by', BigIntType::BIGINT)->setNotnull(true);
        $page->addColumn('updated_at', DateType::DATETIME)->setNotnull(false);
        $page->addColumn('updated_by', BigIntType::BIGINT)->setNotnull(false);

        $page->setPrimaryKey(['id']);
        $page->addUniqueIndex(['title', 'slug'], 'udx_page_title_slug');
        $page->addForeignKeyConstraint('media', ['media_id'], ['id'], [], 'fk_media_id');
        $page->addForeignKeyConstraint('meta', ['meta_id'], ['id'], [], 'fk_meta_id');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
