<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BooleanType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170611182128 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $schema
            ->getTable('section_category')
            ->addColumn('editable', BooleanType::BOOLEAN)
            ->setNotnull(true)
            ->setDefault(false);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }
}
