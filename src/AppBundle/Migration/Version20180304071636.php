<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180304071636 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('author');
        $table->addColumn('id', BigIntType::BIGINT)->setNotnull(true)->setAutoincrement(true);
        $table->addColumn('name', StringType::STRING)->setNotnull(true);
        $table->addColumn('slug', StringType::STRING)->setNotnull(true);
        $table->addColumn('bio', TextType::TEXT)->setNotnull(false);
        $table->addColumn('facebook_link', StringType::STRING)->setNotnull(false);
        $table->addColumn('twitter_link', StringType::STRING)->setNotnull(false);
        $table->addColumn('linkedin_link', StringType::STRING)->setNotnull(false);
        $table->addColumn('email', StringType::STRING)->setNotnull(false);
        $table->addColumn('media_id', BigIntType::BIGINT)->setNotnull(false);
        $table->addColumn('version', IntegerType::INTEGER)->setNotnull(false);

        $table->setPrimaryKey(['id']);
//        $table->addForeignKeyConstraint(
//            'media',
//            ['media_id'], ['id'], [], 'fk_author_media_media_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
