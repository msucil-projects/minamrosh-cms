<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180307181056 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $table = $schema->getTable('post');

        $table->addForeignKeyConstraint('post_category', ['category_id'], ['id'], [], 'fk_post_category_id');
        $table->addForeignKeyConstraint('author', ['author_id'], ['id'], [], 'fk_post_author_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
