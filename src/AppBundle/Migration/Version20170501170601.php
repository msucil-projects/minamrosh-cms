<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170501170601 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('user');

        $table->addColumn('id', BigIntType::BIGINT, ['autoIncrement' => true]);
        $table->addColumn('username', StringType::STRING)->setNotnull(true);
        $table->addColumn('password', StringType::STRING)->setNotnull(true);
        $table->addColumn('email', StringType::STRING)->setNotnull(true);
        $table->addColumn('first_name', StringType::STRING)->setNotnull(true);
        $table->addColumn('last_name', StringType::STRING)->setNotnull(false);
        $table->addColumn('enabled', BooleanType::BOOLEAN)->setDefault(true);
        $table->addColumn('avatar', StringType::STRING)->setNotnull(false);
        $table->addColumn('role_id', BigIntType::BIGINT)->setNotnull(true);
        $table->addColumn('account_non_expired', BooleanType::BOOLEAN)->setDefault(true);
        $table->addColumn('account_non_locked', BooleanType::BOOLEAN)->setDefault(true);
        $table->addColumn('credentials_non_expired', BooleanType::BOOLEAN)->setDefault(true);
        $table->addColumn('created_at', DateType::DATETIME);
        $table->addColumn('updated_at', DateType::DATETIME)->setNotnull(false);

        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['username'], 'udx_user_username');
        $table->addUniqueIndex(['email'], 'udx_user_email');
        $table->addForeignKeyConstraint("role", ['role_id'], ['id'], [], "fk_user_role_role_id");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('user');
    }
}
