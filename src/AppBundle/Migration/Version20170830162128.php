<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\IntegerType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170830162128 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $schema->getTable('member')
               ->addColumn('display_order', IntegerType::INTEGER)
               ->setDefault(0)
               ->setNotnull(true);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }
}
