<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170602111541 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
       $table = $schema->createTable('member');
       $table->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
       $table->addColumn('full_name', StringType::STRING)->setNotnull(true);
       $table->addColumn('media_id', BigIntType::BIGINT)->setNotnull(false);
       $table->addColumn('designation', StringType::STRING)->setNotnull(false);
       $table->addColumn('email', StringType::STRING)->setNotnull(true);
       $table->addColumn('phone_no', StringType::STRING)->setNotnull(false);
       $table->addColumn('mobile_no', StringType::STRING)->setNotnull(false);
       $table->addColumn('bio', TextType::TEXT)->setNotnull(false);
       $table->addColumn('facebook_link', StringType::STRING)->setNotnull(false);
       $table->addColumn('twitter_link',StringType::STRING)->setNotnull(false);
       $table->addColumn('linkedin_link', StringType::STRING)->setNotnull(false);
       $table->addColumn('google_plus_link', StringType::STRING)->setNotnull(false);
       $table->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);

       $table->setPrimaryKey(['id']);
       $table->addUniqueIndex(['full_name'], 'udx_member_full_name');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('member');
    }
}
