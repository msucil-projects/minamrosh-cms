<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170525164822 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('ads_section');
        $table->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $table->addColumn('name', StringType::STRING)->setNotnull(true);
        $table->addColumn('height', IntegerType::INTEGER)->setNotnull(false);
        $table->addColumn('width', IntegerType::INTEGER)->setNotnull(false);
        $table->addColumn('description', StringType::STRING)->setNotnull(false);
        $table->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);
        $table->addColumn('created_at', DateType::DATETIME)->setNotnull(true);
        $table->addColumn('created_by', BigIntType::BIGINT)->setNotnull(true);


        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['name'], 'udx_ads_section_name');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
