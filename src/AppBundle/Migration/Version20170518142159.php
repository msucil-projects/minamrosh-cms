<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170518142159 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $section = $schema->createTable('section_category');
        $section->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $section->addColumn('name', StringType::STRING)->setNotnull(true);
        $section->addColumn('slug', StringType::STRING)->setNotnull(true);
        $section->addColumn('description', StringType::STRING)->setNotnull(false);
        $section->addColumn('section', BooleanType::BOOLEAN)->setNotnull(true);
        $section->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);

        $section->setPrimaryKey(['id']);
        $section->addUniqueIndex(['name','slug'], 'udx_section_category_name_slug');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
