<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170509162138 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('media');
        $table->addColumn('id', BigIntType::BIGINT)->setAutoincrement(true);
        $table->addColumn('name', StringType::STRING)->setNotnull(false);
        $table->addColumn('type', StringType::STRING)->setNotnull(false);
        $table->addColumn('version', IntegerType::INTEGER)->setNotnull(false)->setDefault(0);
        $table->addColumn('created_at', DateType::DATETIME)->setNotnull(true);
        $table->addColumn('updated_at', DateType::DATETIME)->setNotnull(false);
        $table->setPrimaryKey(['id']);

        $post = null;
        try {
            $post = $schema->getTable('post');
            if ($post) {
                $post->addColumn('media_id', BigIntType::BIGINT)->setNotnull(false);
                $post->addForeignKeyConstraint('media', ['media_id'], ['id'], [], 'fk_post_media_id');
            }
        } catch (SchemaException $ex)
        {
            if($ex->getCode() == SchemaException::TABLE_DOESNT_EXIST){
                echo "post does not exist";
            }
            elseif($ex->getCode() == SchemaException::COLUMN_ALREADY_EXISTS){
                $post->addForeignKeyConstraint('media', ['media_id'], ['id'], [], 'fk_post_media_id');
            }
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
