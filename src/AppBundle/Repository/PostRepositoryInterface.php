<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/9/17
 * Time: 5:51 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\DTO\SortQuery;
use AppBundle\Domain\Entity\Post;

interface PostRepositoryInterface
{
    public function save(Post $post);

    public function update(Post $post);

    public function delete(Post $post);

    public function deleteById($id);

    public function findOne(array $criteria);

    public function findAllBy(array $criteria = [], array $orderBy = null, $limit = null, $offset = null);

    public function findAllBySortQuery($searchTerm, SortQuery $sortRequest = null, $limit = null, $offset = null);

}