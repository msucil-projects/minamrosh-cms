<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/1/17
 * Time: 11:12 PM
 */

namespace AppBundle\Repository\Security;


use AppBundle\Domain\Entity\Security\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserRepository extends EntityRepository implements UserLoaderInterface
{

    /**
     * Loads the user for the given username.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @return UserInterface|null
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function save(User $user){
        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    public function update(User $user){
        $this->_em->merge($user);
        $this->_em->flush();
        return $user;
    }

    public function deleteById($id)
    {
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->delete("AppBundle:Security\User", "u")
            ->where("u.id = :id")
            ->setParameter("id", $id);

        $query = $queryBuilder->getQuery();
        return $query->execute();
    }
}