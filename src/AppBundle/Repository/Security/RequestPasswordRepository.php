<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/10/17
 * Time: 3:26 PM
 */

namespace AppBundle\Repository\Security;


use AppBundle\Domain\Entity\Security\RequestPassword;
use Doctrine\ORM\EntityRepository;

class RequestPasswordRepository extends EntityRepository
{
    public function save(RequestPassword $requestPassword){
        $this->_em->persist($requestPassword);
        $this->_em->flush();
    }

    public function remove(RequestPassword $requestPassword){
        $this->_em->remove($requestPassword);
        $this->_em->flush();
    }

}