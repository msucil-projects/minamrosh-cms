<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/14/17
 * Time: 10:43 PM
 */

namespace AppBundle\Repository;


interface FrontPostRepository
{

    public function getPostsByCategory($category, $limit = 10, $offset = null );

    public function getPostByCategory($category, $slug);

    public function search($term, $limit, $offset);

    public function getRecentPosts(array $criteria = [], array $orderBy = null, $limit);

    public function getPopularPosts(array $ids);

    public function getFeaturedPosts($limit, $offset);

    public function getFeaturedPostsByCategory($category, $limit);

    public function getRelatedPostsByCategory($category, $limit, $exclude);

}