<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 7:23 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\Entity\PostCategory;
use Doctrine\ORM\EntityRepository;

class PostCategoryRepository extends EntityRepository
{

    public function save(PostCategory $postCategory){
        $this->_em->persist($postCategory);
        $this->_em->flush();

        return $postCategory;
    }

    public function update(PostCategory $postCategory){
        $this->_em->merge($postCategory);
        $this->_em->flush();

        return $postCategory;
    }

    public function delete(PostCategory $postCategory){
        $this->_em->remove($postCategory);
        $this->_em->flush();
    }

    public function deleteById($id){
        $this->_em->remove($this->findOneBy(['id' => $id]));
        $this->_em->flush();
    }

}