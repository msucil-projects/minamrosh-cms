<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 5/31/2017
 * Time: 10:51 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\Entity\Site;
use Doctrine\ORM\EntityRepository;

class SiteRepository extends EntityRepository {

	public function save(Site $site){
		$this->_em->persist($site);
		$this->_em->flush();

		return $site;
	}

	public function update(Site $site){
		$this->_em->merge($site);
		$this->_em->flush();

		return $site;
	}
}