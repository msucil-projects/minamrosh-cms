<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/27/17
 * Time: 9:01 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\Entity\PostView;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class PostViewRepository extends EntityRepository
{

    public function save(PostView $postView){
        $this->_em->persist($postView);
        $this->_em->flush();
    }

    public function update(PostView $postView){
        $this->_em->merge($postView);
        $this->_em->flush();
    }

    public function remove($id){
        $this->_em->remove($this->findOneBy(['id' => $id]));
        $this->_em->flush();
    }

    public function getPopularPostIdsForCurrentMonth($limit){
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult("post_id", "post_id");

        $sql = "SELECT
  SUM(view_count) as totalView, MONTH(view_date) as month, post_id
FROM post_view
WHERE YEAR(view_date) = YEAR(CURDATE()) AND MONTH(view_date) = MONTH(CURDATE())
GROUP BY month, post_id ORDER BY totalView DESC LIMIT " . $limit;

        $query = $this->_em->createNativeQuery($sql, $rsm);

        $postIds = array_map(function($arr){return $arr['post_id'];}, $query->getResult());

        return $postIds;
    }

}