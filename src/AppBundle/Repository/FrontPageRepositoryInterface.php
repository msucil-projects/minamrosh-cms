<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/11/17
 * Time: 10:03 PM
 */

namespace AppBundle\Repository;


interface FrontPageRepositoryInterface
{
    public function getPageByCategory($category);
}