<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/8/17
 * Time: 10:53 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\DTO\QueryResult;
use AppBundle\Domain\DTO\SortQuery;
use AppBundle\Domain\Entity\Post;
use AppBundle\Domain\PropertyOption\Status;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PostRepository extends EntityRepository implements PostRepositoryInterface, FrontPostRepository
{

    public function save(Post $post)
    {
        $this->_em->persist($post);
        $this->_em->flush();

        return $post;
    }

    public function update(Post $post)
    {
        $this->_em->merge($post);
        $this->_em->flush();
        return $post;
    }

    public function delete(Post $post)
    {
        $this->_em->remove($post);
        $this->_em->flush();
    }

    public function findOne(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * @param $searchTerm
     * @param SortQuery $sortRequest
     * @param null $limit
     * @param null $offset
     * @return QueryResult
     * @internal param array $criteria
     * @internal param array|null $orderBy
     */
    public function findAllBySortQuery($searchTerm, SortQuery $sortRequest = null, $limit = null, $offset = null)
    {
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->select("p.id", "p.title", "fi.name as featureImage", "u.username as createdBy" ,"c.name as category", "p.createdAt", "c.name", "p.status")
            ->from("AppBundle:Post", "p")
            ->leftJoin('AppBundle:Security\User', 'u', Expr\Join::WITH, 'u.id = p.createdBy')
            ->leftJoin('p.featureImage', 'fi')
            ->leftJoin('p.category', 'c');

        if(!empty($searchTerm)){
            $queryBuilder->where($queryBuilder->expr()->orX(
                $queryBuilder->expr()
                    ->like('p.title', $queryBuilder->expr()->literal($searchTerm)),
                $queryBuilder->expr()->like('c.name', $queryBuilder->expr()->literal($searchTerm)),
                    $queryBuilder->expr()->like('u.username', $queryBuilder->expr()->literal($searchTerm))
            ));
        }

        $countQuery = clone $queryBuilder->getQuery();


        if ($sortRequest != null) {
            $queryBuilder->addOrderBy("p.{$sortRequest->getColumnName()}", $sortRequest->getSortDirection());
        } else {
            $queryBuilder->orderBy("p.createdAt", "DESC");
        }

        $queryBuilder->setFirstResult($offset)
            ->setMaxResults($limit);

        $query = $queryBuilder->getQuery();

        return new QueryResult(count($countQuery->execute()), $query->execute());
    }

    public function deleteById($id)
    {
        $post = $this->findOneBy(['id' => $id]);
        $this->_em->remove($post);
        $this->_em->flush();
    }

    public function findAllBy(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function getPostsByCategory($category, $limit = 10, $offset = null)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->innerJoin('AppBundle:PostCategory', 'c', Expr\Join::WITH, 'c.id = p.category');
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('c.slug', $qb->expr()->literal($category)),
                $qb->expr()->eq('p.status', Status::PUBLISH)
            )
        )
            ->orderBy('p.createdAt', 'DESC');

        $qb->setMaxResults($limit);

        if($offset){

            $qb->setFirstResult(($offset -1) * $limit);
        }

        $paginator = new Paginator($qb, false);

        return $paginator;
    }

    public function getPostByCategory($category, $slug)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->innerJoin('AppBundle:PostCategory', 'c');
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('c.slug', $qb->expr()->literal($category)),
                $qb->expr()->eq('p.status', Status::PUBLISH),
                $qb->expr()->eq('p.slug', $qb->expr()->literal($slug))
            )
        );

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function search($term, $limit, $offset)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->innerJoin('AppBundle:PostCategory', 'c', Expr\Join::WITH, 'c.id = p.category');
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->like('p.title', $qb->expr()->literal('%' . $term . '%')),
                $qb->expr()->eq('p.status', Status::PUBLISH)
            )
        )
            ->orderBy('p.createdAt', 'DESC');

        $qb->setMaxResults($limit);

        if($offset){

            $qb->setFirstResult(($offset -1) * $limit);
        }

        $paginator = new Paginator($qb, false);

        return $paginator;
    }

    public function getRecentPosts(array $criteria = [], array $orderBy = null, $limit)
    {
        return $this->findBy($criteria, $orderBy, $limit);
    }

    public function getPopularPosts(array $ids)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where($qb->expr()->in('p.id', $ids));

        return $qb->getQuery()->execute();
    }

	public function getFeaturedPosts( $limit, $offset ) {
		return $this->findAllBy(['featurePost' => true, 'status' => Status::PUBLISH], ['createdAt' => 'desc'], $limit, $offset);
	}

	public function getFeaturedPostsByCategory( $category, $limit ) {
		$qb = $this->createQueryBuilder('p');

		$qb->innerJoin('AppBundle:PostCategory', 'c', Expr\Join::WITH, 'c.id = p.category');
		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('c.slug', $qb->expr()->literal($category)),
				$qb->expr()->eq('p.status', Status::PUBLISH),
				$qb->expr()->eq('p.featurePost', true)
			)
		)
		   ->orderBy('p.createdAt', 'DESC');

		$qb->setMaxResults($limit);

		return $qb->getQuery()->execute();
	}

	public function getRelatedPostsByCategory( $category, $limit, $exclude ) {

		$qb = $this->createQueryBuilder( 'p' );

		$qb->innerJoin( 'AppBundle:PostCategory', 'c', Expr\Join::WITH, 'c.id = p.category' );
		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq( 'c.slug', $qb->expr()->literal( $category ) ),
				$qb->expr()->eq( 'p.status', Status::PUBLISH ),
				$qb->expr()->neq( 'p.id', $exclude )
			)
		)
		   ->orderBy( 'p.createdAt', 'DESC' );

		$qb->setMaxResults( $limit );

		$paginator = new Paginator( $qb, false );

		return $paginator;
	}
}