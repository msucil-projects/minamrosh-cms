<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 3/4/18
 * Time: 5:02 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\Entity\Author;
use AppBundle\Util\StringHelper;
use Doctrine\ORM\EntityRepository;

class AuthorRepository extends EntityRepository
{
    public function save(Author $author){
        $author->setSlug(StringHelper::generateSlug($author->getName()));
        $this->_em->persist($author);
        $this->_em->flush();

        return $author;
    }

    public function update(Author $author){
        $author->setSlug(StringHelper::generateSlug($author->getName()));
        $this->_em->merge($author);
        $this->_em->flush();

        return $author;
    }

    public function deleteById($id){
        $author = $this->findOneBy(['id' => $id]);
        $this->_em->remove($author);
        $this->_em->flush();
    }
}