<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/19/17
 * Time: 12:41 AM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\DTO\QueryResult;
use AppBundle\Domain\DTO\SortQuery;
use AppBundle\Domain\Entity\Page;
use AppBundle\Domain\PropertyOption\Status;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PageRepository extends EntityRepository implements FrontPageRepositoryInterface
{

    public function save(Page $page){
        $this->_em->persist($page);
        $this->_em->flush();

        return $page;
    }

    public function update(Page $page){
        $this->_em->merge($page);
        $this->_em->flush();

        return $page;
    }

    public function deleteById($id){
    	$this->_em->remove($this->findOneBy(['id' => $id]));
    	$this->_em->flush();
    }

    public function findAllBySortQuery(array $criteria, SortQuery $sortRequest = null, $limit = null, $offset = null)
    {
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->select("p.id", "p.title","c.name as category", "p.createdAt", "p.content", "p.status", "u.username as createdBy")->from("AppBundle:Page", "p")
	        ->leftJoin('AppBundle:Security\User', 'u', Expr\Join::WITH, 'u.id = p.createdBy')
            ->leftJoin('p.category', 'c');

        $countQuery = clone $queryBuilder->getQuery();


        if ($sortRequest != null) {
            $queryBuilder->addOrderBy("p.{$sortRequest->getColumnName()}", $sortRequest->getSortDirection());
        } else {
            $queryBuilder->orderBy("p.id", "ASC");
        }

        $queryBuilder->setFirstResult($offset)
            ->setMaxResults($limit);

        $query = $queryBuilder->getQuery();

        return new QueryResult(count($countQuery->execute()), $query->execute());
    }

    public function getPageByCategory($category)
    {
        $cb = $this->createQueryBuilder('p');

        return $cb->innerJoin('p.category', 'c', Expr\Join::WITH, 'c.section = false')
            ->where($cb->expr()->andX(
                $cb->expr()->eq('p.status', Status::PUBLISH),
                $cb->expr()->eq('c.slug', $cb->expr()->literal($category))
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }
}