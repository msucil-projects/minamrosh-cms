<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 8:21 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\Entity\SectionCategory;
use Doctrine\ORM\EntityRepository;

class SectionCategoryRepository extends EntityRepository
{

    public function save(SectionCategory $category){
        $this->_em->persist($category);
        $this->_em->flush();

        return $category;
    }

    public function update(SectionCategory $category){
        $this->_em->merge($category);
        $this->_em->flush();

        return $category;
    }

    public function deleteById($id){
        $this->_em->createQueryBuilder()->delete('AppBundle:SectionCategory', 'sc')
            ->where('sc.id = :id')
            ->setParameter('id', $id)->getQuery()->execute();
    }

}