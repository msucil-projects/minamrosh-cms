<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/20/17
 * Time: 4:41 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\Entity\PageSection;
use AppBundle\Domain\PropertyOption\Status;
use Doctrine\ORM\EntityRepository;

class PageSectionRepository extends EntityRepository
{
    public function save(PageSection $pageSection){
        $this->_em->persist($pageSection);
        $this->_em->flush();

        return $pageSection;
    }

    public function update(PageSection $pageSection) {
        $this->_em->merge($pageSection);
        $this->_em->flush();

        return $pageSection;
    }

    public function deleteById($id) {
        $this->_em->remove($this->findOneBy(['id' => $id]));
        $this->_em->flush();
    }

    public function findBySectionCategory($section) {
	    $qb = $this->createQueryBuilder('p');

	    $qb->innerJoin('AppBundle:SectionCategory', 'c');
	    $qb->where(
		    $qb->expr()->andX(
			    $qb->expr()->eq('c.slug', $qb->expr()->literal($section)),
			    $qb->expr()->eq('p.status', Status::PUBLISH)
		    )
	    );

	    return $qb->getQuery()->getOneOrNullResult();
    }
}