<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 11:07 PM
 */

namespace AppBundle\Repository\Advertisement;


use AppBundle\Domain\Entity\Advertisement\AdClient;
use Doctrine\ORM\EntityRepository;

class AdClientRepository extends EntityRepository
{

    public function save(AdClient $adsClient){
        $this->_em->persist($adsClient);
        $this->_em->flush();

        return $adsClient;
    }

    public function update(AdClient $adsClient){
        $this->_em->merge($adsClient);
        $this->_em->flush();

        return $adsClient;
    }

    public function deleteById($id){
        $this->_em->createQueryBuilder()->delete('AppBundle:Advertisement\AdClient', 'ac')
            ->where('ac.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->execute();
    }

}