<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/25/17
 * Time: 10:45 PM
 */

namespace AppBundle\Repository\Advertisement;


use AppBundle\Domain\Entity\Advertisement\AdSection;
use Doctrine\ORM\EntityRepository;

class AdSectionRepository extends EntityRepository
{
    public function save(AdSection $adsSection){
        $this->_em->persist($adsSection);
        $this->_em->flush();

        return $adsSection;
    }

    public function update(AdSection $adsSection){
        $this->_em->merge($adsSection);
        $this->_em->flush();

        return $adsSection;
    }

    public function deleteById($id){
        return $this->_em->createQueryBuilder()
            ->delete('AppBundle:Advertisement\AdSection', 'asec')
            ->where('asec.id = :id')
            ->setParameter('id' , $id)
            ->getQuery()
            ->execute();
    }

}