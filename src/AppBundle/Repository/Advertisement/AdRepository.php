<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/26/17
 * Time: 10:38 PM
 */

namespace AppBundle\Repository\Advertisement;


use AppBundle\Domain\Entity\Advertisement\Ad;
use AppBundle\Domain\PropertyOption\Status;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class AdRepository extends EntityRepository
{
    public function save(Ad $ad){
        $this->_em->persist($ad);
        $this->_em->flush();

        return $ad;
    }

    public function update(Ad $ads){
        $this->_em->merge($ads);
        $this->_em->flush();

        return $ads;
    }

    public function deleteById($id){
        $this->_em->remove($this->findOneBy(['id' => $id]));
        $this->_em->flush();
    }

    public function getActiveAds($adSection){
        $qb = $this->createQueryBuilder('a');

        $qb->innerJoin('AppBundle:Advertisement\AdSection', 's', Join::WITH, 's.id = a.section');
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq('s.slug', $qb->expr()->literal($adSection)),
            $qb->expr()->eq('a.expired', 'false'),
            $qb->expr()->eq('a.status', Status::PUBLISH),
            $qb->expr()->lte('a.validFrom', $qb->expr()->literal(date_format(new \DateTime(), 'Y-m-d'))),
            $qb->expr()->gte('a.validTo', $qb->expr()->literal(date_format(new \DateTime(),'Y-m-d')))
        ));

        return $qb->getQuery()->execute();
    }
}