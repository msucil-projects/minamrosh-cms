<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:14 PM
 */

namespace AppBundle\Repository;


use AppBundle\Domain\Entity\Member;
use Doctrine\ORM\EntityRepository;

class MemberRepository extends EntityRepository {

	public function save(Member $member){
		$this->_em->persist($member);
		$this->_em->flush();

		return $member;
	}

	public function update(Member $member){
		$this->_em->merge($member);
		$this->_em->flush();

		return $member;
	}

	public function deleteById($id){
		$member = $this->findOneBy(['id' => $id]);
		$this->_em->remove($member);
		$this->_em->flush();
	}

}