<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 12:02 PM
 */

namespace AppBundle\Controller\User;


use AppBundle\Domain\DTO\PageSectionDTO;
use AppBundle\Domain\Form\PageSectionForm;
use AppBundle\Domain\Mapper\PageSectionMapper;
use AppBundle\Util\SectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PageSectionController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/page/section")
 */
class PageSectionController extends Controller
{
    /**
     * @Route("/", name="user-page-sections")
     */
    public function indexAction()
    {
        $sectionService = $this->getService();

        return $this->render(':user/page-section:index.html.twig', [
            "sections" => $sectionService->findAll(['section_type' => SectionType::PAGE])
        ]);
    }

    /**
     * @Route("/new", name="user-new-page-section")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $sectionDTO = new PageSectionDTO();
        $sectionForm = $this->createForm(PageSectionForm::class, $sectionDTO);

        $sectionForm->handleRequest($request);

        if ($sectionForm->isSubmitted() && $sectionForm->isValid()) {

            if($sectionDTO->getFeatureImage()){
                $uploadService = $this->getFileService();
                $fileName = $uploadService->uploadFile($sectionDTO->getFeatureImage()->getName());
                $sectionDTO->setFeatureImage($sectionDTO->getFeatureImage()->setName($fileName));
            }

            $page = $this->getService()->save($sectionDTO);

            return $this->redirectToRoute("user-page-sections");
        }

        return $this->render(":user/page-section:create-page-section.html.twig", [
            'section' => $sectionForm->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @Route(name="user-update-page-section", path="/update/{id}")
     *
     */
    public function updateAction(Request $request, $id)
    {
        $section = $this->getService()->findById($id);

        $sectionDTO = PageSectionMapper::mapToDTO($section);

        $sectionForm = $this->createForm(PageSectionForm::class, $sectionDTO);

        $sectionForm->handleRequest($request);

        if ($sectionForm->isSubmitted() && $sectionForm->isValid()) {

            if($sectionDTO->getFeatureImage()){
                if($sectionDTO->getFeatureImage()->getName() != null){
                    $uploadService = $this->getFileService();

                    if($sectionDTO->getFeatureImage()->getOldName() != null){
                        $uploadService->deleteFile($sectionDTO->getFeatureImage()->getOldName());
                    }

                    $fileName = $uploadService->uploadFile($sectionDTO->getFeatureImage()->getName());
                    $sectionDTO->getFeatureImage()->setName($fileName);
                }
                elseif($sectionDTO->getFeatureImage()->getId()){
                    $sectionDTO->getFeatureImage()->setName($sectionDTO->getFeatureImage()->getOldName());
                }
            }

            $section = $this->getService()->update($sectionDTO);

            return $this->redirectToRoute("user-page-sections");
        }

        return $this->render(":user/page-section:update-page-section.html.twig", [
            'section' => $sectionForm->createView()]);
    }

    /**
     *
     * @Route(name="user-delete-page-section", path="/delete/{id}")
     * @Method({"POST"})
     */
    public function deleteAction($id)
    {
        $this->getService()->deleteById($id);

        return $this->redirectToRoute("user-pages");
    }

    private function getService(){
        return $this->get("page_section_service");
    }

    private function getFileService(){
        return $this->get('file_service');
    }
}