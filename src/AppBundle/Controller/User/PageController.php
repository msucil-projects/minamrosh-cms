<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/19/17
 * Time: 12:06 AM
 */

namespace AppBundle\Controller\User;


use AppBundle\Domain\DTO\PageDTO;
use AppBundle\Domain\Form\PageForm;
use AppBundle\Domain\Mapper\DataTableMapper;
use AppBundle\Domain\Mapper\PageMapper;
use Doctrine\DBAL\Types\IntegerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PageController
 * @package AppBundle\Controller\User
 * @Route("/user/page")
 */
class PageController extends Controller
{
    /**
     * @Route("/", name="user-pages")
     */
    public function indexAction()
    {
        return $this->render(':user/page:index.html.twig', [
            "pages" => $this->getService()->findAll()
        ]);
    }

    /**
     * @Route("/new", name="user-new-page")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $pageDTO = new PageDTO();
        $pageForm = $this->createForm(PageForm::class, $pageDTO);

        $pageForm->handleRequest($request);

        if ($pageForm->isSubmitted() && $pageForm->isValid()) {

            if($pageDTO->getFeatureImage()){
                $uploadService = $this->getFileService();
                $fileName = $uploadService->uploadFile($pageDTO->getFeatureImage()->getName());
                $pageDTO->setFeatureImage($pageDTO->getFeatureImage()->setName($fileName));
            }

            $post = $this->getService()->save($pageDTO);

            return $this->redirectToRoute("user-pages");
        }

        return $this->render(":user/page:create-page.html.twig", [
            'page' => $pageForm->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @Route(name="user-update-page", path="/update/{id}")
     *
     */
    public function updateAction(Request $request, $id)
    {
        $page = $this->getService()->findById($id);

        $pageDTO = PageMapper::mapToDTO($page);

        $pageForm = $this->createForm(PageForm::class, $pageDTO);

        $pageForm->handleRequest($request);

        if ($pageForm->isSubmitted() && $pageForm->isValid()) {

            if($pageDTO->getFeatureImage()){
                if($pageDTO->getFeatureImage()->getName() != null){
                    $uploadService = $this->getFileService();

                    if($pageDTO->getFeatureImage()->getOldName() != null){
                        $uploadService->deleteFile($pageDTO->getFeatureImage()->getOldName());
                    }

                    $fileName = $uploadService->uploadFile($pageDTO->getFeatureImage()->getName());
                    $pageDTO->getFeatureImage()->setName($fileName);
                }
                elseif($pageDTO->getFeatureImage()->getId()){
                    $pageDTO->getFeatureImage()->setName($pageDTO->getFeatureImage()->getOldName());
                }
            }

            $page = $this->getService()->update($pageDTO);

            return $this->redirectToRoute("user-pages");
        }

        return $this->render(":user/page:update-page.html.twig", [
            'page' => $pageForm->createView()]);
    }

    /**
     * @Route(path="/view/{param}", name="user-view-post")
     * @param $param
     * @return Response
     */
    public function viewAction($param)
    {

        $postService = $this->get("post_service");

        if ($param instanceof IntegerType) {
            $post = $postService->findById($param);
        } else {
            $post = $postService->findBySlug($param);
        }

        if ($post == null) {
            throw new NotFoundHttpException("Page not found");
        }

        return new Response();
    }

    /**
     *
     * @Route(name="user-delete-page", path="/delete/{id}")
     * @Method({"POST"})
     */
    public function deleteAction($id)
    {
        $this->getService()->deleteById($id);

        return $this->redirectToRoute("user-pages");
    }

    /**
     * @Route(name="user-datatable-page", path="/datatable")
     * @param Request $request
     * @return Response
     */
    public function dataTableAction(Request $request){
        $dataTableRequest = DataTableMapper::mapToDataTableRequestBody($request);
        return $this->getService()->getPages($dataTableRequest);
    }

    private function getService(){
        return $this->get("page_service");
    }

    private function getFileService(){
        return $this->get('file_service');
    }

}