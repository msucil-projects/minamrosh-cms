<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/9/17
 * Time: 7:44 PM
 */

namespace AppBundle\Controller\User\Security;


use AppBundle\Domain\DTO\Security\ChangePasswordDTO;
use AppBundle\Domain\Form\Security\ChangePasswordForm;
use AppBundle\Domain\Form\Security\ProfileForm;
use AppBundle\Domain\Mapper\Security\UserMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package AppBundle\Controller\User\Security
 *
 * @Route("/user/profile")
 */
class ProfileController extends Controller
{
    /**
     * @Route("/", name="user-profile")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function viewProfile(Request $request){
        $profile = UserMapper::mapToProfile($this->getCurrentUserService()->getUser());
        $profileForm = $this->createForm(ProfileForm::class, $profile);

        $profileForm->handleRequest($request);

        if($profileForm->isSubmitted() && $profileForm->isValid()){
            $this->getUserService()->changeProfile($profile);

            return $this->redirectToRoute('user-profile');
        }

        return $this->render(':user/security/profile:profile.html.twig', [
            'profile' => $profileForm->createView()
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/change-password", name="user-change-password")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePassword(Request $request){
        $changePassword = new ChangePasswordDTO();
        $form = $this->createForm(ChangePasswordForm::class, $changePassword);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->getUserService()->changePassword($changePassword->getPassword(), $this->get('security.token_storage')->getToken()->getUser()->getId());

            return $this->redirectToRoute('user-profile');
        }

        return $this->render(':user/security/profile:change-password.html.twig', [
            'password' => $form->createView()
        ]);
    }

    private function getUserService(){
        return $this->get('user_service');
    }

    private function getCurrentUserService(){
        return $this->get("current_user_service");
    }

}