<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/25/17
 * Time: 11:40 PM
 */

namespace AppBundle\Controller\User\Security;

use AppBundle\Domain\DTO\Security\ChangePasswordDTO;
use AppBundle\Domain\Form\Security\ChangePasswordForm;
use AppBundle\Domain\Form\Security\ResetPassword;
use AppBundle\Service\Messaging\ContentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class UserController
 * @package AppBundle\Controller\User
 *
 */
class SecurityController extends Controller {
	/**
	 * @param Request $request
	 *
	 * @Route("/login", name="user-login")
	 * @Method({"GET", "POST"})
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function loginAction( Request $request ) {
		$authenticationUtils = $this->get( 'security.authentication_utils' );

		// get the login error if there is one
		$error = $authenticationUtils->getLastAuthenticationError();

		// last username entered by the user
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render( ':user/security:login.html.twig', array(
			'last_username' => $lastUsername,
			'error'         => $error,
		) );
	}


	/**
	 * @Route("forget-password", name="forget-password")
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function forgetPasswordAction( Request $request ) {
		$resetPassword = $this->createForm( ResetPassword::class );

		$resetPassword->handleRequest( $request );

		if ( $resetPassword->isSubmitted() && $resetPassword->isValid() ) {

			$user = $this->getUserService()->findOne( [ 'email' => $resetPassword->getData()['email'] ] );

			if ( $user ) {
				$requestPassword = $this->getPasswordService()->requestPassword( $user );

				$msgBody = "Reset your password by following this link " .
				           $this->generateUrl( 'reset-password', [ 'token' => $requestPassword->getRequestKey() ], UrlGeneratorInterface::ABSOLUTE_URL );

				$this->get( 'email_service' )
				     ->sendSystemEmail( 'Reset Password', [
					     'name'    => $user->getFirstName(),
					     'message' => $msgBody
				     ], $resetPassword->getData()['email'], ContentType::HTML );

				return $this->redirectToRoute( 'email-confirm' );
			}

			$resetPassword->get( 'email' )->addError( new FormError( 'Email does not exist!' ) );
		}

		return $this->render( ":user/security:forget-password.html.twig", [
			'resetPassword' => $resetPassword->createView()
		] );
	}

	/**
	 * @Route("/email-confirm", name="email-confirm")
	 *
	 */
	public function emailConfirm() {

		return $this->render( ':user/security:email-confirm.html.twig' );
	}

	/**
	 * @param Request $request
	 * @param $token
	 *
	 * @Route("/reset-password/{token}", name="reset-password")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function resetPassword( Request $request, $token ) {
		$passwordRequest = $this->getPasswordService()->findOneBy( [ 'requestKey' => $token ] );

		if ( $passwordRequest ) {

			$changePassword     = new ChangePasswordDTO();
			$changePasswordForm = $this->createForm( ChangePasswordForm::class, $changePassword );

			$changePasswordForm->handleRequest( $request );

			if ( $changePasswordForm->isSubmitted() && $changePasswordForm->isValid() ) {

				$this->getPasswordService()
				     ->changePassword( $changePassword->getPassword(), $passwordRequest );

				$user = $this->get( 'user_service' )->findOne( [ 'id' => $passwordRequest->getUserId() ] );

				$msgBody = "Your password has been changed!";

				$this->get( 'email_service' )
				     ->sendSystemEmail( "Password Changed",
					     [
						     'name'    => $user->getFirstName(),
						     'message' => $msgBody
					     ], $user->getEmail(), ContentType::HTML );

				return $this->redirectToRoute( 'password-changed' );
			}

			return $this->render( ':user/security:change-password.html.twig', [
				'password' => $changePasswordForm->createView()
			] );
		} else {
			$flashBag = $this->get( 'session' )->getFlashBag();
			$flashBag->add( 'message', 'Invalid token!' );

			return $this->redirectToRoute( 'forget-password' );
		}
	}

	/**
	 * @Route("/password-changed", name="password-changed")
	 */
	public function passwordChanged() {
		return $this->render( ':user/security:password-changed.html.twig' );
	}

	private function getUserService() {
		return $this->get( 'user_service' );
	}

	private function getPasswordService() {
		return $this->get( 'request_password_service' );
	}
}