<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/5/2017
 * Time: 5:59 PM
 */

namespace AppBundle\Controller\User\Security;


use AppBundle\Domain\DTO\Security\RegisterUser;
use AppBundle\Domain\Form\Security\RegisterUserForm;
use AppBundle\Domain\Form\Security\UpdateUserForm;
use AppBundle\Domain\Mapper\Security\UserMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class AccountController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/account")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AccountController extends Controller {

	/**
	 * @Route("/", name="user-accounts")
     *
	 */
	public function actionIndex(){
		return $this->render(':user/security/account:index.html.twig', [
			'users' => $this->getService()->findAll()
		]);
	}

	/**
	 * @Route("/register", name="user-register")
     *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function registerUserAction(Request $request){
		$registerUser = new RegisterUser();
		$registerForm = $this->createForm(RegisterUserForm::class, $registerUser);

		$registerForm->handleRequest($request);

		if($registerForm->isSubmitted() && $registerForm->isValid()){
			$this->get("user_service")->register($registerForm->getData());
			return $this->redirectToRoute('user-accounts');
		}

		return $this->render(':user/security/account:register-user.html.twig', [
			'user' => $registerForm->createView()
		]);
	}


	/**
	 * @param Request $request
	 * @param $id
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/edit/{id}", name="user-update-account")
	 */
	public function updateUser(Request $request, $id){
		$dbUser = $this->getService()->findOne(['id' => $id]);
		$user = UserMapper::mapUserToUpdateUser($dbUser);

		$updateForm = $this->createForm(UpdateUserForm::class, $user);

		$updateForm->handleRequest($request);

		if($updateForm->isSubmitted() && $updateForm->isValid()){
            $this->getService()->update($user);
            return $this->redirectToRoute('user-accounts');
		}

		return $this->render(':user/security/account:update-user.html.twig', [
			'user' => $updateForm->createView()
		]);
	}

	/**
	 * @param $id
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 *
	 * @Route("/delete/{id}", name="delete-account")
	 */
	public function deleteAction($id)
	{
		$this->get('user_service')->deleteById($id);

		return $this->redirectToRoute("user-accounts");
	}

	public function getService(){
		return $this->get('user_service');
	}

}