<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/25/17
 * Time: 11:21 PM
 */

namespace AppBundle\Controller\User\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdSectionDTO;
use AppBundle\Domain\Form\AdSectionForm;
use AppBundle\Domain\Mapper\Advertisement\AdSectionMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdsSectionController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/advertisement/section")
 */
class AdSectionController extends Controller
{
    /**
     * @Route("/", name="user-ad-section")
     */
    public function actionIndex(){
       return $this->render(':user/advertisement/section:index.html.twig',[
           'sections' => $this->getService()->findAll()
       ]);
    }

    /**
     * @Route("/create", name="user-create-ad-section")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function actionCreate(Request $request){
        $sectionDto = new AdSectionDTO();
        $sectionForm = $this->createForm(AdSectionForm::class, $sectionDto);

        $sectionForm->handleRequest($request);

        if($sectionForm->isSubmitted() && $sectionForm->isValid()){
            $this->getService()->save($sectionDto);

            return $this->redirectToRoute('user-ad-section');
        }

        return $this->render(':user/advertisement/section:create-ad-section.html.twig',
            ['section' => $sectionForm->createView()]);

    }

    /**
     * @Route("/update/{id}", name="user-update-ad-section")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function actionUpdate(Request $request, $id){
        $section = $this->getService()->findById($id);
        $sectionDto = AdSectionMapper::mapToDTO($section);
        $sectionForm = $this->createForm(AdSectionForm::class, $sectionDto);

        $sectionForm->handleRequest($request);

        if($sectionForm->isSubmitted() && $sectionForm->isValid()){
            $this->getService()->update($sectionDto);

            return $this->redirectToRoute('user-ad-section');
        }

        return $this->render(':user/advertisement/section:update-ad-section.html.twig', [
            'section' => $sectionForm->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="user-delete-ad-section")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function actionDelete($id){
        $this->getService()->deleteById($id);

        return $this->redirectToRoute('user-ad-section');
    }

    private function getService(){
        return $this->get("ad_section_service");
    }

}