<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 11:34 PM
 */

namespace AppBundle\Controller\User\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdClientDTO;
use AppBundle\Domain\Form\Advertisement\AdClientForm;
use AppBundle\Domain\Mapper\Advertisement\AdClientMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdsClientController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/advertisement/client")
 */
class AdClientController extends Controller
{

    /**
     * @Route("/", name="user-ad-clients")
     */
    public function actionIndex(){
        return $this->render(':user/advertisement/client:index.html.twig',[
            'clients' => $this->getService()->findAll([])
        ]);
    }

    /**
     * @Route("/new", name="user-new-ad-client")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionNew(Request $request){
        $adsClientDTO = new AdClientDTO();
        $adsClientForm = $this->createForm(AdClientForm::class, $adsClientDTO);

        $adsClientForm->handleRequest($request);

        if($adsClientForm->isSubmitted()){
            $this->getService()->save($adsClientDTO);

            return $this->redirectToRoute('user-ad-clients');
        }

        return $this->render('user/advertisement/client/create-client.html.twig',
            ['client' => $adsClientForm->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @Route("/edit/{id}", name="user-update-ad-client")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function actionUpdate(Request $request, $id){
        $adsClient = $this->getService()->findOne(['id' => $id]);
        $adsClientDTO = AdClientMapper::mapToDTO($adsClient);
        $adsClientForm = $this->createForm(AdClientForm::class, $adsClientDTO);

        $adsClientForm->handleRequest($request);

        if($adsClientForm->isSubmitted() && $adsClientForm->isValid()){
            $this->getService()->update($adsClientDTO);
            return $this->redirectToRoute('user-ad-clients');
        }

        return $this->render(':user/advertisement/client:update-client.html.twig', [
            'client' => $adsClientForm->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="user-delete-ad-client")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function actionDelete($id){
        $this->getService()->deleteById($id);

        return $this->redirectToRoute('user-ad-clients');
    }

    private function getService(){
        return $this->get('ad_client_service');
    }

}