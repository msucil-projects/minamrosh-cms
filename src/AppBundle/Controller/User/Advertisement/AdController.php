<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/27/17
 * Time: 12:42 AM
 */

namespace AppBundle\Controller\User\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdDTO;
use AppBundle\Domain\Form\Advertisement\AdForm;
use AppBundle\Domain\Mapper\Advertisement\AdMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class AdController
 * @package AppBundle\Controller\User\Advertisement
 *
 * @Route("/user/advertisement/ad")
 */
class AdController extends Controller
{

    /**
     *@Route("/", name="user-ads")
     */
    public function actionIndex(){
        return $this->render(':user/advertisement/ad:index.html.twig',[
            'ads' => $this->getService()->findAll()
        ]);
    }

    /**
     * @Route("/new", name="user-new-ad")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionNew(Request $request){
        $adDto = new AdDTO();
        $adForm = $this->createForm(AdForm::class, $adDto);

        $adForm->handleRequest($request);

        if($adForm->isSubmitted() && $adForm->isValid()){
            if($adDto->getImage()){
                $uploadService = $this->getFileService();
                $fileName = $uploadService->uploadFile($adDto->getImage()->getName());
                $adDto->setImage($adDto->getImage()->setName($fileName));
            }

            $this->getService()->save($adDto);

            return $this->redirectToRoute("user-ads");
        }

        return $this->render(':user/advertisement/ad:create-ad.html.twig',[
            'ad' => $adForm->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="user-update-ad")
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionUpdate(Request $request, $id){
        $ad = $this->getService()->findOneBy(['id' => $id]);
        $adDto = AdMapper::mapToDTO($ad);

        $adForm = $this->createForm(AdForm::class, $adDto);

        $adForm->handleRequest($request);

        if($adForm->isSubmitted() && $adForm->isValid()){
            if($adDto->getImage()){
                if($adDto->getImage()->getName() != null){
                    $uploadService = $this->getFileService();

                    if($adDto->getImage()->getOldName() != null){
                        $uploadService->deleteFile($adDto->getImage()->getOldName());
                    }

                    $fileName = $uploadService->uploadFile($adDto->getImage()->getName());
                    $adDto->getImage()->setName($fileName);
                }
                elseif($adDto->getImage()->getId()){
                    $adDto->getImage()->setName($adDto->getImage()->getOldName());
                }

                $this->getService()->update($adDto);

                return $this->redirectToRoute('user-ads');
            }

            $this->getService()->update($adDto);
        }

        return $this->render(':user/advertisement/ad:update-ad.html.twig',[
            'ad' => $adForm->createView()
        ]);
    }

    /**
     * @Route("/delete{id}", name="user-delete-ad")
     * @Method(methods={"POST"})
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function actionDelete($id) {
        $this->getService()->deleteById($id);

        return $this->redirectToRoute("user-ads");
    }

    private function getService(){
        return $this->get('ad_service');
    }

    private function getFileService(){
        return $this->get('file_service');
    }

}