<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 5/31/2017
 * Time: 11:28 PM
 */

namespace AppBundle\Controller\User;


use AppBundle\Domain\DTO\SiteDTO;
use AppBundle\Domain\Form\SiteForm;
use AppBundle\Domain\Mapper\SiteMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ConfigurationController
 * @package AppBundle\Controller\User
 *
 * @Route("/user")
 */
class ConfigurationController extends Controller {

	/**
	 * @Route("/site", name="user-view-site")
	 */
	public function actionVewSite(){
		$site = $this->getSiteService()->findOne();

		if($site){
			return $this->render(':user/configuration/site:site.html.twig', ['site' => $site]);
		}

		return $this->redirectToRoute('user-configure-site');
	}

	/**
	 * @param Request $request
	 *
	 * @Route("/configure/new-site", name="user-configure-site")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function actionConfigureSite(Request $request){
		if($this->isSiteExists()){
			return $this->redirectToRoute('user-view-site');
		}
		$site = new SiteDTO();
		$siteForm = $this->createForm(SiteForm::class, $site);

		$siteForm->handleRequest($request);

		if($siteForm->isSubmitted() && $siteForm->isValid()){

			if($site->getBanner()->getName()){
				$uploadService = $this->getFileService();
				$fileName = $uploadService->uploadFile($site->getBanner()->getName());
				$site->getBanner()->setName($fileName);
			}

			if($site->getLogo()->getName()){
				$uploadService = $this->getFileService();
				$fileName = $uploadService->uploadFile($site->getLogo()->getName());
				$site->getLogo()->setName($fileName);
			}


			$this->getSiteService()->save($site);
			return $this->redirectToRoute('user-view-site');
		}

		return $this->render(':user/configuration/site:configure-site.html.twig',
			['site' => $siteForm->createView()]);
	}

	/**
	 * @param Request $request
	 * @param $id
	 *
	 * @Route("/configure/update-site", name="user-update-site")
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function actionUpdateSite(Request $request){
		$site = $this->getSiteService()->findOne();
		$siteDto = SiteMapper::mapToDTO($site);

		$siteForm = $this->createForm(SiteForm::class, $siteDto);

		$siteForm->handleRequest($request);

		if($siteForm->isSubmitted() && $siteForm->isValid()){

			if($siteDto->getBanner()){
				if($siteDto->getBanner()->getName() != null){
					$uploadService = $this->getFileService();

					if($siteDto->getBanner()->getOldName() != null){
						$uploadService->deleteFile($siteDto->getBanner()->getOldName());
					}

					$fileName = $uploadService->uploadFile($siteDto->getBanner()->getName());
					$siteDto->getBanner()->setName($fileName);
				}
				elseif($siteDto->getBanner()->getId()){
					$siteDto->getBanner()->setName($siteDto->getBanner()->getOldName());
				}
			}

			if($siteDto->getLogo()){
				if($siteDto->getLogo()->getName() != null){
					$uploadService = $this->getFileService();

					if($siteDto->getLogo()->getOldName() != null){
						$uploadService->deleteFile($siteDto->getLogo()->getOldName());
					}

					$fileName = $uploadService->uploadFile($siteDto->getLogo()->getName());
					$siteDto->getLogo()->setName($fileName);
				}
				elseif($siteDto->getLogo()->getId()){
					$siteDto->getLogo()->setName($siteDto->getLogo()->getOldName());
				}
			}

			$this->getSiteService()->update($siteDto);

			return $this->redirectToRoute('user-view-site');
		}

		return $this->render(':user/configuration/site:update-site.html.twig', ['site' => $siteForm->createView()]);
	}

	private function getSiteService(){
		return $this->get('site_service');
	}

	private function getFileService(){
		return $this->get('file_service');
	}

	private function isSiteExists(){
		return ($this->getSiteService()->findOne()) ? true: false;
	}
}