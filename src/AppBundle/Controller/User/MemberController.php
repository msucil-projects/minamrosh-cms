<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/5/2017
 * Time: 2:17 PM
 */

namespace AppBundle\Controller\User;


use AppBundle\Domain\DTO\MemberDTO;
use AppBundle\Domain\Form\MemberForm;
use AppBundle\Domain\Mapper\MemberMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MemberController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/member")
 */
class MemberController extends Controller {

	/**
	 * @Route("/", name="user-members")
	 */
	public function actionIndex(){
			return $this->render(':user/member:index.html.twig', [
				'members' => $this->getService()->findAllBy([])
			]);
	}

	/**
	 * @Route("/new", name="user-new-memeber")
	 *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function actionNew(Request $request){
		$memberDto = new MemberDTO();
		$memberForm = $this->createForm(MemberForm::class, $memberDto);

		$memberForm->handleRequest($request);

		if($memberForm->isSubmitted() && $memberForm->isValid()){
			if($memberDto->getImage()){
				$fileName = $this->getFileService()->uploadFile($memberDto->getImage()->getName());
				$memberDto->getImage()->setName($fileName);
			}

			$this->getService()->save($memberDto);

			return $this->redirectToRoute("user-members");
		}

		return $this->render(':user/member:create-member.html.twig', [
			'member' => $memberForm->createView()
		]);
	}

	/**
	 * @param Request $request
	 * @param $id
	 *
	 * @Route("/edit/{id}", name="user-update-member")
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function actionEdit(Request $request, $id){
		$member = $this->getService()->findOne(['id' => $id]);
		$memberDto = MemberMapper::mapToDto($member);
		$memberForm = $this->createForm(MemberForm::class, $memberDto);

		$memberForm->handleRequest($request);

		if($memberForm->isSubmitted() && $memberForm->isValid()){
			if($memberDto->getImage()){
				if($memberDto->getImage()->getName() != null){
					$uploadService = $this->get("file_service");

					if($memberDto->getImage()->getOldName() != null){
						$uploadService->deleteFile($memberDto->getImage()->getOldName());
					}

					$fileName = $uploadService->uploadFile($memberDto->getImage()->getName());
					$memberDto->getImage()->setName($fileName);
				}
				elseif($memberDto->getImage()->getId()){
					$memberDto->getImage()->setName($memberDto->getImage()->getOldName());
				}
			}

			$this->getService()->update($memberDto);

			return $this->redirectToRoute('user-members');
		}

		return $this->render(':user/member:update-member.html.twig', [
			'member' => $memberForm->createView()
		]);
	}


	/**
	 * @Route("/delete/{id}", name="user-delete-member")
	 * @Method(methods={"POST"})
	 * @param $id
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function actionDelete($id){

		$this->getService()->deleteById($id);
		return $this->redirectToRoute('user-members');
	}

	private function getService(){
		return $this->get('member_service');
	}

	private function getFileService(){
		return $this->get('file_service');
	}

}