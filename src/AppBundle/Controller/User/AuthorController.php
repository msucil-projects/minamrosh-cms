<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/5/2017
 * Time: 2:17 PM
 */

namespace AppBundle\Controller\User;


use AppBundle\Domain\DTO\AuthorDto;
use AppBundle\Domain\Form\AuthorForm;
use AppBundle\Domain\Mapper\AuthorMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MemberController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/author")
 */
class AuthorController extends Controller {

	/**
	 * @Route("/", name="user-authors")
	 */
	public function actionIndex(){
			return $this->render(':user/author:index.html.twig', [
				'authors' => $this->getService()->findAllBy([])
			]);
	}

	/**
	 * @Route("/new", name="user-new-author")
	 *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function actionNew(Request $request){
		$dto = new AuthorDto();
		$memberForm = $this->createForm(AuthorForm::class, $dto);

		$memberForm->handleRequest($request);

		if($memberForm->isSubmitted() && $memberForm->isValid()){
			if($dto->getImage()){
				$fileName = $this->getFileService()->uploadFile($dto->getImage()->getName());
				$dto->getImage()->setName($fileName);
			}

			$this->getService()->save($dto);

			return $this->redirectToRoute("user-authors");
		}

		return $this->render(':user/author:create-author.html.twig', [
			'author' => $memberForm->createView()
		]);
	}

	/**
	 * @param Request $request
	 * @param $id
	 *
	 * @Route("/edit/{id}", name="user-update-author")
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function actionEdit(Request $request, $id){
		$author = $this->getService()->findOne(['id' => $id]);
		$dto = AuthorMapper::mapToDto($author);
		$form = $this->createForm(AuthorForm::class, $dto);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			if($dto->getImage()){
				if($dto->getImage()->getName() != null){
					$uploadService = $this->get("file_service");

					if($dto->getImage()->getOldName() != null){
						$uploadService->deleteFile($dto->getImage()->getOldName());
					}

					$fileName = $uploadService->uploadFile($dto->getImage()->getName());
					$dto->getImage()->setName($fileName);
				}
				elseif($dto->getImage()->getId()){
					$dto->getImage()->setName($dto->getImage()->getOldName());
				}
			}

			$this->getService()->update($dto);

			return $this->redirectToRoute('user-authors');
		}

		return $this->render(':user/author:update-author.html.twig', [
			'author' => $form->createView()
		]);
	}


	/**
	 * @Route("/delete/{id}", name="user-delete-author")
	 * @Method(methods={"POST"})
	 * @param $id
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function actionDelete($id){

		$this->getService()->deleteById($id);
		return $this->redirectToRoute('user-authors');
	}

	private function getService(){
		return $this->get('author_service');
	}

	private function getFileService(){
		return $this->get('file_service');
	}

}