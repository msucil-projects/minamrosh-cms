<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 8:44 PM
 */

namespace AppBundle\Controller\User;
use AppBundle\Domain\DTO\PageSectionDTO;
use AppBundle\Domain\Form\SectionForm;
use AppBundle\Domain\Mapper\PageSectionMapper;
use AppBundle\Util\SectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SectionController
 * @package AppBundle\Controller\User
 * @Route("/user/section")
 */
class SectionController extends Controller
{
    /**
     * @Route("/", name="user-sections")
     */
    public function indexAction()
    {
        $sectionService = $this->getService();

        return $this->render(':user/section:index.html.twig', [
            "sections" => $sectionService->findAll(['section_type' => SectionType::SECTION])
        ]);
    }

    /**
     * @Route("/new", name="user-new-section")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $sectionDTO = new PageSectionDTO();
        $sectionForm = $this->createForm(SectionForm::class, $sectionDTO);
        $sectionForm->handleRequest($request);

        if ($sectionForm->isSubmitted() && $sectionForm->isValid()) {

            if($sectionDTO->getFeatureImage()){
                $uploadService = $this->getFileService();
                $fileName = $uploadService->uploadFile($sectionDTO->getFeatureImage()->getName());
                $sectionDTO->setFeatureImage($sectionDTO->getFeatureImage()->setName($fileName));
            }

            $this->getService()->save($sectionDTO);

            return $this->redirectToRoute("user-sections");
        }

        return $this->render(":user/section:create-section.html.twig", [
            'section' => $sectionForm->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @Route(name="user-update-section", path="/update/{id}")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {
        $section = $this->getService()->findById($id);

        $sectionDTO = PageSectionMapper::mapToDTO($section);

        $sectionForm = $this->createForm(SectionForm::class, $sectionDTO);

        $sectionForm->handleRequest($request);

        if ($sectionForm->isSubmitted() && $sectionForm->isValid()) {

            if($sectionDTO->getFeatureImage()){
                if($sectionDTO->getFeatureImage()->getName() != null){
                    $uploadService = $this->getFileService();

                    if($sectionDTO->getFeatureImage()->getOldName() != null){
                        $uploadService->deleteFile($sectionDTO->getFeatureImage()->getOldName());
                    }

                    $fileName = $uploadService->uploadFile($sectionDTO->getFeatureImage()->getName());
                    $sectionDTO->getFeatureImage()->setName($fileName);
                }
                elseif($sectionDTO->getFeatureImage()->getId()){
                    $sectionDTO->getFeatureImage()->setName($sectionDTO->getFeatureImage()->getOldName());
                }
            }

            $this->getService()->update($sectionDTO);

            return $this->redirectToRoute("user-sections");
        }

        return $this->render(":user/section:update-section.html.twig", [
            'section' => $sectionForm->createView()]);
    }

    /**
     *
     * @Route(name="user-delete-section", path="/delete/{id}")
     * @Method({"POST"})
     */
    public function deleteAction($id)
    {
        $this->getService()->deleteById($id);

        return $this->redirectToRoute("user-pages");
    }

    private function getService(){
        return $this->get("page_section_service");
    }

    private function getFileService(){
        return $this->get('file_service');
    }

}