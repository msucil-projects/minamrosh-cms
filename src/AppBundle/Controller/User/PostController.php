<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 3/31/17
 * Time: 8:14 PM
 */

namespace AppBundle\Controller\User;

use AppBundle\Domain\DTO\PostDTO;
use AppBundle\Domain\Form\PostForm;
use AppBundle\Domain\Mapper\DataTableMapper;
use AppBundle\Domain\Mapper\PostMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class PostController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/post")
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="user-posts")
     */
    public function indexAction()
    {
        return $this->render(':user/post:index.html.twig');
    }

    /**
     * @Route("/new", name="user-new-post")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $postDTO = new PostDTO();
        $postForm = $this->createForm(PostForm::class, $postDTO);

        $postForm->handleRequest($request);

        if ($postForm->isSubmitted() && $postForm->isValid()) {

            if($postDTO->getFeatureImage()){
                $uploadService = $this->get("file_service");
                $fileName = $uploadService->uploadFile($postDTO->getFeatureImage()->getName());
                $postDTO->setFeatureImage($postDTO->getFeatureImage()->setName($fileName));
            }
            $postService = $this->get("post_service");
            $post = $postService->save($postDTO);

            return $this->redirectToRoute("user-posts");
        }

        return $this->render(":user/post:create-post.html.twig", [
            'post' => $postForm->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route(name="user-update-post", path="/update/{id}")
     *
     */
    public function updateAction(Request $request, $id)
    {
        $post = $this->get("post_service")->findById($id);

        $postDTO = PostMapper::mapToDTO($post);

        $postForm = $this->createForm(PostForm::class, $postDTO);

        $postForm->handleRequest($request);

        if ($postForm->isSubmitted() && $postForm->isValid()) {

            if($postDTO->getFeatureImage()){
                if($postDTO->getFeatureImage()->getName() != null){
                    $uploadService = $this->get("file_service");

                    if($postDTO->getFeatureImage()->getOldName() != null){
                        $uploadService->deleteFile($postDTO->getFeatureImage()->getOldName());
                    }

                    $fileName = $uploadService->uploadFile($postDTO->getFeatureImage()->getName());
                    $postDTO->getFeatureImage()->setName($fileName);
                }
                elseif($postDTO->getFeatureImage()->getId()){
                    $postDTO->getFeatureImage()->setName($postDTO->getFeatureImage()->getOldName());
                }
            }
            $postService = $this->get("post_service");
            $postService->update($postDTO);

            return $this->redirectToRoute("user-posts");
        }

        return $this->render(":user/post:update-post.html.twig", [
            'post' => $postForm->createView()]);
    }

    /**
     * @Route(path="/view/{param}", name="user-view-post")
     * @param $param
     * @return Response
     */
    public function viewAction($param)
    {

        $postService = $this->get("post_service");

        if ($param instanceof IntegerType) {
            $post = $postService->findById($param);
        } else {
            $post = $postService->findBySlug($param);
        }

        if ($post == null) {
            throw new NotFoundHttpException("Post with pont found");
        }

        return new Response();
    }

    /**
     *
     * @Route(name="user-delete-post", path="/delete/{id}")
     * @Method({"POST"})
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $this->get('post_service')->deleteById($id);

        return $this->redirectToRoute("user-posts");
    }

    /**
     * @Route(name="user-datatable-post", path="/datatable")
     * @param Request $request
     * @return Response
     */
    public function dataTableAction(Request $request){
        $dataTableRequest = DataTableMapper::mapToDataTableRequestBody($request);
        return $this->get("post_service")->getPosts($dataTableRequest);
    }
}