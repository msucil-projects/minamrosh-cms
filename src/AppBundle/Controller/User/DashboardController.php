<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 3/31/17
 * Time: 8:21 PM
 */

namespace AppBundle\Controller\User;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DashboardController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/dashboard", name="user-dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/", name="user-dashboard")
     */
    public function indexAction(){
        return $this->render(':user/dashboard:index.html.twig', [
            'recentPosts' => $this->getService()->getRecentPost(null),
            'pages' => $this->getService()->getPages(),
            'ads' => $this->getService()->getAdvertisements(null),
            'expiredAds' => $this->getService()->getExpiredAdvertisements(),
            'clients' => $this->getService()->getAdvertisementClients()
        ]);
    }

    private function getService(){
        return $this->get('dashboard_service');
    }

}