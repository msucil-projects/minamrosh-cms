<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 9:48 PM
 */

namespace AppBundle\Controller\User;


use AppBundle\Domain\DTO\SectionCategoryDTO;
use AppBundle\Domain\Form\SectionCategoryForm;
use AppBundle\Domain\Mapper\SectionCategoryMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SectionCategoryController
 * @package AppBundle\Controller\User
 * @Route("/user/section-category")
 */
class SectionCategoryController extends Controller
{
    /**
     * @Route("/",  name="user-section-category")
     */
    public function indexAction()
    {
        return $this->render(':user/section-category:index.html.twig',
            ['categories' => $this->getService()->findAll()]);
    }

    /**
     * @param Request $request
     * @Route("/new", name="user-new-section-category")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionNew(Request $request)
    {
        $category = new SectionCategoryDTO();

        $categoryForm = $this->createForm(SectionCategoryForm::class, $category);

        $categoryForm->handleRequest($request);

        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
            $savedCategory = $this->getService()->save($category);
            if ($savedCategory) {
                return $this->redirectToRoute('user-section-category');
            }
        }

        return $this->render(':user/section-category:create-section-category.html.twig', [
            'category' => $categoryForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @Route("/edit/{id}", name="user-update-section-category")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionUpdate(Request $request, $id)
    {

        $category = $this->getService()->findOneBy(['id' => $id, 'editable' => true]);

        if(!$category){
            throw new NotFoundHttpException("The category you are trying to edit does not exist!");
        }

        $categoryDTO = SectionCategoryMapper::mapToDTO($category);

        $categoryForm = $this->createForm(SectionCategoryForm::class, $categoryDTO);

        $categoryForm->handleRequest($request);

        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
            $updatedCategory = $this->getService()->update($categoryDTO);
            if ($updatedCategory) {
                return $this->redirectToRoute('user-section-category');
            }
        }

        return $this->render(':user/section-category:update-section-category.html.twig', [
            'category' => $categoryForm->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="user-delete-section-category")
     * @Method(methods={"POST"})
     */
    public function actionDelete($id){
        $this->getService()->delete($id);

        return $this->redirectToRoute("user-section-category");
    }

    private function getService()
    {
        return $this->get('section_category_service');
    }

}