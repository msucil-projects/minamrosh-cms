<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 3/31/17
 * Time: 8:14 PM
 */

namespace AppBundle\Controller\User;

use AppBundle\Domain\DTO\PostCategoryDTO;
use AppBundle\Domain\Form\PostCategoryForm;
use AppBundle\Domain\Mapper\DataTableMapper;
use AppBundle\Domain\Mapper\PostCategoryMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class PostCategoryController
 * @package AppBundle\Controller\User
 *
 * @Route("/user/post-category")
 */
class PostCategoryController extends Controller
{
    /**
     * @Route("/", name="user-post-categories")
     */
    public function indexAction()
    {
        $categoryService = $this->get("post_category_service");

        return $this->render(':user/post-category:index.html.twig', [
            "categories" => $categoryService->findAll()
        ]);
    }

    /**
     * @Route("/new", name="user-new-post-category")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $categoryDTO = new PostCategoryDTO();

        $category = $this->createForm(PostCategoryForm::class, $categoryDTO);
        $category->add('parent', ChoiceType::class,[
            'label' => 'Parent Category',
            'required' => false,
            'choices' => $this->get('post_category_option')->getCategoryOption(0),
            'placeholder' => 'Select Parent Category'
        ])
            ->add("save", SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-sm btn-default btn-flat"
                ]
            ]);

        $category->handleRequest($request);

        if ($category->isSubmitted() && $category->isValid()) {

            $postService = $this->get("post_category_service");
            $postService->save($categoryDTO);

            return $this->redirectToRoute("user-post-categories");
        }

        return $this->render(":user/post-category:create-post-category.html.twig", [
            'category' => $category->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route(name="user-update-post-category", path="/update/{id}")
     *
     */
    public function updateAction(Request $request, $id)
    {
        $category = $this->get("post_category_service")->findById($id);

        $categoryDTO = PostCategoryMapper::mapToDTO($category);

        $categoryForm = $this->createForm(PostCategoryForm::class, $categoryDTO);
        $categoryForm->add('parent', ChoiceType::class,[
            'label' => 'Parent Category',
            'required' => false,
            'choices' => $this->get('post_category_option')->getCategoryOption($category->getId()),
            'placeholder' => 'Select Parent Category'
        ])
            ->add("save", SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-sm btn-default btn-flat"
                ]
            ]);;

        $categoryForm->handleRequest($request);

        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {


            $categoryService = $this->get("post_category_service");
            $categoryService->update($categoryDTO);

            return $this->redirectToRoute("user-post-categories");
        }

        return $this->render(":user/post-category:update-post-category.html.twig", [
            'category' => $categoryForm->createView()]);
    }

    /**
     * @Route(path="/view/{param}", name="user-view-post-category")
     * @param $param
     * @return Response
     */
    public function viewAction($param)
    {

        $postService = $this->get("post_service");

        if ($param instanceof IntegerType) {
            $post = $postService->findById($param);
        } else {
            $post = $postService->findBySlug($param);
        }

        if ($post == null) {
            throw new NotFoundHttpException("Post with pont found");
        }

        return new Response();
    }

    /**
     *
     * @Route(name="user-delete-post-category", path="/delete/{id}")
     * @Method({"POST"})
     */
    public function deleteAction($id)
    {
        $this->get('post_category_service')->deleteById($id);

       return  $this->redirectToRoute('user-post-categories');
    }

    /**
     * @Route(name="user-datatable-post", path="/datatable")
     * @param Request $request
     * @return Response
     */
    public function dataTableAction(Request $request){
        $dataTableRequest = DataTableMapper::mapToDataTableRequestBody($request);
        return $this->get("post_service")->getPosts($dataTableRequest);
    }
}