<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/12/17
 * Time: 1:45 AM
 */

namespace AppBundle\Controller\User;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MediaController extends Controller
{

    /**
     * @Route("/media/delete-file")
     * @Method({"POST"})
     * @param Request $request
     */
    public function deleteFile(Request $request){
        $fileName = $request->request->get('name');
        $this->get('file_service')->deleteFile($fileName);
        return new JsonResponse();
    }

}