<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 7/23/2017
 * Time: 8:41 PM
 */

namespace AppBundle\Controller;


use AppBundle\Util\StringHelper;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class CustomExceptionController extends ExceptionController {

	public function showAction( Request $request, FlattenException $exception, DebugLoggerInterface $logger = null ) {
		$currentContent = $this->getAndCleanOutputBuffering($request->headers->get('X-Php-Ob-Level', -1));
		$showException = $request->attributes->get('showException', $this->debug);
		$code = $exception->getStatusCode();

//		die(dump($request->getPathInfo()));
		if(StringHelper::startsWith($request->getPathInfo(), '/user')){


			return new Response($this->twig->render(':user:exception.html.twig',
				array(
					'status_code' => $code,
					'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
					'exception' => $exception,
					'logger' => $logger,
					'currentContent' => $currentContent,
				)
			));
		}

		return new Response($this->twig->render(':public:exception.html.twig',
			array(
				'status_code' => $code,
				'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
				'exception' => $exception,
				'logger' => $logger,
				'currentContent' => $currentContent,
			)
		));
//		return parent::showAction( $request, $exception, $logger ); // TODO: Change the autogenerated stub
	}
}