<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/14/17
 * Time: 10:35 PM
 */

namespace AppBundle\Controller\Frontend;


use AppBundle\Event\ViewTracking\PostViewEvent;
use AppBundle\Event\ViewTracking\ViewTrackingEvents;
use AppBundle\Twig\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class FrontPostController
 * @package AppBundle\Controller\Frontend
 *
 * @Route("/news")
 */
class FrontPostController extends Controller
{
	/**
	 * @Route("/{category}/{page}", name="post-category", requirements={"page": "\d+"})
	 *
	 * @param $category
	 * @param int $page
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function categoryAction($category, $page = 1){
        $limit = 10;
        $posts = $this->getService()->getPostsByCategory($category, $this->getParameter('items_per_category'), $page);

        if(!$posts->getIterator()->valid()){
            throw  new NotFoundHttpException("The Requested Page does not exists!");
        }

        $pagination = new Pagination(
            'post-category',
            ['category' => $category],
            $page,
            $limit,
            $posts->count()
        );

        return $this->render(':public:category.html.twig', ['posts' => $posts, 'pagination' => $pagination]);
    }

    /**
     * @Route("/{category}/{slug}", name="post-detail")
     *
     * @param $category
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction($category, $slug){
        $post = $this->getService()->getPostByCategory($category, $slug);

        if(!$post){
            throw new NotFoundHttpException("The Requested Page does not exists!");
        }

        $event = new PostViewEvent($post);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(ViewTrackingEvents::POST_VIEWED, $event);

        $posts = $this->getService()->getRelatedPosts($category, $this->getParameter('items_per_detail_page'), $post->getId());

        return $this->render(':public:single.html.twig', ['post' => $post, 'posts' => $posts]);
    }

    private function getService(){
        return $this->get('front_post_service');
    }

}