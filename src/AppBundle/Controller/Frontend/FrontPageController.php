<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/11/17
 * Time: 6:33 PM
 */

namespace AppBundle\Controller\Frontend;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PageController
 * @package AppBundle\Controller\Frontend
 *
 * @Route("/page")
 */
class FrontPageController extends Controller
{
    private $message = "The requested page does not exist!";
    /**
     * @Route("/about-us", name="about-us")
     */
    public function aboutUsAction(){
        $page = $this->getPage('about-us');
        $members = $this->get('member_service')->findAllBy([], ['displayOrder' => 'asc', 'designation' => 'asc', 'fullName' => 'asc']);

        if($page){
            return $this->render(':public:about-us.html.twig', [
                'page' => $page,
	            'members' => $members
            ]);
        }
        else {
            throw new NotFoundHttpException($this->message);
        }
    }

    /**
     * @Route("/contact-us", name="contact-us")
     */
    public function contactUsPage(){
        $page = $this->getPage('contact-us');
        if($page){
            return $this->render(':public:contact-us.html.twig', ['page' => $page]);
        }
        else {
            throw new NotFoundHttpException($this->message);
        }
    }

    /**
     * @Route("/{category}", name="page")
     *
     * @param $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($category){
        $page = $this->getPage($category);

        if($page){
            return $this->render(':public:page.html.twig', [
                'page' => $page
            ]);
        }
        else {
            throw new NotFoundHttpException($this->message);
        }
    }

    private function getPage($category){
        return $this->get('front_page_service')->getPage($category);
    }
}