<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/29/17
 * Time: 1:46 AM
 */

namespace AppBundle\Controller\Frontend;


use AppBundle\Twig\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontSearchController extends Controller
{
    /**
     * @param $request
     *
     * @Route("/search", name="search")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request){
        $term = $request->get('term');

        if(!empty($term))
        {
            return $this->redirectToRoute('search-result', ['term' => $term]);
        }

        return $this->render(':public:search.html.twig');
    }

    /**
     * @param $term
     * @param int $page
     *
     * @Route("/search/{term}/{page}", name="search-result", requirements={"page": "\d+"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchResultAction($term, $page = 1){
        $limit = $this->getParameter('items_per_page');
        $posts = $this->getService()->search($term, $limit, $page);

        if(!$posts->getIterator()->valid()){
            throw  new NotFoundHttpException("The Requested Page does not exists!");
        }

        $pagination = new Pagination(
            'search-result',
            ['term' => $term],
            $page,
            $limit,
            $posts->count()
        );

        return $this->render(':public:search.html.twig', ['term' => $term, 'posts' => $posts, 'pagination' => $pagination]);
    }

    private function getService(){
        return $this->get('front_post_service');
    }

}