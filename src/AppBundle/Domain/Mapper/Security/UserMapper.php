<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/5/2017
 * Time: 8:47 PM
 */

namespace AppBundle\Domain\Mapper\Security;


use AppBundle\Domain\DTO\Security\ProfileDto;
use AppBundle\Domain\DTO\Security\UpdateUser;
use AppBundle\Domain\Entity\Security\User;

class UserMapper {

	public static function mapUpdateUserToUser(UpdateUser $updateUser){
		$user = new User();
		$user->setFirstName($updateUser->getFirstName())
			->setUsername($updateUser->getUsername())
			->setLastName($updateUser->getLastName())
			->setEmail($updateUser->getEmail())
			->setId($updateUser->getId());
            if($updateUser->getPassword()){
                $user->setPassword($updateUser->getPassword());
            }
		return $user;
	}

	public static function mapUserToUpdateUser(User $user){
		$updateUser = new UpdateUser();
		$updateUser->setId($user->getId())
			->setUsername($user->getUsername())
			->setFirstName($user->getFirstName())
			->setLastName($user->getLastName())
            ->setRole($user->getRole()->getId())
			->setEmail($user->getEmail());

		return $updateUser;
	}

	public static function mapToProfile(User $user){
	    $profile = new ProfileDto();
	    $profile->setUsername($user->getUsername())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setRole($user->getRole()->getName());

	    return $profile;
    }

}