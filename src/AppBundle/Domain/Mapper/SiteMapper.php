<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/1/2017
 * Time: 12:24 AM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\SiteDTO;
use AppBundle\Domain\Entity\Site;

class SiteMapper {

	public static function mapToEntity(SiteDTO $dto) {
		$entity = new Site();
		$entity->setId($dto->getId())
			->setName($dto->getName())
			->setEmail($dto->getEmail())
			->setPhoneNo($dto->getPhoneNo())
			->setMobileNo($dto->getMobileNo())
			->setFaxNo($dto->getFaxNo())
			->setAddress($dto->getAddress())
			->setFacebookLink($dto->getFacebookLink())
			->setTwitterLink($dto->getTwitterLink())
			->setGooglePlusLink($dto->getGooglePlusLink())
			->setLinkedinLink($dto->getLinkedinLink())
			->setGoogleAnalyticLink($dto->getGoogleAnalyticLink())
			->setGoogleAnalyticScript($dto->getGoogleAnalyticScript())
			->setEnabled($dto->isEnabled())
			->setVersion($dto->getVersion());

		$banner = $dto->getBanner();
		if ($banner && $banner->getName()) {
			$entity->setBanner(MediaMapper::mapToEntity($dto->getBanner()));
			$entity->getBanner()->setCreatedAt(new \DateTime());
		}

		$logo = $dto->getLogo();
		if ($logo && $logo->getName()) {
			$entity->setLogo(MediaMapper::mapToEntity($dto->getLogo()));
			$entity->getLogo()->setCreatedAt(new \DateTime());
		}

		if($dto->getMeta()){
			$entity->setMeta(MetaMapper::mapToEntity($dto->getMeta()));
		}

		return $entity;
	}

	public static function mapToDTO(Site $entity) {
		$dto = new SiteDTO();
		$dto->setId($entity->getId())
			->setName($entity->getName())
			->setEmail($entity->getEmail())
			->setPhoneNo($entity->getPhoneNo())
			->setMobileNo($entity->getMobileNo())
			->setFacebookLink($entity->getFaxNo())
			->setAddress($entity->getAddress())
			->setFacebookLink($entity->getFacebookLink())
			->setTwitterLink($entity->getTwitterLink())
			->setGooglePlusLink($entity->getGooglePlusLink())
			->setLinkedinLink($entity->getLinkedinLink())
			->setGoogleAnalyticScript($entity->getGoogleAnalyticScript())
			->setGoogleAnalyticLink($entity->getGoogleAnalyticLink())
			->setEnabled($entity->getEnabled())
			->setVersion($entity->getVersion());

		if($entity->getBanner()){
			$dto->setBanner(MediaMapper::mapToDTO($entity->getBanner()));
		}
		if($entity->getLogo()){
			$dto->setLogo(MediaMapper::mapToDTO($entity->getLogo()));
		}

		if($entity->getMeta()){
			$dto->setMeta(MetaMapper::mapToDTO($entity->getMeta()));
		}

		return $dto;
	}

}