<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/10/17
 * Time: 5:36 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\PageDTO;
use AppBundle\Domain\Entity\Page;
use AppBundle\Domain\Entity\SectionCategory;
use AppBundle\Util\StringHelper;

class PageMapper
{

    public static function mapToDTO(Page $page)
    {
        $dto = new PageDTO();
        $dto->setId($page->getId())
            ->setTitle($page->getTitle())
            ->setContent($page->getContent())
            ->setStatus($page->getStatus())
            ->setCategory($page->getCategory()->getId())
            ->setVersion($page->getVersion());
        if ($page->getFeatureImage()) {
            $dto->setFeatureImage(MediaMapper::mapToDTO($page->getFeatureImage()));
        }

        if($page->getMeta()){
            $dto->setMeta(MetaMapper::mapToDTO($page->getMeta()));
        }

        return $dto;
    }

    public static function mapToEntity(PageDTO $dto)
    {
        $category = new SectionCategory();
        $category->setId($dto->getCategory());

        $post = new Page();
        $post->setId($dto->getId())
            ->setTitle($dto->getTitle())
            ->setContent($dto->getContent())
            ->setSlug(StringHelper::generateSlug($dto->getTitle()))
            ->setStatus($dto->getStatus())
            ->setCategory($category)
            ->setVersion($dto->getVersion());

        $featureImage = $dto->getFeatureImage();
        if ($featureImage && $featureImage->getName()) {
            $post->setFeatureImage(MediaMapper::mapToEntity($dto->getFeatureImage()));
            $post->getFeatureImage()->setCreatedAt(new \DateTime());
        }

        if($dto->getMeta()){
            $post->setMeta(MetaMapper::mapToEntity($dto->getMeta()));
        }

        return $post;
    }
}