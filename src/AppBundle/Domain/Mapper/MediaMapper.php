<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/9/17
 * Time: 10:33 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\MediaDTO;
use AppBundle\Domain\Entity\Media;

class MediaMapper
{
    public static function mapToDTO(Media $media){
       $mediaDTO = new MediaDTO();
       $mediaDTO->setId($media->getId())
           ->setName($media->getName())
           ->setVersion($media->getVersion())
           ->setType($media->getType())
           ->setCreatedAt($media->getCreatedAt())
           ->setUpdatedAt($media->getUpdatedAt())
           ->setCredit($media->getCredit());

       if($media->getName()){
           $mediaDTO->setOldName($media->getName());
       }

       return $mediaDTO;
    }

    public static function mapToEntity(MediaDTO $mediaDTO){
        $media = new Media();
        $media->setId($mediaDTO->getId())
            ->setName($mediaDTO->getName())
            ->setType($mediaDTO->getType())
            ->setVersion($mediaDTO->getVersion())
            ->setCreatedAt($mediaDTO->getCreatedAt())
            ->setUpdatedAt($mediaDTO->getUpdatedAt())
            ->setCredit($mediaDTO->getCredit());

        return $media;
    }

}