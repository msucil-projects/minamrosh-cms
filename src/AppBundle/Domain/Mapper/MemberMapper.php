<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:31 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\MemberDTO;
use AppBundle\Domain\Entity\Member;

class MemberMapper {

	public static function mapToEntity( MemberDTO $dto ) {
		$entity = new Member();

		$entity->setId( $dto->getId() )
		       ->setFullName( $dto->getFullName() )
		       ->setEmail( $dto->getEmail() )
		       ->setBio( $dto->getBio() )
		       ->setPhoneNo( $dto->getPhoneNo() )
		       ->setMobileNo( $dto->getMobileNo() )
		       ->setFacebookLink( $dto->getFacebookLink() )
		       ->setTwitterLink( $dto->getTwitterLink() )
		       ->setLinkedinLink( $dto->getLinkedinLink() )
		       ->setGooglePlusLink( $dto->getGoogleplusLink() )
		       ->setDesignation( $dto->getDesignation() )
		       ->setDisplayOrder( $dto->getDisplayOrder() )
		       ->setVersion( $dto->getVersion() );

		if ( $dto->getImage() && $dto->getImage()->getName() ) {
			$entity->setImage( MediaMapper::mapToEntity( $dto->getImage() ) );
			$entity->getImage()->setCreatedAt( new \DateTime() );
		}

		return $entity;
	}

	public static function mapToDto( Member $entity ) {
		$dto = new MemberDTO();

		$dto->setId( $entity->getId() )
		    ->setFullName( $entity->getFullName() )
		    ->setEmail( $entity->getEmail() )
		    ->setBio( $entity->getBio() )
		    ->setPhoneNo( $entity->getPhoneNo() )
		    ->setMobileNo( $entity->getMobileNo() )
		    ->setFacebookLink( $entity->getFacebookLink() )
		    ->setTwitterLink( $entity->getTwitterLink() )
		    ->setLinkedinLink( $entity->getLinkedinLink() )
		    ->setGooglePlusLink( $entity->getGoogleplusLink() )
		    ->setDesignation( $entity->getDesignation() )
		    ->setDisplayOrder( $entity->getDisplayOrder() )
		    ->setVersion( $entity->getVersion() );

		if ( $entity->getImage() ) {
			$dto->setImage( MediaMapper::mapToDTO( $entity->getImage() ) );
		}

		return $dto;
	}

}