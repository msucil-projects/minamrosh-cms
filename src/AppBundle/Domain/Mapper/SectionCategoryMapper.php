<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 8:48 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\SectionCategoryDTO;
use AppBundle\Domain\Entity\SectionCategory;

class SectionCategoryMapper
{
    public static function mapToDTO(SectionCategory $entity){
        $dto = new SectionCategoryDTO();
        $dto->setId($entity->getId())
            ->setName($entity->getName())
            ->setDescription($entity->getDescription())
            ->setSection($entity->isSection())
            ->setEditable($entity->getEditable())
            ->setVersion($entity->getVersion());

        return $dto;
    }

    public static function mapToEntity(SectionCategoryDTO $dto){
        $entity = new SectionCategory();
        $entity->setId($dto->getId())
            ->setName($dto->getName())
            ->setDescription($dto->getDescription())
            ->setVersion($dto->getVersion())
            ->setEditable($dto->getEditable())
            ->setSection($dto->getSection());

        return $entity;
    }
}