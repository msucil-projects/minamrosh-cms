<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 3/5/18
 * Time: 10:26 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\AuthorDto;
use AppBundle\Domain\Entity\Author;

class AuthorMapper
{
    public static function mapToDTO(Author $entity): AuthorDto {
        $dto = new AuthorDto();
        $dto->setId($entity->getId())
            ->setName($entity->getName())
            ->setBio($entity->getBio())
            ->setEmail($entity->getEmail())
            ->setFacebookLink($entity->getFacebookLink())
            ->setTwitterLink($entity->getTwitterLink())
            ->setLinkedinLink($entity->getLinkedinLink())
            ->setVersion($entity->getVersion());

        if ( $entity->getImage() ) {
            $dto->setImage( MediaMapper::mapToDTO( $entity->getImage() ) );
        }

        return $dto;
    }

    public static function mapToEntity(AuthorDto $dto): Author {
        $entity = new Author();
        $entity->setId($dto->getId())
            ->setName($dto->getName())
            ->setBio($dto->getBio())
            ->setEmail($dto->getEmail())
            ->setImage($dto->getImage())
            ->setFacebookLink($dto->getFacebookLink())
            ->setTwitterLink($dto->getTwitterLink())
            ->setLinkedinLink($dto->getLinkedinLink())
            ->setVersion($dto->getVersion());

        if ( $dto->getImage() && $dto->getImage()->getName() ) {
            $entity->setImage( MediaMapper::mapToEntity( $dto->getImage() ) );
            $entity->getImage()->setCreatedAt( new \DateTime() );
        }

        return $entity;
    }
}