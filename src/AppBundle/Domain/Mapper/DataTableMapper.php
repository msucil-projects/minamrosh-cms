<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/12/17
 * Time: 11:02 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\DataTableRequestBody;
use AppBundle\Domain\DTO\DataTableResponseBody;
use Symfony\Component\HttpFoundation\Request;

class DataTableMapper
{
    public static function mapToDataTableRequestBody(Request $request){
        $dataTableRequestBody = new DataTableRequestBody();

        $start = ($request->request->get("start") == null) ? 0 : $request->request->get("start");
        $dataTableRequestBody->setStart($start)
            ->setDraw($request->request->get("draw"))
            ->setLength($request->request->get("length"))
            ->setOrder($request->request->get("order"))
            ->setSearch($request->request->get("search"))
            ->setColumns($request->request->get("columns"));

        return $dataTableRequestBody;
    }

    public static function mapToDataTableResponseBody(array $result){
        $dataTableResponse = new DataTableResponseBody();
        $dataTableResponse->setDraw($result["draw"])
            ->setRecordsFiltered($result["recordsFiltered"])
            ->setRecordsTotal($result["recordsTotal"])
            ->setData($result["data"]);

        return $dataTableResponse;
    }

}