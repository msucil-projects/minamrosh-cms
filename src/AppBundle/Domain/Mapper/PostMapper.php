<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/10/17
 * Time: 5:36 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\PostDTO;
use AppBundle\Domain\Entity\Author;
use AppBundle\Domain\Entity\Post;
use AppBundle\Domain\Entity\PostCategory;
use AppBundle\Util\StringHelper;


class PostMapper
{

    public static function mapToDTO(Post $post)
    {
        $dto = new PostDTO();
        $dto->setId($post->getId())
            ->setTitle($post->getTitle())
            ->setContent($post->getContent())
            ->setStatus($post->getStatus())
            ->setCategoryId($post->getCategory()->getId())
            ->setVersion($post->getVersion())
	        ->setFeaturePost($post->getFeaturePost());
        if ($post->getFeatureImage()) {
            $dto->setFeatureImage(MediaMapper::mapToDTO($post->getFeatureImage()));
        }

        if($post->getAuthor()) {
            $dto->setAuthor($post->getAuthor()->getId());
        }

        if($post->getMeta()){
            $dto->setMeta(MetaMapper::mapToDTO($post->getMeta()));
        }

        return $dto;
    }

    public static function mapToEntity(PostDTO $dto)
    {
        $category = new PostCategory();
        $category->setId($dto->getCategoryId());

        $post = new Post();
        $post->setId($dto->getId())
            ->setTitle($dto->getTitle())
            ->setContent($dto->getContent())
            ->setSlug(StringHelper::generateSlug($dto->getTitle()))
            ->setStatus($dto->getStatus())
            ->setCategory($category)
	        ->setFeaturePost($dto->getFeaturePost())
            ->setVersion($dto->getVersion());

        $featureImage = $dto->getFeatureImage();
        if ($featureImage && $featureImage->getName()) {
            $post->setFeatureImage(MediaMapper::mapToEntity($dto->getFeatureImage()));
            $post->getFeatureImage()->setCreatedAt(new \DateTime());
        }

        if($dto->getMeta()){
            $post->setMeta(MetaMapper::mapToEntity($dto->getMeta()));
        }

        if($dto->getAuthor() != 0) {
            $author = new Author();
            $author->setId($dto->getAuthor());
            $post->setAuthor($author);
        }
        return $post;
    }
}