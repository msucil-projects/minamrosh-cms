<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 8:01 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\PostCategoryDTO;
use AppBundle\Domain\Entity\PostCategory;

class PostCategoryMapper
{

    public static function mapToEntity(PostCategoryDTO $dto)
    {
        $entity = new PostCategory();
        $entity->setId($dto->getId())
            ->setName($dto->getName())
            ->setVersion($dto->getVersion())
            ->setDisplayOrder($dto->getDisplayOrder())
            ->setDescription($dto->getDescription());

        if($dto->getParent()){
            $category = new PostCategory();
            $category->setId($dto->getParent());

            $entity->setParent($category);
        }

        return $entity;
    }

    public static function mapToDTO(PostCategory $entity)
    {
        $dto = new PostCategoryDTO();
        $dto->setId($entity->getId())
            ->setName($entity->getName())
            ->setVersion($entity->getVersion())
            ->setDisplayOrder($entity->getDisplayOrder())
            ->setDescription($entity->getDescription());

        if($entity->getParent()){
            $dto->setParent($entity->getParent()->getId());
        }

        return $dto;
    }
}