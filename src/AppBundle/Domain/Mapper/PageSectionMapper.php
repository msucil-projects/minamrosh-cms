<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 11:24 AM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\PageSectionDTO;
use AppBundle\Domain\Entity\Page;
use AppBundle\Domain\Entity\PageSection;
use AppBundle\Domain\Entity\SectionCategory;

class PageSectionMapper
{
    public static function mapToDTO(PageSection $entity){
        $dto = new PageSectionDTO();
        $dto->setId($entity->getId())
            ->setTitle($entity->getTitle())
            ->setFeatureImage($entity->getFeatureImage())
            ->setContent($entity->getContent())
            ->setStatus($entity->getStatus())
            ->setVersion($entity->getVersion())
            ->setCategory($entity->getCategory()->getId());

        if($entity->getPage()){
            $dto->setPage($entity->getPage()->getId());
        }

        if($entity->getCategory()){
            $dto->setCategory($entity->getCategory()->getId());
        }

        if ($entity->getFeatureImage()) {
            $dto->setFeatureImage(MediaMapper::mapToDTO($entity->getFeatureImage()));
        }

        return $dto;
    }
    public static function mapToEntity(PageSectionDTO $dto){
        $entity = new PageSection();
        $entity->setId($dto->getId())
            ->setTitle($dto->getTitle())
            ->setContent($dto->getContent())
            ->setFeatureImage($dto->getFeatureImage())
            ->setStatus($dto->getStatus())
            ->setVersion($dto->getVersion());

        if($dto->getPage()){
            $page = new Page();
            $page->setId($dto->getPage());
            $entity->setPage($page);
        }

        if($dto->getCategory()){
            $category = new SectionCategory();
            $category->setId($dto->getCategory());

            $entity->setCategory($category);
        }

        $featureImage = $dto->getFeatureImage();
        if ($featureImage && $featureImage->getName()) {
            $entity->setFeatureImage(MediaMapper::mapToEntity($dto->getFeatureImage()));
            $entity->getFeatureImage()->setCreatedAt(new \DateTime('now'));
        }

        return $entity;
    }

}