<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/13/17
 * Time: 11:27 PM
 */

namespace AppBundle\Domain\Mapper;


use AppBundle\Domain\DTO\MetaDTO;
use AppBundle\Domain\Entity\Meta;

class MetaMapper
{
    public static function mapToDTO(Meta $entity){
        $dto = new MetaDTO();
        $dto->setId($entity->getId())
            ->setTitle($entity->getTitle())
            ->setKeywords($entity->getKeywords())
            ->setDescription($entity->getDescription())
            ->setVersion($entity->getVersion());

        return $dto;
    }

    public static function mapToEntity(MetaDTO $dto){
        $entity = new Meta();
        $entity->setId($dto->getId())
            ->setTitle($dto->getTitle())
            ->setKeywords($dto->getKeywords())
            ->setDescription($dto->getDescription())
            ->setVersion($dto->getVersion());

        return $entity;
    }
}