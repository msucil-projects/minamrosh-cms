<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 11:21 PM
 */

namespace AppBundle\Domain\Mapper\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdClientDTO;
use AppBundle\Domain\Entity\Advertisement\AdClient;

class AdClientMapper
{
    public static function mapToDTO(AdClient $entity){
        $dto = new AdClientDTO();
        $dto->setId($entity->getId())
            ->setFullName($entity->getFullName())
            ->setEmail($entity->getEmail())
            ->setPhoneNo($entity->getPhoneNo())
            ->setMobileNo($entity->getMobileNo())
            ->setDescription($entity->getDescription())
            ->setVersion($entity->getVersion())
            ->setCreatedBy($entity->getCreatedBy())
            ->setCreatedAt($entity->getCreatedAt());

        return $dto;
    }

    public static function mapToEntity(AdClientDTO $dto){
        $entity = new AdClient();
        $entity->setId($dto->getId())
            ->setFullName($dto->getFullName())
            ->setEmail($dto->getEmail())
            ->setPhoneNo($dto->getPhoneNo())
            ->setMobileNo($dto->getMobileNo())
            ->setDescription($dto->getDescription())
            ->setVersion($dto->getVersion())
            ->setCreatedBy($dto->getCreatedBy())
            ->setCreatedAt($dto->getCreatedAt());

        return $entity;
    }

}