<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/26/17
 * Time: 9:41 AM
 */

namespace AppBundle\Domain\Mapper\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdDTO;
use AppBundle\Domain\DTO\Advertisement\AdSectionDTO;
use AppBundle\Domain\Entity\Advertisement\Ad;
use AppBundle\Domain\Entity\Advertisement\AdClient;
use AppBundle\Domain\Entity\Advertisement\AdSection;
use AppBundle\Domain\Mapper\MediaMapper;
use Symfony\Component\HttpKernel\Client;
use Symfony\Component\Stopwatch\Section;

class AdMapper
{
    public static function mapToDTO(Ad $entity){
        $dto = new AdDTO();
        $dto->setId($entity->getId())
            ->setDescription($entity->getDescription())
            ->setClient($entity->getClient()->getId())
            ->setSection($entity->getSection()->getId())
            ->setStatus($entity->getStatus())
            ->setExpired($entity->getExpired())
            ->setValidFrom($entity->getValidFrom())
            ->setValidTo($entity->getValidTo())
            ->setAdUrl($entity->getAdUrl())
            ->setVersion($entity->getVersion());

        if ($entity->getImage()) {
            $dto->setImage(MediaMapper::mapToDTO($entity->getImage()));
        }

        return $dto;
    }

    public static function mapToEntity(AdDTO $dto){
        $section = new AdSection();
        $section->setId($dto->getSection());

        $client = new AdClient();
        $client->setId($dto->getClient());

        $entity = new Ad();
        $entity->setId($dto->getId())
            ->setDescription($dto->getDescription())
            ->setClient($client)
            ->setSection($section)
            ->setStatus($dto->getStatus())
            ->setExpired($dto->getExpired())
            ->setValidFrom($dto->getValidFrom())
            ->setValidTo($dto->getValidTo())
            ->setAdUrl($dto->getAdUrl())
            ->setVersion($dto->getVersion());

        $image = $dto->getImage();
        if ($image && $image->getName()) {
            $entity->setImage(MediaMapper::mapToEntity($dto->getImage()));
            $entity->getImage()->setCreatedAt(new \DateTime());
        }

        return $entity;
    }

}