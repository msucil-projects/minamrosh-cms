<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/25/17
 * Time: 10:40 PM
 */

namespace AppBundle\Domain\Mapper\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdSectionDTO;
use AppBundle\Domain\Entity\Advertisement\AdSection;

class AdSectionMapper
{

    public static function mapToDTO(AdSection $entity){
        $dto = new AdSectionDTO();
        $dto->setId($entity->getId())
            ->setName($entity->getName())
            ->setDescription($entity->getDescription())
            ->setVersion($entity->getVersion())
            ->setHeight($entity->getHeight())
            ->setEditable($entity->getEditable())
            ->setWidth($entity->getWidth());

        return $dto;
    }

    public static function mapToEntity(AdSectionDTO $dto){
        $entity = new AdSection();
        $entity->setId($dto->getId())
            ->setName($dto->getName())
            ->setDescription($dto->getDescription())
            ->setVersion($dto->getVersion())
            ->setEditable($dto->isEditable())
            ->setHeight($dto->getHeight())
            ->setWidth($dto->getWidth());

        return $entity;
    }

}