<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/17/17
 * Time: 10:03 PM
 */

namespace AppBundle\Domain\PropertyOption;


use AppBundle\Repository\PostCategoryRepository;
use AppBundle\Repository\SectionCategoryRepository;
use AppBundle\Util\StringHelper;

class SectionCategoryOption
{
    private $categoryRepository;

    private $pageOption = [];

    private $sectionOption = [];

    public function __construct(SectionCategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    private function generatePageOption(){
        if(!empty($this->pageOption)){
            return $this->pageOption;
        }

        $categories = $this->categoryRepository->findBy(['section' => false]);

        foreach($categories as $category){
            $this->pageOption[StringHelper::capitalize($category->getName())] = $category->getId();
        }

        return $this->pageOption;
    }

    private function generateSectionOption(){
        if(!empty($this->sectionOption)){
            return $this->sectionOption;
        }

        $categories = $this->categoryRepository->findBy(['section' => true]);

        foreach($categories as $category){
            $this->sectionOption[StringHelper::capitalize($category->getName())] = $category->getId();
        }

        return $this->sectionOption;
    }

    public function getPageCategoryOption(){
        return $this->generatePageOption();
    }

    public function getSectionCategoryOption(){
        return $this->generateSectionOption();
    }

}