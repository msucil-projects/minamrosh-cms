<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 11:44 AM
 */

namespace AppBundle\Domain\PropertyOption;


use AppBundle\Repository\PageRepository;
use AppBundle\Util\StringHelper;

class PageOption
{
    private $pageRepository;

    private $pageOption = [];

    public function __construct(PageRepository $repository)
    {
        $this->pageRepository = $repository;
    }

    private function generatePageOption(){
        if(!empty($this->pageOption)){
            return $this->pageOption;
        }

        $pages = $this->pageRepository->findAll();

        foreach($pages as $page){
            $this->pageOption[StringHelper::capitalize($page->getTitle())] = $page->getId();
        }

        return $this->pageOption;
    }


    public function getPageOption(){
        return $this->generatePageOption();
    }

}