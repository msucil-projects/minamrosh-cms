<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 8/30/2017
 * Time: 9:46 PM
 */

namespace AppBundle\Domain\PropertyOption;


use AppBundle\Repository\MemberRepository;

class MemberOption {
	private $memberRepository;

	public function __construct(MemberRepository $repository) {
		$this->memberRepository = $repository;
	}

	public function getDisplayOrder() {
		$total = count( $this->memberRepository->findAll() );
		$order = [];

		if($total == null || $total == 0){
			$order[0] = 0;
		}

		for ( $i = 0; $i <= $total; $i ++ ) {
			$order[ $i ] = $i;
		}

		return $order;
	}
}