<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/2/17
 * Time: 7:31 PM
 */

namespace AppBundle\Domain\PropertyOption;


class Status
{
    const DRAFT = 0;
    const PUBLISH = 1;
    const ARCHIVE = 2;

    private static $status = [];
    private static $statusOptions = [];

    public static function getStatus()
    {
        self::init();
        return self::$status;
    }

    private static function init()
    {
        if (empty(self::$status)) {
            self::$status = [
                self::DRAFT => 'Draft',
                self::PUBLISH => "Publish",
                self::ARCHIVE => 'Archive'
            ];
        }
    }

    public static function getStatusOption()
    {
        self::initOptions();
        return self::$statusOptions;
    }

    private static function initOptions()
    {
        if (empty(self::$statusOptions)) {
            self::$statusOptions = [
                'Draft' => self::DRAFT,
                'Publish' => self::PUBLISH,
                'Archive' => self::ARCHIVE
            ];
        }
    }

    public static function getStatusLabel($code)
    {
        self::init();
        return self::$status[$code] ? self::$status[$code] : null;
    }

    public static function getStatusValues(){
        return [self::DRAFT, self::PUBLISH, self::ARCHIVE];
    }
}