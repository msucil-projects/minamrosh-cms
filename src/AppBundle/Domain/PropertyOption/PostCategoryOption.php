<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/17/17
 * Time: 10:03 PM
 */

namespace AppBundle\Domain\PropertyOption;


use AppBundle\Repository\PostCategoryRepository;
use AppBundle\Util\StringHelper;

class PostCategoryOption
{
    private $categoryRepository;

    private $categories;
    private $categoryOption = [];

    public function __construct(PostCategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->categories = $this->categoryRepository->findAll();
    }

    private function generateCategoryOption($excludeId){
        if(!empty($this->categoryOption)){
            return $this->categoryOption;
        }

        foreach($this->categories as $category){
            if($category->getId() != $excludeId){
                $this->categoryOption[StringHelper::capitalize($category->getName())] = $category->getId();
            }
        }

        return $this->categoryOption;
    }

    public function getCategoryOption($excludeId){
        return $this->generateCategoryOption($excludeId);
    }

    public function getCategoryDisplayOrder(){
        $total = count($this->categories);

        if($total) {
            $order = [];
            for($i = 0; $i < $total; $i++){
                $order[$i] = $i;
            }
        }

        return [0];
    }

}