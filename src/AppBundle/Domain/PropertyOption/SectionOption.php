<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 8:27 PM
 */

namespace AppBundle\Domain\PropertyOption;


class SectionOption
{
    const SECTION = true;
    const PAGE = false;

    private static $label = null;
    private static $option = null;

    private static function generateLabel(){
        self::$label = [self::SECTION => 'Section', self::PAGE => 'Page'];
    }

    private static function generateOption(){
        self::$option = ['Section' => self::SECTION, 'Page' => self::PAGE];
    }

    public static function getOption(){
        self::generateOption();
        return self::$option;
    }

    public static function getOptionLabel($value){
        self::generateLabel();
        return self::$option[$value] ? self::$option[$value] : null;
    }

    public static function getOptionValue(){
        return [ self::SECTION, self::PAGE];
    }
}