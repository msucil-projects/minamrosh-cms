<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/28/17
 * Time: 5:50 PM
 */

namespace AppBundle\Domain\PropertyOption\Advertisement;


use AppBundle\Repository\Advertisement\AdClientRepository;
use AppBundle\Repository\Advertisement\AdSectionRepository;
use AppBundle\Util\StringHelper;

class AdSectionOption
{
    private $sectionRepository;

    private $sectionOption = [];

    public function __construct(AdSectionRepository $repository)
    {
        $this->sectionRepository = $repository;
    }

    private function generateSectionOption(){
        if(!empty($this->sectionOption)){
            return $this->sectionOption;
        }

        $sections = $this->sectionRepository->findAll();

        foreach($sections as $section){
            $this->sectionOption[StringHelper::capitalize($section->getName())] = $section->getId();
        }

        return $this->sectionOption;
    }


    public function getSectionOption(){
        return $this->generateSectionOption();
    }

}