<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/28/17
 * Time: 5:56 PM
 */

namespace AppBundle\Domain\PropertyOption\Advertisement;


use AppBundle\Repository\Advertisement\AdClientRepository;
use AppBundle\Util\StringHelper;

class AdClientOption
{
    private $clientRepository;

    private $clientOption = [];

    public function __construct(AdClientRepository $repository)
    {
        $this->clientRepository = $repository;
    }

    private function generateClientOption(){
        if(!empty($this->clientOption)){
            return $this->clientOption;
        }

        $clients = $this->clientRepository->findAll();

        foreach($clients as $client){
            $this->clientOption[StringHelper::capitalize($client->getFullName())] = $client->getId();
        }

        return $this->clientOption;
    }


    public function getClientOption(){
        return $this->generateClientOption();
    }

}