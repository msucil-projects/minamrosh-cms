<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/17/17
 * Time: 10:03 PM
 */

namespace AppBundle\Domain\PropertyOption;

use AppBundle\Service\AuthorServiceInterface;
use AppBundle\Util\StringHelper;

class AuthorOption
{
    private $authorService;

    private $authors;
    private $authorOptions = [];

    public function __construct(AuthorServiceInterface $authorService)
    {
        $this->authorService = $authorService;
        $this->authors = $this->authorService->findAllBy([], ['name' => 'asc']);
    }

    public function getAuthorOption(){
        return $this->generateAuthorOption();
    }

    private function generateAuthorOption(){
        if(!empty($this->authorOptions)){
            return $this->authorOptions;
        }

        foreach ($this->authors as $author) {
            $this->authorOptions[StringHelper::capitalize($author->getName())] = $author->getId();
        }

        return $this->authorOptions;
    }
}