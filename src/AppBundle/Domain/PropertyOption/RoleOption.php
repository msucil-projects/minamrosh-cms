<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/5/2017
 * Time: 8:52 PM
 */

namespace AppBundle\Domain\PropertyOption;



use AppBundle\Repository\Security\RoleRepository;

class RoleOption {
	private $repository;

	private $roleOption = [];

	public function __construct(RoleRepository $repository) {
		$this->repository = $repository;
	}

	private function init(){
		if(!empty($this->roleOption)){
			return $this->roleOption;
		}

		$roles = $this->repository->findAll();

		foreach($roles as $role){
			if($role->getName() !== 'ROLE_ADMIN'){
				$this->roleOption[$role->getName()] = $role->getId();
			}
		}

		return $this->roleOption;
	}

	public function get(){
		return $this->init();
	}
}