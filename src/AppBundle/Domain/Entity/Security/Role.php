<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/3/17
 * Time: 10:17 PM
 */

namespace AppBundle\Domain\Entity\Security;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role
 * @package AppBundle\Domain\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Security\RoleRepository")
 * @ORM\Table(name="role")
 * @UniqueEntity(fields={"name"}, message="This role is already exist.")
 */
class Role
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Domain\Entity\Security\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=true)
     */
    private $childRole;

    /**
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version()
     */
    private $version;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getChildRole()
    {
        return $this->childRole;
    }

    /**
     * @param mixed $childRole
     * @return $this
     */
    public function setChildRole(Role $childRole)
    {
        $this->childRole = $childRole;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param mixed $version
     * @return Role
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }
}