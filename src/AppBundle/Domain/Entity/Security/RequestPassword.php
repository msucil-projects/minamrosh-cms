<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/10/17
 * Time: 2:54 PM
 */

namespace AppBundle\Domain\Entity\Security;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RequestPassword
 * @package AppBundle\Domain\Entity\Security
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Security\RequestPasswordRepository")
 * @ORM\Table(name="request_password")
 * @UniqueEntity(fields={"userId", "requestKey"})
 */
class RequestPassword
{
    /**
     * @ORM\Column(name="request_key", type="string", nullable=false)
     * @Assert\NotNull()
     * @ORM\Id()
     */
    private $requestKey;

    /**
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @Assert\NotNull()
     */
    private $userId;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getRequestKey()
    {
        return $this->requestKey;
    }

    /**
     * @param mixed $requestKey
     * @return RequestPassword
     */
    public function setRequestKey($requestKey)
    {
        $this->requestKey = $requestKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     * @return RequestPassword
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return RequestPassword
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

}