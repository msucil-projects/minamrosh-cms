<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 3/4/18
 * Time: 1:11 PM
 */

namespace AppBundle\Domain\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="author")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AuthorRepository")
 *
 */
class Author
{
    /**
     * @var bigint
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="slug")
     * @Assert\NotBlank()
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(name="bio", type="text")
     */
    private $bio;

    /**
     * @var string
     * @ORM\Column(name="facebook_link")
     * @Assert\Url()
     */
    private $facebookLink;

    /**
     * @var string
     * @ORM\Column(name="twitter_link")
     * @Assert\Url()
     */
    private $twitterLink;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin_link")
     * @Assert\Url()
     */
    private $linkedinLink;

    /**
     * @var string
     *
     * @ORM\Column(name="email")
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Media",
     *     fetch="EAGER",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(nullable=true,
     *     name="media_id",
     *     referencedColumnName="id"
     * )
     */
    private $image;

    /**
     * @var array
     *
     * @ORM\OneToMany(
     *     mappedBy="author",
     *     orphanRemoval=true,
     *     targetEntity="AppBundle\Domain\Entity\Post",
     *     cascade={"remove"}
     * )
     */
    private $posts;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version()
     */
    private $version = 0;

    public function __construct() {
        $this->posts = new ArrayCollection();
    }

    /**
     * @return bigint
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param bigint $id
     * @return Author
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Author
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param string $bio
     * @return Author
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookLink()
    {
        return $this->facebookLink;
    }

    /**
     * @param string $facebookLink
     * @return Author
     */
    public function setFacebookLink($facebookLink)
    {
        $this->facebookLink = $facebookLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwitterLink()
    {
        return $this->twitterLink;
    }

    /**
     * @param string $twitterLink
     * @return Author
     */
    public function setTwitterLink($twitterLink)
    {
        $this->twitterLink = $twitterLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkedinLink()
    {
        return $this->linkedinLink;
    }

    /**
     * @param string $linkedinLink
     * @return Author
     */
    public function setLinkedinLink($linkedinLink)
    {
        $this->linkedinLink = $linkedinLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Author
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Author
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return Author
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }
}