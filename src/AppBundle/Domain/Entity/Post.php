<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/6/17
 * Time: 9:19 PM
 */

namespace AppBundle\Domain\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Post
 * @package AppBundle\Domain\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @ORM\Table(name="post")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\PostListener"})
 */
class Post
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255, unique=true, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     *
     */
    private $title;

    /**
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=false)
     * @Assert\NotNull()
     */
    private $slug;

    /**
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $content;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $status;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime()
     * @Assert\NotNull()
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Assert\DateTime()
     * @Assert\NotNull()
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_by", type="bigint")
     * @Assert\NotNull()
     */
    private $createdBy;

    /**
     * @ORM\Column(name="updated_by", type="bigint")
     */
    private $updatedBy;

    /**
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version
     */
    private $version;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Media",
     *     fetch="EAGER",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(
     *     nullable=true,
     *     name="media_id",
     *     referencedColumnName="id"
     * )
     */
    private $featureImage;

	/**
	 * @ORM\Column(name="feature_post", type="boolean", nullable=false)
	 * @Assert\NotNull()
	 */
    private $featurePost = false;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Meta",
     *     fetch="EAGER",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(
     *     nullable=true,
     *     name="meta_id",
     *     referencedColumnName="id"
     * )
     */
    private $meta;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Domain\Entity\PostCategory",
     *     fetch="EAGER",
     *     inversedBy="posts"
     * )
     * @ORM\JoinColumn(
     *     nullable=false,
     *     name="category_id",
     *     referencedColumnName="id"
     * )
     */
    private $category;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Domain\Entity\Author",
     *     fetch="EAGER",
     *     inversedBy="posts"
     * )
     * @ORM\JoinColumn(
     *     nullable=true,
     *     name="author_id",
     *     referencedColumnName="id"
     * )
     */
    private $author;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    public function setFeatureImage(Media $featureImage){
        $this->featureImage = $featureImage;
        return $this;
    }

    public function getFeatureImage(){
        return $this->featureImage;
    }

    /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param mixed $meta
     * @return Post
     */
    public function setMeta(Meta $meta)
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Post
     */
    public function setCategory(PostCategory $category)
    {
        $this->category = $category;
        return $this;
    }

	/**
	 * @return mixed
	 */
	public function getFeaturePost() {
		return $this->featurePost;
	}

	/**
	 * @param mixed $featurePost
	 *
	 * @return Post
	 */
	public function setFeaturePost( $featurePost ) {
		$this->featurePost = $featurePost;

		return $this;
	}

    /**
     * @param mixed $author
     * @return Post
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

}