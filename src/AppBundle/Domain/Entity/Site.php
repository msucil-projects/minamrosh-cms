<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 5/31/2017
 * Time: 10:22 PM
 */

namespace AppBundle\Domain\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Site
 * @package AppBundle\Domain\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SiteRepository")
 * @ORM\Table(name="site")
 */
class Site {
	/**
	 * @ORM\Column(name="id", type="bigint")
	 * @ORM\Id()
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="string", nullable=false)
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 */
	private $name;

	/**
	 * @ORM\OneToOne(
	 *     targetEntity="AppBundle\Domain\Entity\Meta",
	 *     fetch="EAGER",
	 *     orphanRemoval=true,
	 *     cascade={"persist", "remove", "merge"}
	 * )
	 * @ORM\JoinColumn(
	 *     nullable=true,
	 *     name="meta_id",
	 *     referencedColumnName="id"
	 * )
	 */
	private $meta;

	/**
	 * @ORM\OneToOne(
	 *     targetEntity="AppBundle\Domain\Entity\Media",
	 *     fetch="EAGER",
	 *     orphanRemoval=true,
	 *     cascade={"persist", "remove", "merge"}
	 * )
	 * @ORM\JoinColumn(
	 *     nullable=true,
	 *     name="banner",
	 *     referencedColumnName="id"
	 * )
	 */
	private $banner;

	/**
	 * @ORM\OneToOne(
	 *     targetEntity="AppBundle\Domain\Entity\Media",
	 *     fetch="EAGER",
	 *     orphanRemoval=true,
	 *     cascade={"persist", "remove", "merge"}
	 * )
	 * @ORM\JoinColumn(
	 *     nullable=true,
	 *     name="logo",
	 *     referencedColumnName="id"
	 * )
	 */
	private $logo;

	/**
	 * @ORM\Column(name="phone_no", type="string")
	 */
	private $phoneNo;

	/**
	 * @ORM\Column(name="mobile_no", type="string")
	 */
	private $mobileNo;

	/**
	 * @ORM\Column(name="fax_no", type="string")
	 */
	private $faxNo;

	/**
	 * @ORM\Column(name="email", type="string", nullable=false)
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @ORM\Column(name="facebook_link", type="string")
	 * @Assert\Url()
	 */
	private $facebookLink;

	/**
	 * @ORM\Column(name="twitter_link", type="string")
	 * @Assert\Url()
	 */
	private $twitterLink;

	/**
	 * @ORM\Column(name="google_plus_link", type="string")
	 * @Assert\Url()
	 */
	private $googlePlusLink;

	/**
	 * @ORM\Column(name="linkedin_link", type="string")
	 * @Assert\Url()
	 */
	private $linkedinLink;

	/**
	 * @ORM\Column(name="google_analytic_script", type="string")
	 *
	 */
	private $googleAnalyticScript;

	/**
	 * @ORM\Column(name="google_analytic_link", type="string")
	 * @Assert\Url()
	 */
	private $googleAnalyticLink;

	/**
	 * @ORM\Column(name="address", type="string")
	 */
	private $address;

	/**
	 * @ORM\Column(name="version", type="integer")
	 * @ORM\Version()
	 */
	private $version;

	/**
	 * @ORM\Column(name="enabled", type="boolean")
	 */
	private $enabled = true;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return Site
	 */
	public function setId( $id ) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 *
	 * @return Site
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMeta() {
		return $this->meta;
	}

	/**
	 * @param mixed $meta
	 *
	 * @return Site
	 */
	public function setMeta(Meta $meta ) {
		$this->meta = $meta;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBanner() {
		return $this->banner;
	}

	/**
	 * @param mixed $banner
	 *
	 * @return Site
	 */
	public function setBanner(Media $banner ) {
		$this->banner = $banner;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLogo() {
		return $this->logo;
	}

	/**
	 * @param mixed $logo
	 *
	 * @return Site
	 */
	public function setLogo(Media $logo ) {
		$this->logo = $logo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPhoneNo() {
		return $this->phoneNo;
	}

	/**
	 * @param mixed $phoneNo
	 *
	 * @return Site
	 */
	public function setPhoneNo( $phoneNo ) {
		$this->phoneNo = $phoneNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMobileNo() {
		return $this->mobileNo;
	}

	/**
	 * @param mixed $mobileNo
	 *
	 * @return Site
	 */
	public function setMobileNo( $mobileNo ) {
		$this->mobileNo = $mobileNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFaxNo() {
		return $this->faxNo;
	}

	/**
	 * @param mixed $faxNo
	 *
	 * @return Site
	 */
	public function setFaxNo( $faxNo ) {
		$this->faxNo = $faxNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 *
	 * @return Site
	 */
	public function setEmail( $email ) {
		$this->email = $email;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFacebookLink() {
		return $this->facebookLink;
	}

	/**
	 * @param mixed $facebookLink
	 *
	 * @return Site
	 */
	public function setFacebookLink( $facebookLink ) {
		$this->facebookLink = $facebookLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTwitterLink() {
		return $this->twitterLink;
	}

	/**
	 * @param mixed $twitterLink
	 *
	 * @return Site
	 */
	public function setTwitterLink( $twitterLink ) {
		$this->twitterLink = $twitterLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGooglePlusLink() {
		return $this->googlePlusLink;
	}

	/**
	 * @param mixed $googlePlusLink
	 *
	 * @return Site
	 */
	public function setGooglePlusLink( $googlePlusLink ) {
		$this->googlePlusLink = $googlePlusLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLinkedinLink() {
		return $this->linkedinLink;
	}

	/**
	 * @param mixed $linkedinLink
	 *
	 * @return Site
	 */
	public function setLinkedinLink( $linkedinLink ) {
		$this->linkedinLink = $linkedinLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGoogleAnalyticScript() {
		return $this->googleAnalyticScript;
	}

	/**
	 * @param mixed $googleAnalyticScript
	 *
	 * @return Site
	 */
	public function setGoogleAnalyticScript( $googleAnalyticScript ) {
		$this->googleAnalyticScript = $googleAnalyticScript;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGoogleAnalyticLink() {
		return $this->googleAnalyticLink;
	}

	/**
	 * @param mixed $googleAnalyticLink
	 *
	 * @return Site
	 */
	public function setGoogleAnalyticLink( $googleAnalyticLink ) {
		$this->googleAnalyticLink = $googleAnalyticLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param mixed $address
	 *
	 * @return Site
	 */
	public function setAddress( $address ) {
		$this->address = $address;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * @param mixed $version
	 *
	 * @return Site
	 */
	public function setVersion( $version ) {
		$this->version = $version;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEnabled() {
		return $this->enabled;
	}

	/**
	 * @param mixed $enabled
	 *
	 * @return Site
	 */
	public function setEnabled( $enabled ) {
		$this->enabled = $enabled;

		return $this;
	}
}