<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 7:12 PM
 */

namespace AppBundle\Domain\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class PostCategory
 * @package AppBundle\Domain\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostCategoryRepository")
 * @ORM\Table(name="post_category")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\PostCategoryListener"})
 */
class PostCategory
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", unique=true)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(name="editable", type="boolean")
     */
    private $editable;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Domain\Entity\PostCategory", fetch="EAGER", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     *
     */
    private $parent;

    /**
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Domain\Entity\PostCategory",
     *     mappedBy="parent",
     *     fetch="EAGER",
     *     cascade={"remove"})
     */
    private $children;

    /**
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version()
     * @ParamConverter()
     */
    private $version;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="display_order", type="integer", nullable=false)
     */
    private $displayOrder;

    /**
     * @ORM\OneToMany(
     *     cascade={"remove"},
     *     targetEntity="AppBundle\Domain\Entity\Post",
     *     orphanRemoval=true,
     *     mappedBy="category"
     * )
     */
    private $posts;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->displayOrder = 0;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PostCategory
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return PostCategory
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return PostCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * @param mixed $editable
     * @return PostCategory
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return PostCategory
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return PostCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return PostCategory
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return PostCategory
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * @param mixed $displayOrder
     * @return PostCategory
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     * @return PostCategory
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
        return $this;
    }
}