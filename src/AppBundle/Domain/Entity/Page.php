<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 11:26 PM
 */

namespace AppBundle\Domain\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Page
 * @package AppBundle\Domain\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 * @ORM\Table(name="page")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\PageListener"})
 */
class Page
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", nullable=false, unique=true)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(name="slug", type="string", nullable=false, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\SectionCategory",
     *     fetch="EAGER"
     * )
     * @ORM\JoinColumn(
     *     nullable=false,
     *     name="category_id",
     *     referencedColumnName="id"
     * )
     */
    private $category;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Media",
     *     fetch="EAGER",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(
     *     nullable=true,
     *     name="media_id",
     *     referencedColumnName="id"
     * )
     */
    private $featureImage;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     * @Assert\NotNull()
     */
    private $status;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Meta",
     *     fetch="EAGER",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(
     *     nullable=true,
     *     name="meta_id",
     *     referencedColumnName="id"
     * )
     */
    private $meta;

    /**
     * @ORM\Column(name="version", type="integer", nullable=true)
     * @ORM\Version()
     */
    private $version;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @ORM\Column(name="created_by", type="bigint", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $createdBy;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="updated_by", type="bigint", nullable=true)
     */
    private $updatedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Page
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Page
     */
    public function setCategory(SectionCategory $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeatureImage()
    {
        return $this->featureImage;
    }

    /**
     * @param mixed $featureImage
     * @return Page
     */
    public function setFeatureImage(Media $featureImage)
    {
        $this->featureImage = $featureImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Page
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param mixed $meta
     * @return Page
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Page
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return Page
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return Page
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     * @return Page
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return Page
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

}