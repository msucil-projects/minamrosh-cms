<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 8:01 PM
 */

namespace AppBundle\Domain\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SectionCategory
 * @package AppBundle\Domain\Entity
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\SectionCategoryRepository"
 * )
 * @ORM\Table(name="section_category")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\SectionCategoryListener"})
 *
 */
class SectionCategory
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     *
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", nullable=false, unique=true)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $slug;

    /**
     * @ORM\Column(name="description", type="string", nullable=true, )
     */
    private $description;


    /**
     * @ORM\Column(name="section", type="boolean", nullable=false)
     */
    private $section;

    /**
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version()
     */
    private $version;

    /**
     * @ORM\Column(name="editable", type="boolean")
     */
    private $editable = true;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SectionCategory
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return SectionCategory
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return SectionCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return SectionCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     * @return SectionCategory
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return SectionCategory
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * @param mixed $editable
     * @return SectionCategory
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
        return $this;
    }

}