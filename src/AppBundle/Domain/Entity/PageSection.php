<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/20/17
 * Time: 4:28 PM
 */

namespace AppBundle\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PageSection
 * @package AppBundle\Domain\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageSectionRepository")
 * @ORM\Table(name="page_section")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\PageSectionListener"})
 */
class PageSection
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id();
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $title;

    /**
     * @ORM\Column(name="slug", type="string", nullable=false)
     * @Assert\NotNull()
     */
    private $slug;

    /**
     * @ORM\Column(name="content", type="text", nullable=false)
     * @Assert\NotNull()
     */
    private $content;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     * @Assert\NotNull()
     */
    private $status;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Media",
     *     fetch="EAGER",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(
     *     nullable=true,
     *     name="media_id",
     *     referencedColumnName="id"
     * )
     */
    private $featureImage;

    /**
     * @ORM\Column(name="section_type", type="integer", nullable=false)
     * @Assert\NotNull()
     */
    private $section_type;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\SectionCategory",
     *     fetch="EAGER"
     * )
     * @ORM\JoinColumn
     *     nullable= true,
     *     name="category_id",
     *     referencedColumnName="id"
     * )
     */
    private $category;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Page",
     *     fetch="EAGER"
     * )
     * @ORM\JoinColumn(
     *     nullable= true,
     *     name="page_id",
     *     referencedColumnName="id"
     * )
     */
    private $page;

    /**
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version()
     */
    private $version;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $createdAt;

    /**
     * @ORM\Column(name="created_by", type="bigint", nullable=false)
     * @Assert\NotNull()
     */
    private $createdBy;

    /**
     * @ORM\Column(name="updated_at", type="bigint", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="updated_by", type="bigint", nullable=true)
     */
    private $updatedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PageSection
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSectionType()
    {
        return $this->section_type;
    }

    /**
     * @param mixed $section_type
     * @return PageSection
     */
    public function setSectionType($section_type)
    {
        $this->section_type = $section_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return PageSection
     */
    public function setCategory(SectionCategory $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     * @return PageSection
     */
    public function setPage(Page $page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return PageSection
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return PageSection
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return PageSection
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return PageSection
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     * @return PageSection
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return PageSection
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return PageSection
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return PageSection
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeatureImage()
    {
        return $this->featureImage;
    }

    /**
     * @param mixed $featureImage
     * @return PageSection
     */
    public function setFeatureImage($featureImage)
    {
        $this->featureImage = $featureImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return PageSection
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

}