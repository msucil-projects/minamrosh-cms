<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 4:50 PM
 */

namespace AppBundle\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Member
 * @package AppBundle\Domain\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MemberRepository")
 * @ORM\Table(name="member")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\MemberListener"})
 */
class Member {

	/**
	 * @ORM\Column(name="id", type="bigint")
	 * @ORM\Id()
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="full_name", type="string", nullable=false, unique=true)
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 */
	private $fullName;

	/**
	 * @ORM\OneToOne(
	 *     targetEntity="AppBundle\Domain\Entity\Media",
	 *     fetch="EAGER",
	 *     orphanRemoval=true,
	 *     cascade={"persist", "remove", "merge"}
	 * )
	 * @ORM\JoinColumn(
	 *     nullable=true,
	 *     name="media_id",
	 *     referencedColumnName="id"
	 * )
	 */
	private $image;

	/**
	 * @ORM\Column(name="designation", type="string", nullable=false)
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 */
	private $designation;

	/**
	 * @ORM\Column(name="email", type="string", nullable=false)
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @ORM\Column(name="facebook_link", type="string")
	 * @Assert\Url()
	 */
	private $facebookLink;

	/**
	 * @ORM\Column(name="twitter_link", type="string")
	 * @Assert\Url()
	 */
	private $twitterLink;

	/**
	 * @ORM\Column(name="google_plus_link", type="string")
	 * @Assert\Url()
	 */
	private $googlePlusLink;

	/**
	 * @ORM\Column(name="linkedin_link", type="string")
	 * @Assert\Url()
	 */
	private $linkedinLink;

	/**
	 * @ORM\Column(name="phone_no", type="string")
	 */
	private $phoneNo;

	/**
	 * @ORM\Column(name="mobile_no", type="string")
	 */
	private $mobileNo;

	/***
	 * @ORM\Column(name="bio", type="text")
	 */
	private $bio;

	/**
	 * @ORM\Column(name="version", type="integer")
	 * @ORM\Version()
	 */
	private $version;

	/**
	 * @var int
	 * @ORM\Column(name="display_order", type="integer")
	 */
	private $displayOrder = 0;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return Member
	 */
	public function setId( $id ) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFullName() {
		return $this->fullName;
	}

	/**
	 * @param mixed $fullName
	 *
	 * @return Member
	 */
	public function setFullName( $fullName ) {
		$this->fullName = $fullName;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param mixed $image
	 *
	 * @return Member
	 */
	public function setImage( $image ) {
		$this->image = $image;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDesignation() {
		return $this->designation;
	}

	/**
	 * @param mixed $designation
	 *
	 * @return Member
	 */
	public function setDesignation( $designation ) {
		$this->designation = $designation;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 *
	 * @return Member
	 */
	public function setEmail( $email ) {
		$this->email = $email;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFacebookLink() {
		return $this->facebookLink;
	}

	/**
	 * @param mixed $facebookLink
	 *
	 * @return Member
	 */
	public function setFacebookLink( $facebookLink ) {
		$this->facebookLink = $facebookLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTwitterLink() {
		return $this->twitterLink;
	}

	/**
	 * @param mixed $twitterLink
	 *
	 * @return Member
	 */
	public function setTwitterLink( $twitterLink ) {
		$this->twitterLink = $twitterLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGooglePlusLink() {
		return $this->googlePlusLink;
	}

	/**
	 * @param mixed $googlePlusLink
	 *
	 * @return Member
	 */
	public function setGooglePlusLink( $googlePlusLink ) {
		$this->googlePlusLink = $googlePlusLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLinkedinLink() {
		return $this->linkedinLink;
	}

	/**
	 * @param mixed $linkedinLink
	 *
	 * @return Member
	 */
	public function setLinkedinLink( $linkedinLink ) {
		$this->linkedinLink = $linkedinLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPhoneNo() {
		return $this->phoneNo;
	}

	/**
	 * @param mixed $phoneNo
	 *
	 * @return Member
	 */
	public function setPhoneNo( $phoneNo ) {
		$this->phoneNo = $phoneNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMobileNo() {
		return $this->mobileNo;
	}

	/**
	 * @param mixed $mobileNo
	 *
	 * @return Member
	 */
	public function setMobileNo( $mobileNo ) {
		$this->mobileNo = $mobileNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBio() {
		return $this->bio;
	}

	/**
	 * @param mixed $bio
	 *
	 * @return Member
	 */
	public function setBio( $bio ) {
		$this->bio = $bio;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * @param mixed $version
	 *
	 * @return Member
	 */
	public function setVersion( $version ) {
		$this->version = $version;

		return $this;
	}

	/**
	 * @param int $displayOrder
	 *
	 * @return $this
	 */
	public function setDisplayOrder( $displayOrder ) {
		$this->displayOrder = $displayOrder;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getDisplayOrder() {
		return $this->displayOrder;
	}
}