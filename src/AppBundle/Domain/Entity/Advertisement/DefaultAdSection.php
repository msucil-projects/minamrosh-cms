<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 7/5/17
 * Time: 10:46 PM
 */

namespace AppBundle\Domain\Entity\Advertisement;


class DefaultAdSection
{
    const HEADER_AD = 'header-ad';
    const TOP_CONTENT_AD = 'top-content-ad';
    const BOTTOM_CONTENT_AD = 'bottom-content-ad';
    const HOMEPAGE_AD = 'homepage-ad';
    const CATEGORY_AD = 'category-ad';
    const TOP_SIDEBAR_AD = 'top-sidebar-ad';
    const BOTTOM_SIDEBAR_AD = 'bottom-sidebar-ad';
}