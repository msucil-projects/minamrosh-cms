<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 10:58 PM
 */

namespace AppBundle\Domain\Entity\Advertisement;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AdsClient
 * @package AppBundle\Domain\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Advertisement\AdClientRepository")
 * @ORM\Table(name="ads_client")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\Advertisement\AdClientListener"})
 */
class AdClient
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="full_name", type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $fullName;

    /**
     * @ORM\Column(name="email", type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(name="phone_no", type="string")
     */
    private $phoneNo;

    /**
     * @ORM\Column(name="mobile_no", type="string")
     */
    private $mobileNo;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version()
     */
    private $version;

    /**
     * @ORM\Column(name="created_by", type="bigint")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $createdBy;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AdClient
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return AdClient
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return AdClient
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     * @return AdClient
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobileNo()
    {
        return $this->mobileNo;
    }

    /**
     * @param mixed $mobileNo
     * @return AdClient
     */
    public function setMobileNo($mobileNo)
    {
        $this->mobileNo = $mobileNo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return AdClient
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return AdClient
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return AdClient
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return AdClient
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

}