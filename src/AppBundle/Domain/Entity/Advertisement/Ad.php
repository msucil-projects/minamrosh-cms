<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/26/17
 * Time: 8:33 AM
 */

namespace AppBundle\Domain\Entity\Advertisement;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Ads
 * @package AppBundle\Domain\Entity\Advertisement
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Advertisement\AdRepository")
 * @ORM\Table(name="ad")
 * @ORM\EntityListeners(value={"AppBundle\Event\EntityListener\Advertisement\AdListener"})
 */
class Ad
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Media",
     *     fetch="EAGER",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(
     *     nullable=false,
     *     name="media_id",
     *     referencedColumnName="id"
     * )
     */
    private $image;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $status;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Advertisement\AdClient",
     *     fetch="EAGER"
     * )
     * @ORM\JoinColumn(
     *     nullable=false,
     *     name="ads_client_id",
     *     referencedColumnName="id"
     * )
     */
    private $client;

    /**
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Domain\Entity\Advertisement\AdSection",
     *     fetch="EAGER"
     * )
     * @ORM\JoinColumn(
     *     nullable=false,
     *     name="ads_section_id",
     *     referencedColumnName="id"
     * )
     */
    private $section;

    /**
     * @ORM\Column(name="expired", type="boolean", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $expired = false;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @ORM\Column(name="created_by", type="bigint", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $createdBy;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="updated_by", type="bigint", nullable=true)
     */
    private $updatedBy;

    /**
     * @ORM\Column(name="valid_from", type="date", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $validFrom;

    /**
     * @ORM\Column(name="valid_to", type="date", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $validTo;

    /**
     * @ORM\Column(name="version", type="integer")
     * @ORM\Version()
     */
    private $version;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="ad_url", type="string", nullable=false)
     * @Assert\Url()
     */
    private $adUrl;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Ad
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Ad
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Ad
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     * @return Ad
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     * @return Ad
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * @param mixed $expired
     * @return Ad
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Ad
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return Ad
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return Ad
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     * @return Ad
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * @param mixed $validFrom
     * @return Ad
     */
    public function setValidFrom($validFrom)
    {
        $this->validFrom = $validFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * @param mixed $validTo
     * @return Ad
     */
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return Ad
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Ad
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdUrl()
    {
        return $this->adUrl;
    }

    /**
     * @param mixed $adUrl
     * @return Ad
     */
    public function setAdUrl($adUrl)
    {
        $this->adUrl = $adUrl;
        return $this;
    }
}
