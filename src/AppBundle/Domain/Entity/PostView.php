<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/27/17
 * Time: 8:40 PM
 */

namespace AppBundle\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PostView
 * @package AppBundle\Domain\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostViewRepository")
 * @ORM\Table(name="post_view")
 */
class PostView
{
    /**
     * @ORM\Column(name="id")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="post_id")
     * @Assert\NotNull()
     */
    private $postId;

    /**
     * @ORM\Column(name="view_count")
     * @Assert\NotNull()
     */
    private $viewCount = 1;

    /**
     * @ORM\Column(name="view_date")
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $viewDate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PostView
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * @param mixed $postId
     * @return PostView
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }

    /**
     * @param mixed $viewCount
     * @return PostView
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewDate()
    {
        return $this->viewDate;
    }

    /**
     * @param mixed $viewDate
     * @return PostView
     */
    public function setViewDate($viewDate)
    {
        $this->viewDate = $viewDate;
        return $this;
    }

}