<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/13/17
 * Time: 11:11 PM
 */

namespace AppBundle\Domain\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Meta
 * @package AppBundle\Domain\Entity
 * @ORM\Entity()
 * @ORM\Table(name="meta")
 */
class Meta
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @ORM\Column(name="keywords", nullable=true, type="string")
     */
    private $keywords;

    /**
     * @ORM\Column(name="description", nullable=true, type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="version", type="integer")
     */
    private $version;

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return Meta
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Meta
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     * @return Meta
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Meta
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Meta
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

}