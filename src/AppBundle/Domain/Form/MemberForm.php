<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/5/2017
 * Time: 2:05 PM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\MemberDTO;
use AppBundle\Domain\PropertyOption\MemberOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MemberForm extends AbstractType {
	private $memberOption;

	public function __construct(MemberOption $option) {
		$this->memberOption = $option;
	}

	public function buildForm( FormBuilderInterface $builder, array $options ) {
		parent::buildForm( $builder, $options );

		$builder->add('id', HiddenType::class)
			->add('fullName', TextType::class, [
				'label' => 'Full Name'
			])
			->add('designation', TextType::class, ['required' => true])
			->add('image', MediaForm::class, [
				'required' => false
			])
			->add('email', EmailType::class)
			->add('bio', TextareaType::class,[
				'required' => false
			])
			->add('displayOrder', ChoiceType::class, [
				'label' => 'Display Order',
				'choices' => $this->memberOption->getDisplayOrder(),
				'placeholder' => 'Select Display Order'
			])
			->add('phoneNo', TextType::class,[
				'label' => 'Phone No',
				'required' => false
			])
			->add('mobileNo', TextType::class, [
				'label' => 'Mobile No',
				'required' => false
			])
			->add('facebookLink', UrlType::class, [
				'label' => 'Facebook Profile Url',
				'required' => false
			])
			->add('twitterLink', UrlType::class, [
				'label' => 'Twitter Profile Url',
				'required' => false
			])
			->add('googleplusLink', UrlType::class, [
				'label' => 'Goolge+ Profile Url',
				'required' => false
			])
			->add('linkedinLink', UrlType::class, [
				'label' => 'LinkedIn Profile Url',
				'required' => false
			])
			->add('version', HiddenType::class)
			->add('save', SubmitType::class);
	}

	public function configureOptions( OptionsResolver $resolver ) {
		parent::configureOptions( $resolver );

		$resolver->setDefaults([
			'data_class' => MemberDTO::class
		]);
	}

}