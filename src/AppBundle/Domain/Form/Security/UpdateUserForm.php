<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 8:19 PM
 */

namespace AppBundle\Domain\Form\Security;

use AppBundle\Domain\DTO\Security\UpdateUser;
use AppBundle\Domain\PropertyOption\RoleOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateUserForm extends AbstractType
{
	private $roleOption;

	public function __construct(RoleOption $option) {
		$this->roleOption = $option;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->setMethod("POST")
            ->add("username", TextType::class, [
                "label" => "Username",
                "required" => true,
                "attr" => ["placeholder" => "Username", 'readonly' => true]
            ])
            ->add("firstName", TextType::class,
                [
                    "label" => "First Name",
                    "required" => true,
                    "attr" => ["placeholder" => "First Name"]
                ])
            ->add('lastName', TextType::class, [
                "label" => "Last Name",
                "required" => false,
                "attr" => ["placeholder" => "Last Name"]
            ])
	        ->add('role', ChoiceType::class, [
	        	'label' => 'Role',
		        'choices' => array_merge(['Select Role' => ''], $this->roleOption->get())
	        ])
            ->add("email", EmailType::class, [
                "label" => "Email",
                "required" => true,
                "attr" => ["placeholder" => "Email"]
            ])
            ->add("password", PasswordType::class, [
                "label" => "Password",
                "required" => false,
                "attr" => ["placeholder" => "Password"]
            ])
            ->add("verifyPassword", PasswordType::class, [
                "label" => "Re-type Password",
                "required" => false,
                "attr" => ["placeholder" => "Re-type Password"]
            ])
            ->add("save", SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-sm btn-default btn-flat"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            "data_class" => UpdateUser::class
        ]);
    }

}