<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/6/17
 * Time: 10:27 PM
 */

namespace AppBundle\Domain\Form\Security;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ResetPassword extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->setMethod("POST")
            ->add("email", EmailType::class, [
                "label" => "Email",
                "required" => true,
                "attr" => ["placeholder" => "Email", 'class' => 'form-control']
            ])
            ->add("resetPassword", SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-sm btn-default btn-flat"
                ]
            ]);
    }

}