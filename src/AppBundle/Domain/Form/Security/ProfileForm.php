<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/9/17
 * Time: 8:15 PM
 */

namespace AppBundle\Domain\Form\Security;


use AppBundle\Domain\DTO\Security\ProfileDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('username', TextType::class, [
                'attr' => ['readonly' => true]
            ])
            ->add('firstName', TextType::class, [
                'label' => 'First Name'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last Name',
                'required' => false
            ])
            ->add('email', EmailType::class)
            ->add('role', TextType::class, [
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Change Profile'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => ProfileDto::class
        ]);
    }

}