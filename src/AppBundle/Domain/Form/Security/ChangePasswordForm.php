<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/8/17
 * Time: 11:46 PM
 */

namespace AppBundle\Domain\Form\Security;


use AppBundle\Domain\DTO\Security\ChangePasswordDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePasswordForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('password', PasswordType::class)
            ->add('verifyPassword', PasswordType::class, [
                'label' => 'Re-type Password'
            ])
            ->add('save', SubmitType::class,[
                'label' => 'Change Password',
                'attr' => [
                    'class' => 'btn btn-sm btn-default btn-flat'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => ChangePasswordDTO::class
        ]);
    }

}