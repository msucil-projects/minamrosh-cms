<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 5/31/2017
 * Time: 11:01 PM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\SiteDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteForm extends AbstractType {

	public function buildForm( FormBuilderInterface $builder, array $options ) {
		parent::buildForm( $builder, $options );

		$builder->setMethod('POST')
			->add('id', HiddenType::class)
			->add('name', TextType::class, [
				'label' => 'Site Name',
				'attr' => [
					'placeholder' => 'Site Name'
				]
			])
			->add('banner', MediaForm::class)
			->add('logo', MediaForm::class)
			->add('phoneNo', TextType::class, [
				'label' => 'Phone No',
				'required' => false
			])
			->add('mobileNo', TextType::class, [
				'label' => 'Mobile No',
				'required' => false
			])
			->add('faxNo', TextType::class, [
				'label' => 'Fax No',
				'required' => false
			])
			->add('email', EmailType::class)
			->add('facebookLink', UrlType::class, [
				'label' => 'Facebook Page Url',
				'required' => false
			])
			->add('twitterLink', UrlType::class, [
				'label' => 'Twitter Profile Url',
				'required' => false
			])
			->add('googlePlusLink', UrlType::class, [
				'label' => 'Google+ Page Url',
				'required' => false
			])
			->add('linkedinLink', UrlType::class, [
				'label' => 'LinkedIn Profile Url',
				'required' => false
			])
			->add('googleAnalyticScript', TextareaType::class, [
				'label' => 'Google Analytic Script',
				'required' => false
			])
			->add('googleAnalyticLink', UrlType::class, [
				'label' => 'Google Analytic Url',
				'required' => false
			])
			->add('address', TextareaType::class, [
				'required' => false
			])
			->add('meta', MetaForm::class)
			->add('version', HiddenType::class)
			->add('enabled', HiddenType::class)
			->add('save', SubmitType::class);
	}

	public function configureOptions( OptionsResolver $resolver ) {
		parent::configureOptions( $resolver );

		$resolver->setDefaults([
			'data_class' => SiteDTO::class
		]);
	}

}