<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/2/17
 * Time: 11:51 AM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\PostDTO;
use AppBundle\Domain\PropertyOption\AuthorOption;
use AppBundle\Domain\PropertyOption\PostCategoryOption;
use AppBundle\Domain\PropertyOption\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostForm extends AbstractType
{
    private $categoryOption;
    private $authorOption;

    public function __construct(PostCategoryOption $categoryOption, AuthorOption $authorOption)
    {
        $this->categoryOption = $categoryOption;
        $this->authorOption = $authorOption;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->setMethod("POST")
            ->add("id", HiddenType::class, [

            ])
            ->add("title", TextType::class, [
                "label" => "Title",
                "required" => true
            ])
            ->add("featureImage", MediaForm::class, [
                "label" => "Feature Image",
                "required" => false,
            ])
            ->add("featurePost", CheckboxType::class, [
                "label" => "Feature Post",
                "required" => false,
	            'attr' => [
	            	'value' => true
	            ]
            ])
            ->add("content", TextareaType::class, [
                "label" => "Content"
            ])
            ->add('categoryId', ChoiceType::class, [
                'label' => 'Category',
                'required' => true,
                'choices' => $this->categoryOption->getCategoryOption(0),
                'placeholder' => 'Select Category'
            ])
            ->add('author', ChoiceType::class, [
                'required' => false,
                'choices' => $this->authorOption->getAuthorOption(),
                'placeholder' => 'Select Author'
            ])
            ->add("status", ChoiceType::class, [
                "label" => "Status",
                "required" => true,
                "choices" => Status::getStatusOption()
            ])
            ->add('meta', MetaForm::class, [
                'label' => 'SEO Section',
                'required' => false
            ])
            ->add("save", SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-sm btn-default btn-flat"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            "data_class" => PostDTO::class
        ]);
    }
}