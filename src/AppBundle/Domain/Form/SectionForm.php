<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 11:48 AM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\PageSectionDTO;
use AppBundle\Domain\PropertyOption\PageOption;
use AppBundle\Domain\PropertyOption\SectionCategoryOption;
use AppBundle\Domain\PropertyOption\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectionForm extends AbstractType
{
    private $categoryOption;

    public function __construct(SectionCategoryOption $categoryOption)
    {
        $this->categoryOption = $categoryOption;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->setMethod("POST")
            ->add("id", HiddenType::class, [
            ])
            ->add("title", TextType::class, [
                "label" => "Title",
                "required" => true
            ])
            ->add('version', HiddenType::class)
            ->add("featureImage", MediaForm::class, [
                "label" => "Feature Image",
                "required" => false
            ])
            ->add("content", TextareaType::class, [
                "label" => "Content"
            ])
            ->add('category', ChoiceType::class, [
                'label' => 'Section Category',
                'required' => true,
                'choices' => $this->categoryOption->getSectionCategoryOption(),
                'placeholder' => 'Select Section Category'
            ])
            ->add("status", ChoiceType::class, [
                "label" => "Status",
                "required" => true,
                "choices" => Status::getStatusOption()
            ])
            ->add("save", SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-sm btn-default btn-flat"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            "data_class" => PageSectionDTO::class
        ]);
    }

}