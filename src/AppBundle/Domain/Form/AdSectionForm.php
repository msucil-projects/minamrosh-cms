<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/25/17
 * Time: 11:04 PM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\Advertisement\AdSectionDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdSectionForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->setMethod('POST')
            ->add('id', HiddenType::class)
            ->add('name', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Ads Section Name'
                ]
            ])
            ->add('height', NumberType::class, [
                'label' => 'Ads Height',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ads Height'
                ]
            ])
            ->add('width', NumberType::class, [
                'label' => 'Ads Width',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ads Width'
                ]
            ])
            ->add('version', HiddenType::class)
            ->add('description', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Description'
                ]
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(['data_class' => AdSectionDTO::class]);
    }

}