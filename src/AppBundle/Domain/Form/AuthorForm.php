<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 3/4/18
 * Time: 4:46 PM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\AuthorDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AuthorForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->setMethod('POST')
            ->add('id', HiddenType::class)
            ->add('name', TextType::class)
            ->add('image', MediaForm::class, ['required' => false])
            ->add('bio', TextareaType::class, ['required' => false])
            ->add('facebookLink', UrlType::class, ['required' => false])
            ->add('twitterLink', UrlType::class, ['required' => false])
            ->add('linkedinLink', UrlType::class, ['required' => false])
            ->add('email', EmailType::class, ['required' => false])
            ->add('version', HiddenType::class)
            ->add('save', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(['data_class' => AuthorDto::class]);
    }
}