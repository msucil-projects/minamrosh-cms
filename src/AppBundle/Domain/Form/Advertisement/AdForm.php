<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/26/17
 * Time: 10:52 PM
 */

namespace AppBundle\Domain\Form\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdDTO;
use AppBundle\Domain\Form\MediaForm;
use AppBundle\Domain\PropertyOption\Advertisement\AdClientOption;
use AppBundle\Domain\PropertyOption\Advertisement\AdSectionOption;
use AppBundle\Domain\PropertyOption\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdForm extends AbstractType
{
    private $clientOption;
    private $sectionOption;

    public function __construct(AdClientOption $adClientOption, AdSectionOption $adSectionOption)
    {
        $this->clientOption = $adClientOption;
        $this->sectionOption = $adSectionOption;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('id', HiddenType::class)
            ->add('client', ChoiceType::class, [
                'label' => 'Client',
                'required' => true,
                'choices' => array_merge(['Select Client' => ''],$this->clientOption->getClientOption())
            ])
            ->add('section', ChoiceType::class, [
                'label' => 'Ad Section',
                'required' => true,
                'choices' => $this->sectionOption->getSectionOption(),
                'placeholder' => 'Select Ad Section'
            ])
            ->add('image', MediaForm::class, [
                'label' => 'Ad Image'
            ])
            ->add('adUrl', UrlType::class, [
                'label' => 'Url'
            ])
            ->add("status", ChoiceType::class, [
                "label" => "Status",
                "required" => true,
                "choices" => Status::getStatusOption()
            ])
            ->add('description', TextareaType::class, [
                'required' => false
            ])
            ->add('expired', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'value' => '1'
                ]
            ])
            ->add('validFrom', DateType::class, [
                'label' => 'Valid From',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'datepicker-from'
                ]
            ])
            ->add('validTo', DateType::class, [
                'label' => 'Valid To',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'datepicker-to'
                ]
            ])
            ->add('version', HiddenType::class)
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => AdDTO::class
        ]);
    }

}