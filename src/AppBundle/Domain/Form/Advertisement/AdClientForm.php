<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 11:26 PM
 */

namespace AppBundle\Domain\Form\Advertisement;


use AppBundle\Domain\DTO\Advertisement\AdClientDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdClientForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->setMethod('POST')
            ->add('id', HiddenType::class, [
                'required' => false
            ])
            ->add('fullName', TextType::class, [
                'label' => 'Full Name'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email'
            ])
            ->add('phoneNo', TextType::class, [
                'label' => 'Phone No',
                'required' => false
            ])
            ->add('mobileNo', TextType::class, [
                'label' => 'Mobile No',
                'required' => false
            ])
            ->add('description', TextareaType::class,[
                'required' => false
            ])
            ->add('version', HiddenType::class)
            ->add('createdBy', HiddenType::class)
            ->add('save', SubmitType::class,[
                'label' => 'Save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => AdClientDTO::class
        ]);
    }

}