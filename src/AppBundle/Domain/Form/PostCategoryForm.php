<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 7:44 PM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\PostCategoryDTO;
use AppBundle\Domain\PropertyOption\PostCategoryOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostCategoryForm extends AbstractType
{
    private $categoryOption;

    public function __construct(PostCategoryOption $categoryOption)
    {
        $this->categoryOption = $categoryOption;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('id', HiddenType::class, ['required' => false])
            ->add('name', TextType::class, ['attr' => ['placeholder' => 'Post Category']])
            ->add('description', TextareaType::class,
                [
                    'required' => false,
                    'attr' => ['placeholder' => 'Description'
                    ]
                ])
            ->add('displayOrder', ChoiceType::class, [
                'label' => 'Display Order',
                'choices' => $this->categoryOption->getCategoryDisplayOrder(),
                'placeholder' => 'Select Display Order'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => PostCategoryDTO::class
        ]);
    }

}