<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/7/17
 * Time: 3:37 PM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\MediaDTO;
use AppBundle\Domain\Form\DataTransformer\MediaViewTransformer;
use AppBundle\Service\FileServiceInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediaForm extends AbstractType
{
    private $fileService;

    public function __construct(FileServiceInterface $fileService)
    {
        $this->fileService = $fileService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('name', FileType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'img-fileinput'
                ]
            ])
            ->add('credit', TextType::class, [
                'label' => 'Image Credit'
            ])
            ->add('id', HiddenType::class, [
                'required' => false
            ])->add('oldName', HiddenType::class, [
                'required' => false
            ])
            ->add('type', HiddenType::class, [
                'required' => false
            ])
            ->add('version', HiddenType::class, [
                'required' => false
            ]);

        $builder->get('name')
            ->addViewTransformer(new MediaViewTransformer($this->fileService));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => MediaDTO::class
        ]);
    }

}