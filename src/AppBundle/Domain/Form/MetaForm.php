<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/13/17
 * Time: 11:39 PM
 */

namespace AppBundle\Domain\Form;


use AppBundle\Domain\DTO\MetaDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetaForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('id', HiddenType::class, [
            'required' => false
        ])
            ->add('title', TextType::class, [
                'label' => 'Page Title',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Page Title - It will be shown in browser'
                ]
            ])
            ->add('keywords', TextType::class, [
                'label' => 'Meta Keywords',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Meta Keywords'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Meta Description',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Meta Description'
                ]
            ])
            ->add('version', HiddenType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => MetaDTO::class
        ]);
    }

}