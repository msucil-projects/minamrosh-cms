<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 8:05 PM
 */

namespace AppBundle\Domain\DTO\Security;


use AppBundle\Validator\Constraints\EqualToField;
use AppBundle\Validator\Constraints\UniqueProperty;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RegisterUser
 * @package AppBundle\Domain\DTO
 */
class RegisterUser
{

    /**
     * @Assert\NotBlank(message="First Name can not be blank.")
     * @Assert\NotNull(message="First Name can not be blank.")
     * @Assert\Length(min=3, minMessage="First Name must be at least 3 character long.")
     */
    private $firstName;

    /**
     * @Assert\Length(
     *     min="1",
     *     minMessage="Last Name must be at least 1 character long."
     * )
     */
    private $lastName;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="4",
     *     minMessage="Username must be at least 4 character long.")
     * @UniqueProperty(
     *     property="username",
     *     repositoryMethod="findOneBy",
     *     entityClass="AppBundle\Domain\Entity\Security\User",
     *     message="This username is already used")
     */
    private $username;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     min="6",
     * minMessage="Password must be at least 6 character long.")
     */
    private $password;

    /**
     * @EqualToField(field="password", message="Password does not match")
     */
    private $verifyPassword;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return RegisterUser
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return RegisterUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return RegisterUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return RegisterUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return RegisterUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerifyPassword()
    {
        return $this->verifyPassword;
    }

    /**
     * @param mixed $verifyPassword
     * @return RegisterUser
     */
    public function setVerifyPassword($verifyPassword)
    {
        $this->verifyPassword = $verifyPassword;
        return $this;
    }
}