<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 8:05 PM
 */

namespace AppBundle\Domain\DTO\Security;


use AppBundle\Validator\Constraints\EqualToField;
use AppBundle\Validator\Constraints\UniqueProperty;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UpdateUser
 * @package AppBundle\Domain\DTO
 */
class UpdateUser
{

	private $id;

    /**
     * @Assert\NotBlank(message="First Name can not be blank.")
     * @Assert\NotNull(message="First Name can not be blank.")
     * @Assert\Length(min=3, minMessage="First Name must be at least 3 character long.")
     */
    private $firstName;

    /**
     * @Assert\Length(
     *     min="1",
     *     minMessage="Last Name must be at least 1 character long."
     * )
     */
    private $lastName;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="4",
     *     minMessage="Username must be at least 4 character long.")
     * @UniqueProperty(
     *     property="username",
     *     repositoryMethod="findOneBy",
     *     entityClass="AppBundle\Domain\Entity\Security\User",
     *     message="This username is already used")
     */
    private $username;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    private $role;

    private $oldPassword;

    private $verifyOldPassword;

    /**
     * @Assert\Length(
     *     min="6",
     * minMessage="Password must be at least 6 character long.")
     */
    private $password;

    /**
     * @EqualToField(field="password", message="Password does not match")
     */
    private $verifyPassword;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return UpdateUser
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return UpdateUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return UpdateUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return UpdateUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return UpdateUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerifyPassword()
    {
        return $this->verifyPassword;
    }

    /**
     * @param mixed $verifyPassword
     * @return UpdateUser
     */
    public function setVerifyPassword($verifyPassword)
    {
        $this->verifyPassword = $verifyPassword;
        return $this;
    }

	/**
	 * @return mixed
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * @param mixed $role
	 *
	 * @return UpdateUser
	 */
	public function setRole( $role ) {
		$this->role = $role;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getOldPassword() {
		return $this->oldPassword;
	}

	/**
	 * @param mixed $oldPassword
	 *
	 * @return UpdateUser
	 */
	public function setOldPassword( $oldPassword ) {
		$this->oldPassword = $oldPassword;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVerifyOldPassword() {
		return $this->verifyOldPassword;
	}

	/**
	 * @param mixed $verifyOldPassword
	 *
	 * @return UpdateUser
	 */
	public function setVerifyOldPassword( $verifyOldPassword ) {
		$this->verifyOldPassword = $verifyOldPassword;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return UpdateUser
	 */
	public function setId( $id ) {
		$this->id = $id;

		return $this;
	}
}