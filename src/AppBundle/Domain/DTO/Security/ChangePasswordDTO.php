<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/8/17
 * Time: 11:44 PM
 */

namespace AppBundle\Domain\DTO\Security;


use AppBundle\Validator\Constraints\EqualToField;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePasswordDTO
{
    /**
     * @Assert\Length(
     *     min="6",
     * minMessage="Password must be at least 6 character long.")
     */
    private $password;

    /**
     * @EqualToField(field="password", message="Password does not match")
     */
    private $verifyPassword;

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return ChangePasswordDTO
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerifyPassword()
    {
        return $this->verifyPassword;
    }

    /**
     * @param mixed $verifyPassword
     * @return ChangePasswordDTO
     */
    public function setVerifyPassword($verifyPassword)
    {
        $this->verifyPassword = $verifyPassword;
        return $this;
    }

}