<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/9/17
 * Time: 8:06 PM
 */

namespace AppBundle\Domain\DTO\Security;


use Symfony\Component\Validator\Constraints as Assert;

class ProfileDto
{
    private $username;

    /**
     * @Assert\NotNull(message="Email can not be Blank")
     * @Assert\NotBlank(message="Email can not be Blank")
     * @Assert\Email()
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $firstName;

    private $lastName;

    private $role;

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return ProfileDto
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return ProfileDto
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return ProfileDto
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return ProfileDto
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     * @return ProfileDto
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }
}