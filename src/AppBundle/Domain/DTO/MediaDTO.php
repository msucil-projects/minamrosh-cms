<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/7/17
 * Time: 3:30 PM
 */

namespace AppBundle\Domain\DTO;


use Symfony\Component\Validator\Constraints as Assert;

class MediaDTO
{
    /**
     * @Assert\Image()
     */
    private $name;

    private $type;

    private $id;

    private $oldName;

    private $credit;

    /**
     * @return mixed
     */
    public function getOldName()
    {
        return $this->oldName;
    }

    /**
     * @param mixed $oldName
     * @return MediaDTO
     */
    public function setOldName($oldName)
    {
        $this->oldName = $oldName;
        return $this;
    }

    /**
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @Assert\DateTime()
     */
    private $updatedAt;

    private $version;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return MediaDTO
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return MediaDTO
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return MediaDTO
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return MediaDTO
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return MediaDTO
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return MediaDTO
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param mixed $credit
     * @return MediaDTO
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
        return $this;
    }

}