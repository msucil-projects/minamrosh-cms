<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 11:18 AM
 */

namespace AppBundle\Domain\DTO;


use Symfony\Component\Validator\Constraints as Assert;

class PageSectionDTO
{
    private $id;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @Assert\Type(type="AppBundle\Domain\DTO\MediaDTO")
     * @Assert\Valid()
     */
    private $featureImage;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $content;

    private $page;

    private $category;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $status;

    private $version;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PageSectionDTO
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return PageSectionDTO
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeatureImage()
    {
        return $this->featureImage;
    }

    /**
     * @param mixed $featureImage
     * @return PageSectionDTO
     */
    public function setFeatureImage($featureImage)
    {
        $this->featureImage = $featureImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return PageSectionDTO
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     * @return PageSectionDTO
     */
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return PageSectionDTO
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return PageSectionDTO
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return PageSectionDTO
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}