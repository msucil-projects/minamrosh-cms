<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/12/17
 * Time: 12:38 AM
 */

namespace AppBundle\Domain\DTO;


class DataTableRequestBody
{
    private $start;

    private $length;

    private $columns;

    private $order;

    private $search;

    private $draw;

    public function getDraw()
    {
        return $this->draw;
    }

    public function setDraw($draw)
    {
        $this->draw = $draw;
        return $this;
    }

    public function getStart(): int
    {
        return $this->start;
    }

    public function setStart(int $start): DataTableRequestBody
    {
        $this->start = $start;
        return $this;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function setLength(int $length): DataTableRequestBody
    {
        $this->length = $length;
        return $this;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function setColumns( $columns)
    {
        $this->columns = $columns;
        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Object $order
     * @return DataTableRequestBody
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return Object
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param Object $search
     * @return DataTableRequestBody
     */
    public function setSearch( $search)
    {
        $this->search = $search;
        return $this;
    }
}