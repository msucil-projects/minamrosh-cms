<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/26/17
 * Time: 9:31 AM
 */

namespace AppBundle\Domain\DTO\Advertisement;


use Symfony\Component\Validator\Constraints as Assert;

class AdDTO
{

    private $id;

    /**
     * @Assert\Type(type="AppBundle\Domain\DTO\MediaDTO")
     * @Assert\Valid()
     */
    private $image;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $client;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $section;

    private $description;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $validFrom;

    /**
     * @Assert\NotNull()
     * @Assert\Date()
     * @Assert\NotBlank()
     */
    private $validTo;

    private $expired = false;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Choice(callback= {"AppBundle\Domain\PropertyOption\Status", "getStatusValues"})
     */
    private $status;

    private $version;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $adUrl;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AdDTO
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return AdDTO
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     * @return AdDTO
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     * @return AdDTO
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return AdDTO
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * @param mixed $validFrom
     * @return AdDTO
     */
    public function setValidFrom($validFrom)
    {
        $this->validFrom = $validFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * @param mixed $validTo
     * @return AdDTO
     */
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * @param mixed $expired
     * @return AdDTO
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return AdDTO
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return AdDTO
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdUrl()
    {
        return $this->adUrl;
    }

    /**
     * @param mixed $adUrl
     * @return AdDTO
     */
    public function setAdUrl($adUrl)
    {
        $this->adUrl = $adUrl;
        return $this;
    }
}