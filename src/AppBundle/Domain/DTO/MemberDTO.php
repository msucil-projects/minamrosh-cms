<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:09 PM
 */

namespace AppBundle\Domain\DTO;


use Symfony\Component\Validator\Constraints as Assert;

class MemberDTO {

	private $id;

	/**
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 */
	private $fullName;

	/**
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @Assert\Type(type="AppBundle\Domain\DTO\MediaDTO")
	 * @Assert\Valid()
	 */
	private $image;

	private $bio;

	/**
	 * @Assert\Url()
	 */
	private $facebookLink;

	/**
	 * @Assert\Url()
	 */
	private $twitterLink;

	/**
	 * @Assert\Url()
	 */
	private $googleplusLink;

	/**
	 * @Assert\Url()
	 */
	private $linkedinLink;

	private $phoneNo;

	private $mobileNo;

	private $version;

	/***
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 */
	private $designation;

	/**
	 * @Assert\NotBlank()
	 */
	private $displayOrder = 0;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return MemberDTO
	 */
	public function setId( $id ) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFullName() {
		return $this->fullName;
	}

	/**
	 * @param mixed $fullName
	 *
	 * @return MemberDTO
	 */
	public function setFullName( $fullName ) {
		$this->fullName = $fullName;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 *
	 * @return MemberDTO
	 */
	public function setEmail( $email ) {
		$this->email = $email;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param mixed $image
	 *
	 * @return MemberDTO
	 */
	public function setImage( $image ) {
		$this->image = $image;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBio() {
		return $this->bio;
	}

	/**
	 * @param mixed $bio
	 *
	 * @return MemberDTO
	 */
	public function setBio( $bio ) {
		$this->bio = $bio;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFacebookLink() {
		return $this->facebookLink;
	}

	/**
	 * @param mixed $facebookLink
	 *
	 * @return MemberDTO
	 */
	public function setFacebookLink( $facebookLink ) {
		$this->facebookLink = $facebookLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTwitterLink() {
		return $this->twitterLink;
	}

	/**
	 * @param mixed $twitterLink
	 *
	 * @return MemberDTO
	 */
	public function setTwitterLink( $twitterLink ) {
		$this->twitterLink = $twitterLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGoogleplusLink() {
		return $this->googleplusLink;
	}

	/**
	 * @param mixed $googleplusLink
	 *
	 * @return MemberDTO
	 */
	public function setGoogleplusLink( $googleplusLink ) {
		$this->googleplusLink = $googleplusLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLinkedinLink() {
		return $this->linkedinLink;
	}

	/**
	 * @param mixed $linkedinLink
	 *
	 * @return MemberDTO
	 */
	public function setLinkedinLink( $linkedinLink ) {
		$this->linkedinLink = $linkedinLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPhoneNo() {
		return $this->phoneNo;
	}

	/**
	 * @param mixed $phoneNo
	 *
	 * @return MemberDTO
	 */
	public function setPhoneNo( $phoneNo ) {
		$this->phoneNo = $phoneNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMobileNo() {
		return $this->mobileNo;
	}

	/**
	 * @param mixed $mobileNo
	 *
	 * @return MemberDTO
	 */
	public function setMobileNo( $mobileNo ) {
		$this->mobileNo = $mobileNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * @param mixed $version
	 *
	 * @return MemberDTO
	 */
	public function setVersion( $version ) {
		$this->version = $version;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDesignation() {
		return $this->designation;
	}

	/**
	 * @param mixed $designation
	 *
	 * @return MemberDTO
	 */
	public function setDesignation( $designation ) {
		$this->designation = $designation;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDisplayOrder() {
		return $this->displayOrder;
	}

	/**
	 * @param mixed $displayOrder
	 */
	public function setDisplayOrder( $displayOrder ) {
		$this->displayOrder = $displayOrder;
		return $this;
	}
}