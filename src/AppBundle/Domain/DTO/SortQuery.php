<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/18/17
 * Time: 9:34 PM
 */

namespace AppBundle\Domain\DTO;


class SortQuery
{

    private $columnName;

    private $sortDirection;

    /**
     * DataTableOrderRequest constructor.
     * @param $columnName
     * @param $orderDirection
     */
    public function __construct($columnName, $orderDirection)
    {
        $this->columnName = $columnName;
        $this->sortDirection = $orderDirection;
    }

    /**
     * @return mixed
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * @param mixed $columnName
     * @return SortQuery
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSortDirection()
    {
        return $this->sortDirection;
    }

    /**
     * @param mixed $sortDirection
     * @return SortQuery
     */
    public function setSortDirection($sortDirection)
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }

}