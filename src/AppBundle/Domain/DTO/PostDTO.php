<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/2/17
 * Time: 11:48 AM
 */

namespace AppBundle\Domain\DTO;


use AppBundle\Domain\PropertyOption\Status;
use Symfony\Component\Validator\Constraints as Assert;

class PostDTO
{
    /**
     * @Assert\NotBlank(
     *     message="Title can not be blank."
     * )
     * @Assert\Length(
     *     min="5",
     *     minMessage="Title should be of at least 5 character."
     * )
     */
    private $title;

    private $id;

    /**
     * @Assert\NotBlank(
     *     message="Content can not be blank."
     * )
     */
    private $content;

    /**
     * @Assert\Type(type="AppBundle\Domain\DTO\MediaDTO")
     * @Assert\Valid()
     */
    private $featureImage;

	/**
	 * @Assert\NotNull()
	 */
    private $featurePost = false;

    /**
     * @Assert\NotBlank()
     *
     */
    private $status;

    private $version;

    /**
     * @Assert\Type(type="AppBundle\Domain\DTO\MetaDTO")
     * @Assert\Valid()
     */
    private $meta;

    private $categoryId;

    private $author;

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return PostDTO
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeatureImage()
    {
        return $this->featureImage;
    }

    /**
     * @param mixed $featureImage
     */
    public function setFeatureImage(MediaDTO $featureImage)
    {
        $this->featureImage = $featureImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    public function getStatusOptions(){
        Status::getStatusValues();
    }

    public function setVersion($version){
        $this->version = $version;
        return $this;
    }

    public function getVersion(){
        return $this->version;
    }

    public function setId($id){
        $this->id = $id;
        return $this;
    }

    public function getId(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param mixed $meta
     * @return PostDTO
     */
    public function setMeta(MetaDTO $meta)
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     * @return PostDTO
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
        return $this;
    }

	/**
	 * @return mixed
	 */
	public function getFeaturePost() {
		return $this->featurePost;
	}

	/**
	 * @param mixed $featurePost
	 *
	 * @return PostDTO
	 */
	public function setFeaturePost( $featurePost ) {
		$this->featurePost = $featurePost;

		return $this;
	}

}