<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/17/17
 * Time: 11:00 PM
 */

namespace AppBundle\Domain\DTO;

/**
 * Class QueryResult
 * @package AppBundle\Domain\DTO
 *
 * Used for returning result including additional information required for pagination
 */
class QueryResult
{
    private $totalResult;

    private $result;

    /**
     * QueryResult constructor.
     * @param $totalResult
     * @param $result
     */
    public function __construct($totalResult, $result)
    {
        $this->totalResult = $totalResult;
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getTotalResult()
    {
        return $this->totalResult;
    }

    /**
     * @param mixed $totalResult
     * @return QueryResult
     */
    public function setTotalResult($totalResult)
    {
        $this->totalResult = $totalResult;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     * @return QueryResult
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }
}