<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 8:15 PM
 */

namespace AppBundle\Domain\DTO;


use AppBundle\Validator\Constraints\UniqueProperty;
use Symfony\Component\Validator\Constraints as Assert;

class SectionCategoryDTO
{

    private $id;

    /**
     * @UniqueProperty(
     *     property="name",
     *     entityClass="AppBundle\Domain\Entity\SectionCategory",
     *     repositoryMethod="findOneBy",
     *     message="This category is already used.")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $name;


    private $description;

    /**
     * @Assert\NotNull()
     * @Assert\Choice(callback= {"AppBundle\Domain\PropertyOption\SectionOption", "getOptionValue"})
     */
    private $section;

    private $version;

    private $editable = true;

    /**
     * @return mixed
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * @param mixed $editable
     * @return SectionCategoryDTO
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SectionCategoryDTO
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return SectionCategoryDTO
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return SectionCategoryDTO
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     * @return SectionCategoryDTO
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return SectionCategoryDTO
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

}