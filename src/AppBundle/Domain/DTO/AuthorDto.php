<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 3/4/18
 * Time: 4:30 PM
 */

namespace AppBundle\Domain\DTO;


use Symfony\Component\Validator\Constraints as Assert;

class AuthorDto
{
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $name;

    private $bio;

    /**
     * @var string
     * @Assert\Url()
     */
    private $facebookLink;

    /**
     * @var string
     *
     * @Assert\Url()
     */
    private $twitterLink;

    /**
     * @var string
     *
     * @Assert\Url()
     */
    private $linkedinLink;

    /**
     * @var string
     *
     * @Assert\Email()
     */
    private $email;

    /**
     *
     * @Assert\Type(type="AppBundle\Domain\DTO\MediaDTO")
     * @Assert\Valid()
     */
    private $image;

    private $version;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AuthorDto
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AuthorDto
     */
    public function setName( $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param mixed $bio
     * @return AuthorDto
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookLink()
    {
        return $this->facebookLink;
    }

    /**
     * @param string $facebookLink
     * @return AuthorDto
     */
    public function setFacebookLink( $facebookLink)
    {
        $this->facebookLink = $facebookLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwitterLink()
    {
        return $this->twitterLink;
    }

    /**
     * @param string $twitterLink
     * @return AuthorDto
     */
    public function setTwitterLink( $twitterLink)
    {
        $this->twitterLink = $twitterLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkedinLink()
    {
        return $this->linkedinLink;
    }

    /**
     * @param string $linkedinLink
     * @return AuthorDto
     */
    public function setLinkedinLink( $linkedinLink)
    {
        $this->linkedinLink = $linkedinLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return AuthorDto
     */
    public function setEmail( $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return MediaDTO
     */
    public function getImage()
    {
        return $this->image;
    }


    public function setImage( $image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return AuthorDto
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }
}