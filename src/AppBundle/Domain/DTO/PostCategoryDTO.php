<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 7:38 PM
 */

namespace AppBundle\Domain\DTO;


use AppBundle\Validator\Constraints\UniqueProperty;
use Symfony\Component\Validator\Constraints as Assert;

class PostCategoryDTO
{

    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @UniqueProperty(
     *     property="name",
     *     entityClass="AppBundle\Domain\Entity\PostCategory",
     *     repositoryMethod="findOneBy",
     *     message="This category is already used."
     *
     * )
     */
    private $name;

    private $version;

    private $description;

    private $parent;

    /**
     * @Assert\NotNull()
     */
    private $displayOrder;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PostCategoryDTO
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return PostCategoryDTO
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return PostCategoryDTO
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return PostCategoryDTO
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return PostCategoryDTO
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * @param mixed $displayOrder
     * @return PostCategoryDTO
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;
        return $this;
    }
}