<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 5/31/2017
 * Time: 10:22 PM
 */

namespace AppBundle\Domain\DTO;
use Symfony\Component\Validator\Constraints as Assert;


class SiteDTO {

	private $id;

	/**
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 */
	private $name;

	private $meta;

	private $banner;

	private $logo;

	private $phoneNo;


	private $mobileNo;

	private $faxNo;

	/**
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @Assert\Url()
	 */
	private $facebookLink;

	/**
	 * @Assert\Url()
	 */
	private $twitterLink;

	/**
	 * @Assert\Url()
	 */
	private $googlePlusLink;

	/**
	 * @Assert\Url()
	 */
	private $linkedinLink;

	/**
	 *
	 */
	private $googleAnalyticScript;

	/**
	 * @Assert\Url()
	 */
	private $googleAnalyticLink;

	/**
	 */
	private $address;

	/**
	 */
	private $version;


	private $enabled = true;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return SiteDTO
	 */
	public function setId( $id ) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 *
	 * @return SiteDTO
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMeta() {
		return $this->meta;
	}

	/**
	 * @param mixed $meta
	 *
	 * @return SiteDTO
	 */
	public function setMeta(MetaDTO $meta ) {
		$this->meta = $meta;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBanner() {
		return $this->banner;
	}

	/**
	 * @param mixed $banner
	 *
	 * @return SiteDTO
	 */
	public function setBanner(MediaDTO $banner ) {
		$this->banner = $banner;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLogo() {
		return $this->logo;
	}

	/**
	 * @param mixed $logo
	 *
	 * @return SiteDTO
	 */
	public function setLogo(MediaDTO $logo ) {
		$this->logo = $logo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPhoneNo() {
		return $this->phoneNo;
	}

	/**
	 * @param mixed $phoneNo
	 *
	 * @return SiteDTO
	 */
	public function setPhoneNo( $phoneNo ) {
		$this->phoneNo = $phoneNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMobileNo() {
		return $this->mobileNo;
	}

	/**
	 * @param mixed $mobileNo
	 *
	 * @return SiteDTO
	 */
	public function setMobileNo( $mobileNo ) {
		$this->mobileNo = $mobileNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFaxNo() {
		return $this->faxNo;
	}

	/**
	 * @param mixed $faxNo
	 *
	 * @return SiteDTO
	 */
	public function setFaxNo( $faxNo ) {
		$this->faxNo = $faxNo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 *
	 * @return SiteDTO
	 */
	public function setEmail( $email ) {
		$this->email = $email;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFacebookLink() {
		return $this->facebookLink;
	}

	/**
	 * @param mixed $facebookLink
	 *
	 * @return SiteDTO
	 */
	public function setFacebookLink( $facebookLink ) {
		$this->facebookLink = $facebookLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTwitterLink() {
		return $this->twitterLink;
	}

	/**
	 * @param mixed $twitterLink
	 *
	 * @return SiteDTO
	 */
	public function setTwitterLink( $twitterLink ) {
		$this->twitterLink = $twitterLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGooglePlusLink() {
		return $this->googlePlusLink;
	}

	/**
	 * @param mixed $googlePlusLink
	 *
	 * @return SiteDTO
	 */
	public function setGooglePlusLink( $googlePlusLink ) {
		$this->googlePlusLink = $googlePlusLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLinkedinLink() {
		return $this->linkedinLink;
	}

	/**
	 * @param mixed $linkedinLink
	 *
	 * @return SiteDTO
	 */
	public function setLinkedinLink( $linkedinLink ) {
		$this->linkedinLink = $linkedinLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGoogleAnalyticScript() {
		return $this->googleAnalyticScript;
	}

	/**
	 * @param mixed $googleAnalyticScript
	 *
	 * @return SiteDTO
	 */
	public function setGoogleAnalyticScript( $googleAnalyticScript ) {
		$this->googleAnalyticScript = $googleAnalyticScript;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getGoogleAnalyticLink() {
		return $this->googleAnalyticLink;
	}

	/**
	 * @param mixed $googleAnalyticLink
	 *
	 * @return SiteDTO
	 */
	public function setGoogleAnalyticLink( $googleAnalyticLink ) {
		$this->googleAnalyticLink = $googleAnalyticLink;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param mixed $address
	 *
	 * @return SiteDTO
	 */
	public function setAddress( $address ) {
		$this->address = $address;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * @param mixed $version
	 *
	 * @return SiteDTO
	 */
	public function setVersion( $version ) {
		$this->version = $version;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isEnabled() {
		return $this->enabled;
	}

	/**
	 * @param bool $enabled
	 *
	 * @return SiteDTO
	 */
	public function setEnabled( $enabled ) {
		$this->enabled = $enabled;

		return $this;
	}
}