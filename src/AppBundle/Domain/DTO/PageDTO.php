<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 11:56 PM
 */

namespace AppBundle\Domain\DTO;


use Symfony\Component\Validator\Constraints as Assert;

class PageDTO
{

    private $id;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $title;

    /**
     *
     */
    private $content;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $category;

    /**
     * @Assert\NotNull()
     */
    private $status;

    private $version;

    /**
     * @Assert\Type(type="AppBundle\Domain\DTO\MediaDTO")
     * @Assert\Valid()
     */
    private $featureImage;

    /**
     * @Assert\Type(type="AppBundle\Domain\DTO\MetaDTO")
     * @Assert\Valid()
     */
    private $meta;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param mixed $meta
     */
    public function setMeta(MetaDTO $meta)
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeatureImage()
    {
        return $this->featureImage;
    }

    /**
     * @param mixed $featureImage
     * @return PageDTO
     */
    public function setFeatureImage(MediaDTO $featureImage)
    {
        $this->featureImage = $featureImage;
        return $this;
    }

}