<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/21/17
 * Time: 11:58 AM
 */

namespace AppBundle\Util;


class SectionType
{
    const PAGE = 0;
    const SECTION = 1;
}