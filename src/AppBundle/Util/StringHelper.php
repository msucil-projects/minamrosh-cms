<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/4/17
 * Time: 11:33 PM
 */

namespace AppBundle\Util;


class StringHelper
{
   private static $HYPHEN = "-";
   private static $SPACE = " ";

    public static function upperFirst($string){
        return ucfirst($string);
    }

    public static function capitalize($string){
        return ucwords($string);
    }

    public static function generateSlug($string){
       return str_replace(self::$SPACE, self::$HYPHEN, strtolower($string));
    }

    public static function generateUnique($string) {
        return uniqid() . $string;
    }

    public static function truncateWords($string, $count, $suffix = '[...]')
    {
        $words = preg_split('/(\s+)/u', trim($string), null, PREG_SPLIT_DELIM_CAPTURE);
        if (count($words) / 2 > $count) {
            return implode('', array_slice($words, 0, ($count * 2) - 1)) . $suffix;
        } else {
            return $string;
        }
    }

    public static function countWords($string)
    {
        return count(preg_split('/(\s+)/u', trim($string), null, PREG_SPLIT_DELIM_CAPTURE));
    }

    public static function startsWith($string, $compareWith){
    	return (substr($string, 0, strlen($compareWith)) === $compareWith);
    }
}