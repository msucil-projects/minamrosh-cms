<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 4/12/17
 * Time: 10:58 PM
 */

namespace AppBundle\Util;


class DataTableUtil
{
    private static $sortDirection = [
        "asc" => SORT_ASC,
        "desc" => SORT_DESC
    ];

    public static function getSortDirection($dir){
        return self::$sortDirection[$dir];
    }

}