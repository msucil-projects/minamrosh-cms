<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/27/17
 * Time: 8:15 PM
 */

namespace AppBundle\Event\ViewTracking;


final class ViewTrackingEvents
{
    const POST_VIEWED = 'post.viewed';
    const SITE_VIEWED = 'site.viewed';
    const POST_CATEGORY_VIEWED = 'post.category.viewed';
}