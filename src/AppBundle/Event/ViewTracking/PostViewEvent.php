<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/27/17
 * Time: 8:06 PM
 */

namespace AppBundle\Event\ViewTracking;


use AppBundle\Domain\Entity\Post;
use Symfony\Component\EventDispatcher\Event;

class PostViewEvent extends Event
{
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function getPost()
    {
        return $this->post;
    }
}