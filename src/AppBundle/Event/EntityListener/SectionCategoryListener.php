<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/18/17
 * Time: 10:47 PM
 */

namespace AppBundle\Event\EntityListener;


use AppBundle\Domain\Entity\SectionCategory;
use AppBundle\Util\StringHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class SectionCategoryListener
{
    public function preUpdate(SectionCategory $category, PreUpdateEventArgs $event){

        if($event->hasChangedField('name')){
            $category->setSlug(StringHelper::generateSlug($category->getName()));
        }
        else {
            $category->setSlug($event->getOldValue('slug'));
        }
    }

    public function prePersist(SectionCategory $category, LifecycleEventArgs $eventArgs){
        $category->setSlug(StringHelper::generateSlug($category->getName()));
    }
}