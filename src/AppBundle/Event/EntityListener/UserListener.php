<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 6/8/17
 * Time: 10:15 PM
 */

namespace AppBundle\Event\EntityListener;


use AppBundle\Domain\Entity\Security\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class UserListener
{
    public function preUpdate(User $user, PreUpdateEventArgs $eventArgs){
        if(!$user->getPassword()){
            $user->setPassword($eventArgs->getOldValue('password'));
        }

        if(!$user->getRole()){
            $user->setRole($eventArgs->getOldValue('role'));
        }

        if(!$user->getCreatedAt()){
            $user->setCreatedAt($eventArgs->getOldValue('createdAt'));
        }

        $user->setUpdatedAt(new \DateTime());
    }

    public function prePersist(User $user, LifecycleEventArgs $eventArgs){
        $user->setCreatedAt(new \DateTime());
    }

}