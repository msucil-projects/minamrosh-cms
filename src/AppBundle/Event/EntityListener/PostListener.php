<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 2:04 PM
 */

namespace AppBundle\Event\EntityListener;


use AppBundle\Domain\Entity\Post;
use AppBundle\Service\FileServiceInterface;
use AppBundle\Service\Security\CurrentUserServiceInterface;
use AppBundle\Util\StringHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class PostListener
{
    private $currentUserService;
    private $fileService;

    public function __construct(CurrentUserServiceInterface $currentUserService, FileServiceInterface $fileService)
    {
        $this->currentUserService = $currentUserService;
        $this->fileService        = $fileService;
    }

    public function preUpdate(Post $post, PreUpdateEventArgs $event){
        if($event->hasChangedField('title')){
            $post->setSlug(StringHelper::generateSlug($post->getTitle()));
        }

        $post->setUpdatedBy($this->currentUserService->getId());
        $post->setUpdatedAt(new \DateTime('now'));

        $post->setCreatedAt($event->getOldValue('createdAt'));
        $post->setCreatedBy($event->getOldValue('createdBy'));
    }

    public function prePersist(Post $post, LifecycleEventArgs $eventArgs){
        $post->setSlug(StringHelper::generateSlug($post->getTitle()));
        $post->setCreatedAt(new \DateTime('now'));
        $post->setCreatedBy($this->currentUserService->getId());
    }

    public function postRemove(Post $post, LifecycleEventArgs $eventArgs){
	    if ($post->getFeatureImage()){
		    $this->fileService->deleteFile($post->getFeatureImage()->getName());
	    }
    }
}