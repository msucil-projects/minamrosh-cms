<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/16/17
 * Time: 11:11 PM
 */

namespace AppBundle\Event\EntityListener;


use AppBundle\Domain\Entity\PostCategory;
use AppBundle\Util\StringHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class PostCategoryListener
{
    public function preUpdate(PostCategory $category, PreUpdateEventArgs $event){

        if($event->hasChangedField('name')){
            $category->setSlug(StringHelper::generateSlug($category->getName()));
        }
        else {
            $category->setSlug($event->getOldValue('slug'));
        }
    }

    public function prePersist(PostCategory $category, LifecycleEventArgs $eventArgs){
        $category->setSlug(StringHelper::generateSlug($category->getName()));
    }
}