<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/14/17
 * Time: 2:04 PM
 */

namespace AppBundle\Event\EntityListener;


use AppBundle\Domain\Entity\Page;
use AppBundle\Service\FileServiceInterface;
use AppBundle\Service\Security\CurrentUserServiceInterface;
use AppBundle\Util\StringHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class PageListener
{
    private $currentUserService;
    private $fileService;

    public function __construct(CurrentUserServiceInterface $currentUserService, FileServiceInterface $fileSerivce)
    {
        $this->currentUserService = $currentUserService;
        $this->fileService        = $fileSerivce;
    }

    public function preUpdate(Page $page, PreUpdateEventArgs $event){
        if($event->hasChangedField('title')){
            $page->setSlug(StringHelper::generateSlug($page->getTitle()));
        }

        $page->setUpdatedBy($this->currentUserService->getId());
        $page->setUpdatedAt(new \DateTime('now'));

        $page->setCreatedAt($event->getOldValue('createdAt'));
        $page->setCreatedBy($event->getOldValue('createdBy'));
    }

    public function prePersist(Page $page, LifecycleEventArgs $eventArgs){
        $page->setSlug(StringHelper::generateSlug($page->getTitle()));
        $page->setCreatedAt(new \DateTime('now'));
        $page->setCreatedBy($this->currentUserService->getId());
    }

    public function postRemove(Page $page, LifecycleEventArgs $eventArgs){
    	if ($page->getFeatureImage()){
    		$this->fileService->deleteFile($page->getFeatureImage()->getName());
	    }
    }
}