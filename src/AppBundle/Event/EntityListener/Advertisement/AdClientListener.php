<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/23/17
 * Time: 11:08 PM
 */

namespace AppBundle\Event\EntityListener\Advertisement;


use AppBundle\Domain\Entity\Advertisement\AdClient;
use AppBundle\Service\Security\CurrentUserServiceInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class AdClientListener
{
    private $currentUserService;

    public function __construct(CurrentUserServiceInterface $currentUserService)
    {
        $this->currentUserService = $currentUserService;
    }

    public function preUpdate(AdClient $client, PreUpdateEventArgs $eventArgs){
        $client->setCreatedAt($eventArgs->getOldValue('created_at'));
        $client->setCreatedBy($eventArgs->getOldValue('created_by'));
    }

    public function prePersist(AdClient $client, LifecycleEventArgs $eventArgs){

        $client->setCreatedAt(new \DateTime('now'));
        $client->setCreatedBy($this->currentUserService->getId());
    }
}