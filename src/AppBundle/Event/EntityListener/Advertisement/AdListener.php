<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/26/17
 * Time: 10:42 PM
 */

namespace AppBundle\Event\EntityListener\Advertisement;


use AppBundle\Domain\Entity\Advertisement\Ad;
use AppBundle\Service\FileServiceInterface;
use AppBundle\Service\Security\CurrentUserServiceInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class AdListener
{
    private $currentUserService;
    private $fileService;

    public function __construct(CurrentUserServiceInterface $currentUserService, FileServiceInterface $fileService){
        $this->currentUserService = $currentUserService;
        $this->fileService        = $fileService;
    }

    public function preUpdate(Ad $ads, PreUpdateEventArgs $eventArgs){
        $ads->setCreatedBy($eventArgs->getOldValue('createdBy'))
            ->setCreatedAt($eventArgs->getOldValue('createdAt'))
            ->setUpdatedAt(new \DateTime('now'))
            ->setUpdatedBy($this->currentUserService->getId());

    }

    public function prePersist(Ad $ads, LifecycleEventArgs $eventArgs){
        $ads->setCreatedAt(new \DateTime());
        $ads->setCreatedBy($this->currentUserService->getId());
    }

    public function postRemove(Ad $ad, LifecycleEventArgs $eventArgs){
    	if($ad->getImage()){
		    $this->fileService->deleteFile($ad->getImage()->getName());
	    }
    }
}