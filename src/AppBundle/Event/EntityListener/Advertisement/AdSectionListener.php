<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/25/17
 * Time: 10:52 PM
 */

namespace AppBundle\Event\EntityListener\Advertisement;


use AppBundle\Domain\Entity\Advertisement\AdSection;
use AppBundle\Service\Security\CurrentUserServiceInterface;
use AppBundle\Util\StringHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class AdSectionListener
{
    private $currentUserService;

    public function __construct(CurrentUserServiceInterface $currentUserService)
    {
        $this->currentUserService = $currentUserService;
    }

    public function preUpdate(AdSection $section, PreUpdateEventArgs $eventArgs){
        if($eventArgs->hasChangedField('name')){
            $section->setSlug(StringHelper::generateSlug($section->getName()));
        }

        $section->setCreatedAt($eventArgs->getOldValue('createdAt'));
    }

    public function prePersist(AdSection $section, LifecycleEventArgs $eventArgs){
        $section->setSlug(StringHelper::generateSlug($section->getName()));
        $section->setCreatedAt(new \DateTime('now'));
    }
}