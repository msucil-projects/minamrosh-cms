<?php
/**
 * Created by IntelliJ IDEA.
 * User: ales
 * Date: 6/2/2017
 * Time: 5:19 PM
 */

namespace AppBundle\Event\EntityListener;


use AppBundle\Domain\Entity\Member;
use AppBundle\Service\FileService;
use AppBundle\Service\FileServiceInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class MemberListener {
	private $fileService;

	public function __construct(FileServiceInterface $fileService) {
		$this->fileService = $fileService;
	}

	public function postRemove(Member $member, LifecycleEventArgs $eventArgs){
		if ($member->getImage()->getName() !== null){
			$this->fileService->deleteFile($member->getImage()->getName());
		}
	}
}