<?php
/**
 * Created by IntelliJ IDEA.
 * User: msucil
 * Date: 5/20/17
 * Time: 4:42 PM
 */

namespace AppBundle\Event\EntityListener;


use AppBundle\Domain\Entity\PageSection;
use AppBundle\Service\Security\CurrentUserServiceInterface;
use AppBundle\Util\SectionType;
use AppBundle\Util\StringHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class PageSectionListener
{
    private $currentUserService;

    public function __construct(CurrentUserServiceInterface $currentUserService)
    {
        $this->currentUserService = $currentUserService;
    }

    public function preUpdate(PageSection $section, PreUpdateEventArgs $event){
        if($event->hasChangedField('title')){
            $section->setSlug(StringHelper::generateSlug($section->getTitle()));
        }
        else{
            $section->setSlug($event->getOldValue('slug'));
        }

        $section->setSectionType($event->getOldValue('section_type'));
        $section->setUpdatedBy($this->currentUserService->getId());

        $section->setCreatedAt($event->getOldValue('createdAt'));
        $section->setCreatedBy($event->getOldValue('createdBy'));
    }

    public function prePersist(PageSection $section, LifecycleEventArgs $eventArgs){
        if($section->getPage()){
            $section->setSectionType(SectionType::PAGE);
        }
        else{
            $section->setSectionType(SectionType::SECTION);
        }

        $section->setSlug(StringHelper::generateSlug($section->getTitle()));
        $section->setCreatedAt(new \DateTime('now'));
        $section->setCreatedBy($this->currentUserService->getId());
    }
}