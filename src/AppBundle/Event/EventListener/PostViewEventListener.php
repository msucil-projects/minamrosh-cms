<?php
/**
 * Created by PhpStorm.
 * User: msucil
 * Date: 6/27/17
 * Time: 8:21 PM
 */

namespace AppBundle\Event\EventListener;


use AppBundle\Domain\Entity\PostView;
use AppBundle\Event\ViewTracking\PostViewEvent;
use AppBundle\Repository\PostViewRepository;

class PostViewEventListener
{
    private $postViewRepository;

    public function __construct(PostViewRepository $postViewRepository)
    {
        $this->postViewRepository = $postViewRepository;

    }

    public function onPostViewed(PostViewEvent $viewEvent)
    {
        $post = $viewEvent->getPost();
        $viewDate = new \DateTime();

        $postView = $this->getPostView($post->getId(), $viewDate->format("Y-m-d"));

        if ($postView) {
            $postView->setViewCount($postView->getViewCount() + 1);
            $this->postViewRepository->update($postView);
        } else {
            $postView = new PostView();
            $postView->setPostId($post->getId())
                ->setViewDate($viewDate->format("Y-m-d"));

            $this->postViewRepository->save($postView);
        }
    }

    private function getPostView($postId, $viewDate)
    {
        return $this->postViewRepository->findOneBy(['postId' => $postId, 'viewDate' => $viewDate]);
    }
}