/**
 * Created by msucil on 4/2/17.
 */
(function(window ){
    window.configCkeditor = configCKEditor;
    window.initBootstrapTagsInput = initBootstrapTagsInput();
    window.deleteItem = deleteItem;
    window.initBootstrapFileInput = initBootstrapFileInput;

    function configCKEditor(fieldId) {
        CKEDITOR.replace(fieldId, {
            filebrowserBrowseUrl: '/web/assets/library/responsive-filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
            filebrowserUploadUrl: '/web/assets/library/responsive-filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
            filebrowserImageBrowseUrl: '/web/assets/library/responsive-filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
        });
    }

    function initBootstrapTagsInput() {
        $("[data-role='input-tags']").tagsinput({
            tagClass : 'label label-default'
        });
    }

    /**
     * delete datatable item
     * @param event
     */
    function deleteItem(event){
        var target = event.currentTarget;
        var url = $(target).data('action');

        $.ajax({
            url: url,
            method: 'POST',
            success: function (data, status, jqXhr) {
                console.log(jqXhr);

                window.location.reload();
                $("#delete-modal").modal('hide');
              /* var datatable =  $('#datatable').dataTable();
                datatable._fnAjaxUpdate(datatable.fnSettings());*/

            },
            error: function (data) {
                alert(data.responseText);

            }

        });

    }

    function initBootstrapFileInput(selector, config) {
        $(selector).fileinput(config);
    }
    
})(window);