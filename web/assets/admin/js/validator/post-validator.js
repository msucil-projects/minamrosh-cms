/**
 * Created by msucil on 4/2/17.
 */
(function (window) {
    window.validatePostForm = validatePostForm();

    function validatePostForm() {
        var postForm = $("form[name='post_form']");

        postForm
            .submit(function (event) {
            if (!$(this).valid()) {
                event.preventDefault();
                return false
            }

            return true;
        });

        postForm
            .validate({
                rules: {
                    "post_form[title]": {
                        required: true,
                        minlength: 5,
                    },
                    "post_form[pageTitle": {
                        maxlength: 5
                    }
                },
                messages: {
                    "post_form[title]": {
                        minlength: "Title should be of at least 5 character long."
                    },
                    "post_form[featureImage]": {
                        accept: "Feature Image should be of JPG, JPEG, PNG or GIF type."
                    }
                }
            })

    }

})(window);