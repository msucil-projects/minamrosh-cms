/**
 * Created by msucil on 4/2/17.
 */

(function (window){
    window.bootstrapValidator = bootstrapValidator();

    function titleValidator(value, element, fieldName) {
        var regex = new RegExp("^[a-z]+|[\\d]+|[\\s]+|[\\w\\.\\-\\_]+","igm");
        return this.optional(element) || regex.test(value);
    }

    function bootstrapValidator() {
        $.validator.addMethod("title", titleValidator, $.validator.format("{0} can contain only A-Z,a-z,0-9,-,_,&."));
    }
    
})(window);